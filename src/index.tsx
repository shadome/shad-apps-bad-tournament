import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import "./front/css/button.css";
import "./front/css/dimensions.css";
import "./front/css/spacing.css";
import "./front/css/flex.css";
import "./front/css/radiobutton.css";
import "./front/css/input.css";
import "./front/css/misc.css";
import App from "./front/pages/App";
import reportWebVitals from "./reportWebVitals";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
