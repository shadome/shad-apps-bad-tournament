import * as MeetingRepository from "./meetings";
import { Field } from "../core/types";
import { isMeetingComplete } from "../core/functions/utils";

export function getAll(): Field[] {
  return getAllStr().map(fromStr);
}

export function getAllStr(): string[] {
  return Object.keys(Field);
}

export function fromStr(fieldStr: string): Field {
  return Field[fieldStr as keyof typeof Field];
}

export function toString(field: Field): string {
  switch (field) {
    case Field.Field1:
      return "Terrain 1";
    case Field.Field2:
      return "Terrain 2";
    case Field.Field3:
      return "Terrain 3";
    case Field.Field4:
      return "Terrain 4";
    case Field.Field5:
      return "Terrain 5";
    case Field.Field6:
      return "Terrain 6";
    case Field.Field7:
      return "Terrain 7";
  }
}

export function getAvailableFields(): Field[] {
  return (
    MeetingRepository
      // get all meetings
      .getAll()
      // keep ongoing meetings only
      .filter((meeting) => !isMeetingComplete(meeting))
      // extract fields currently used
      .reduce(
        (fields, meeting) =>
          fields.filter(
            (field) => field !== meeting.field && field !== meeting.field_2
          ),
        getAll()
      )
  );
}
