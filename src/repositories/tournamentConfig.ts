import { TournamentConfig, TournamentPhase } from "../core/types";
import { _getConfig, _setConfig } from "./localstorage";

// default value
// const default_config = {
//   current_phase: TournamentPhase.Ranking,
//   turn_nb: 1,
//   nb_of_pools: 4,
//   nb_of_meetings_per_team: 4,
// };
export const DEFAULT_CONFIG = {
  current_phase: TournamentPhase.Setup,
  turn_nb: 1,
  nb_of_pools: undefined,
  nb_of_meetings_per_team: undefined,
};

// let config: TournamentConfig = { ...default_config };

export function get(): TournamentConfig {
  return _getConfig();
}

export function incrementTurn(): void {
  const config = _getConfig();
  config.turn_nb++;
  _setConfig(config);
}

export function decrementTurn(): void {
  const config = _getConfig();
  config.turn_nb--;
  _setConfig(config);
}

/**
 * Must be called when teams are added or deleted
 * because the old pools config values become irrelevant.
 */
export function resetPoolsConfiguration(): void {
  const config = _getConfig();
  config.nb_of_pools = undefined;
  config.nb_of_meetings_per_team = undefined;
  _setConfig(config);
}

export function setNbOfPools(nb_of_pools: number): void {
  const config = _getConfig();
  config.nb_of_pools = nb_of_pools;
  _setConfig(config);
}

export function setNbOfMeetingsPerTeam(nb_of_meetings_per_team: number): void {
  const config = _getConfig();
  config.nb_of_meetings_per_team = nb_of_meetings_per_team;
  _setConfig(config);
}

export function setPhase(phase: TournamentPhase): void {
  const config = _getConfig();
  config.current_phase = phase;
  _setConfig(config);
}

export function reset(): void {
  _setConfig(DEFAULT_CONFIG);
}
