import { isValidBadmintonScore } from "../core/functions/utils";
import { Field, Meeting } from "../core/types";
import { _getMeetings, _setMeetings } from "./localstorage";

// const default_meetings: Meeting[] = [];
// const default_meetings: Meeting[] = [
//   {
//     id: 2,
//     team1_id: 1,
//     team2_id: 11,
//     started_at: new Date("2024-06-20T12:48:17.736Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 1,
//     team1_doubles_score: 21,
//     team2_singles_score: 21,
//     team2_doubles_score: 9,
//     ended_at: new Date("2024-06-20T12:48:37.406Z"),
//   },
//   {
//     id: 3,
//     team1_id: 2,
//     team2_id: 12,
//     started_at: new Date("2024-06-20T12:48:19.385Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 6,
//     team1_doubles_score: 16,
//     team2_singles_score: 21,
//     team2_doubles_score: 21,
//     ended_at: new Date("2024-06-20T12:48:41.225Z"),
//   },
//   {
//     id: 4,
//     team1_id: 3,
//     team2_id: 13,
//     started_at: new Date("2024-06-20T12:48:21.048Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 20,
//     team1_doubles_score: 21,
//     team2_singles_score: 22,
//     team2_doubles_score: 9,
//     ended_at: new Date("2024-06-20T12:48:49.278Z"),
//   },
//   {
//     id: 5,
//     team1_id: 4,
//     team2_id: 14,
//     started_at: new Date("2024-06-20T12:48:23.169Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 4,
//     team1_doubles_score: 21,
//     team2_singles_score: 21,
//     team2_doubles_score: 4,
//     ended_at: new Date("2024-06-20T12:49:03.663Z"),
//   },
//   {
//     id: 6,
//     team1_id: 5,
//     team2_id: 15,
//     started_at: new Date("2024-06-20T12:48:25.124Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 4,
//     team1_doubles_score: 21,
//     team2_singles_score: 21,
//     team2_doubles_score: 3,
//     ended_at: new Date("2024-06-20T12:48:58.327Z"),
//   },
//   {
//     id: 7,
//     team1_id: 6,
//     team2_id: 16,
//     started_at: new Date("2024-06-20T12:48:27.320Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 21,
//     team1_doubles_score: 17,
//     team2_singles_score: 8,
//     team2_doubles_score: 21,
//     ended_at: new Date("2024-06-20T12:48:53.326Z"),
//   },
//   {
//     id: 8,
//     team1_id: 7,
//     team2_id: 17,
//     started_at: new Date("2024-06-20T12:48:29.625Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 1,
//     team1_doubles_score: 0,
//     team2_singles_score: 21,
//     team2_doubles_score: 21,
//     ended_at: new Date("2024-06-20T12:49:07.486Z"),
//   },
//   {
//     id: 9,
//     team1_id: 8,
//     team2_id: 18,
//     started_at: new Date("2024-06-20T12:49:10.422Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 6,
//     team1_doubles_score: 5,
//     team2_singles_score: 21,
//     team2_doubles_score: 21,
//     ended_at: new Date("2024-06-20T12:49:19.128Z"),
//   },
//   {
//     id: 10,
//     team1_id: 9,
//     team2_id: 19,
//     started_at: new Date("2024-06-20T12:49:11.983Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 19,
//     team1_doubles_score: 20,
//     team2_singles_score: 21,
//     team2_doubles_score: 22,
//     ended_at: new Date("2024-06-20T12:49:22.974Z"),
//   },
//   {
//     id: 11,
//     team1_id: 10,
//     team2_id: 20,
//     started_at: new Date("2024-06-20T12:49:14.375Z"),
//     ranking_turn_nb: 1,
//     team1_singles_score: 12,
//     team1_doubles_score: 24,
//     team2_singles_score: 21,
//     team2_doubles_score: 22,
//     ended_at: new Date("2024-06-20T12:49:28.679Z"),
//   },
// ];

// let meetings: Meeting[] = [...default_meetings];

function getNextId(): number {
  const max_id = _getMeetings().reduce(
    (current_max_id, meeting) => Math.max(current_max_id, meeting.id),
    1
  );
  return max_id + 1;
}

export function getAll(): Meeting[] {
  return _getMeetings();
}

export function get(id: number): Meeting {
  const meetings = _getMeetings();
  const opt_idx = meetings.findIndex((meeting) => meeting.id === id);
  if (opt_idx === -1) {
    throw Error("meetings.ts-get bad id");
  }
  return meetings[opt_idx];
}

export function setScores(
  id: number,
  team1_singles_score: number,
  team1_doubles_score: number,
  team2_singles_score: number,
  team2_doubles_score: number
): void {
  const meetings = _getMeetings();
  const are_valid_scores =
    isValidBadmintonScore(team1_singles_score, team2_singles_score) &&
    isValidBadmintonScore(team1_doubles_score, team2_doubles_score);
  const opt_idx = meetings.findIndex((meeting) => meeting.id === id);
  if (opt_idx === -1 || !are_valid_scores) {
    throw Error("meetings.ts-setScores bad id or invalid scores");
  }
  meetings[opt_idx].team1_singles_score = team1_singles_score;
  meetings[opt_idx].team1_doubles_score = team1_doubles_score;
  meetings[opt_idx].team2_singles_score = team2_singles_score;
  meetings[opt_idx].team2_doubles_score = team2_doubles_score;
  meetings[opt_idx].field = undefined;
  meetings[opt_idx].field_2 = undefined;
  meetings[opt_idx].ended_at = new Date().getTime();
  _setMeetings(meetings);
}

export function startNew(
  team1_id: number,
  team2_id: number | undefined,
  field: Field,
  field_2: Field | undefined,
  ranking_turn_nb: number | undefined
): void {
  const meetings = _getMeetings();
  const new_meeting: Meeting = {
    id: getNextId(),
    team1_id,
    team2_id,
    started_at: new Date().getTime(),
    field,
    field_2,
    ranking_turn_nb,
  };
  meetings.push(new_meeting);
  _setMeetings(meetings);
}

export function cancelMeeting(meetingId: number): void {
  const meetings = _getMeetings().filter((meeting) => meeting.id !== meetingId);
  _setMeetings(meetings);
}

export function reset(): void {
  _setMeetings([]);
}
