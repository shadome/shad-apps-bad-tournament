import { Meeting, Team, TournamentConfig } from "../core/types";
import { DEFAULT_CONFIG } from "./tournamentConfig";

export enum LocalStorageEvent {
  doSelectNbOfPools,
  doSelectNbOfMeetings,
  doValidateConfig,
  doResetTournament,
  doMoveTeam,
  doAddTeam,
  doDeleteTeam,
  doDeleteAllTeams,
  doCancelMeeting,
  doSetMeetingScore,
  doStartMeeting,
  doCancelNewTurn,
  doStartNewTurn,
  doStartPoolsPhase,
}

export interface LocalStorageItem {
  key: string;
  evt: LocalStorageEvent;
  evt_date: number;
  evt_description: string;
  meetings_state: Meeting[];
  teams_state: Team[];
  config_state: TournamentConfig;
}

let meetings_state: Meeting[] = [];
let teams_state: Team[] = [];
let config_state: TournamentConfig = DEFAULT_CONFIG;

// Functions prefixed with an _ must only be called in the Repository layer

export function _getMeetings(): Meeting[] {
  return [...meetings_state];
}

export function _setMeetings(meetings: Meeting[]): void {
  meetings_state = [...meetings];
}

export function _getTeams(): Team[] {
  return [...teams_state];
}

export function _setTeams(teams: Team[]): void {
  teams_state = [...teams];
}

export function _getConfig(): TournamentConfig {
  return { ...config_state };
}

export function _setConfig(config: TournamentConfig): void {
  config_state = { ...config };
}

export function persistState(
  evt: LocalStorageEvent,
  evt_description: string
): void {
  const new_evt_idx = window.localStorage.length;
  const key = `${new_evt_idx}`;
  const state: LocalStorageItem = {
    key,
    evt,
    evt_description,
    evt_date: new Date().getTime(),
    meetings_state,
    teams_state,
    config_state,
  };
  window.localStorage.setItem(key, JSON.stringify(state));
}

export function restoreLastState(): void {
  if (window.localStorage.length > 0) {
    const key = `${window.localStorage.length - 1}`;
    restoreStateFromKey(key);
  }
}

export function restoreStateFromKey(key: string): void {
  // try to fetch the state
  const jsonItem = window.localStorage.getItem(key);
  if (jsonItem === undefined || jsonItem === null) {
    console.error(
      `Invalid local storage key ('${key}'), aborting state restoration.`
    );
    return;
  }
  // inflate stateful variables from the fetched state
  const item: LocalStorageItem = JSON.parse(jsonItem!);
  meetings_state = item.meetings_state;
  teams_state = item.teams_state;
  config_state = item.config_state;
}

export function purgeStatesBeyondKey(key: string): void {
  // remove more recent states
  const idx = Number(key);
  for (let i = window.localStorage.length - 1; i > idx; i--) {
    window.localStorage.removeItem(`${i}`);
  }
}

export function getAllItems(): LocalStorageItem[] {
  let result: LocalStorageItem[] = [];
  // from most recent to most ancient
  for (let i = window.localStorage.length - 1; i >= 0; i--) {
    const jsonItem = window.localStorage.getItem(`${i}`)!;
    const item: LocalStorageItem = JSON.parse(jsonItem);
    result.push({ ...item });
  }
  return result;
}
