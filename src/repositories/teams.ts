import { Team } from "../core/types";
import { _getTeams, _setTeams } from "./localstorage";

// const TMP_MOCK_DATA: Team[] = Array(20)
//   .fill(undefined)
//   .map((_, idx) => ({
//     id: idx + 1,
//     estimated_ranking: idx + 1,
//     name: `Nom de l'équipe numéro ${idx + 1}`,
//     player1Name: "John Doe",
//     player2Name: "Jane Dae",
//     player3Name: "Charles Dupont",
//     player4Name: idx % 3 === 0 ? "Anne-Marie Delacour" : undefined,
//   }));

// const default_teams: Team[] = [];
// let teams: Team[] = [...TMP_MOCK_DATA];

function getNextId(): number {
  const teams = _getTeams();
  const max_id = teams.reduce(
    (current_max_id, team) => Math.max(current_max_id, team.id),
    1
  );
  return max_id + 1;
}

export function getAll(): Team[] {
  return _getTeams();
}

export function getById(id: number): Team {
  const teams = _getTeams();
  const opt_idx = teams.findIndex((team) => team.id === id);
  if (opt_idx === -1) {
    throw Error("teams.ts-getById id does not exist");
  }
  return teams[opt_idx];
}

export function addTeam(
  name: string,
  player1Name: string,
  player2Name: string,
  player3Name?: string,
  player4Name?: string
): void {
  const teams = _getTeams();
  const trimmed_name = name?.trim();
  if (trimmed_name === undefined || trimmed_name.length === 0) {
    throw Error("teams.ts-addTeam name should not be null nor trimmed empty");
  }
  const new_team: Team = {
    id: getNextId(),
    name: trimmed_name,
    estimated_ranking: teams.length + 1,
    player1Name: player1Name.trim(),
    player2Name: player2Name.trim(),
    player3Name: player3Name?.trim(),
    player4Name: player4Name?.trim(),
  };
  _setTeams([...teams, new_team]);
}

export function deleteTeam(id: number): void {
  const teams = _getTeams();
  // Note: O(n) or 3*O(n) is the same; the code is cleaner with `findIndex`
  const opt_idx = teams.findIndex((team) => team.id === id);
  if (opt_idx === -1) {
    throw Error("teams.ts-deleteTeam id does not exist");
  }
  const estimated_ranking_to_delete = teams[opt_idx].estimated_ranking;
  _setTeams(
    teams
      .filter((team) => team.id !== id)
      .map((team) => {
        if (team.estimated_ranking > estimated_ranking_to_delete) {
          team.estimated_ranking--;
        }
        return team;
      })
  );
}

export function invertRespectiveEstimatedRankings(
  team1_id: number,
  team2_id: number
): void {
  const teams = _getTeams();
  const opt_idx_1 = teams.findIndex((team) => team.id === team1_id);
  const opt_idx_2 = teams.findIndex((team) => team.id === team2_id);
  if (opt_idx_1 === opt_idx_2 || opt_idx_1 === -1 || opt_idx_2 === -1) {
    throw Error(
      "teams.ts-invertRespectiveEstimatedRankings same teams or id not found"
    );
  }
  const tmp_ranking = teams[opt_idx_1].estimated_ranking;
  teams[opt_idx_1].estimated_ranking = teams[opt_idx_2].estimated_ranking;
  teams[opt_idx_2].estimated_ranking = tmp_ranking;
  _setTeams(teams);
}

export function reset(): void {
  _setTeams([]);
}
