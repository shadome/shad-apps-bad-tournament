export const DEFAULT_TURN_NB = 1;

export enum Field {
  Field1 = "Field1",
  Field2 = "Field2",
  Field3 = "Field3",
  Field4 = "Field4",
  Field5 = "Field5",
  Field6 = "Field6",
  Field7 = "Field7",
}

export enum TurnType {
  Ended = "ended",
  Current = "current",
  Anticipation = "anticipation",
}

export enum TournamentPhase {
  // default is Setup
  Setup = "setup",
  Ranking = "ranking",
  Pools = "pools",
}

export interface Team {
  id: number;
  // trimmed, case-insensitive-unique
  name: string;
  // ranking estimation in setup phase:
  // set by the organisation staff so that an initial
  // ranking can be made before the ranking phase
  // unique, contiguous, 1-based
  estimated_ranking: number;
  player1Name: string;
  player2Name: string;
  player3Name?: string;
  player4Name?: string;
}

export interface TournamentConfig {
  current_phase: TournamentPhase;
  // 1+, default is 1
  // inv: set to 1 if phase 1
  // inv: set to the last turn if phase 3
  // gives the anticipation turn nb if revelant,
  // else the current turn number
  turn_nb: number;
  // from 1 to teams.length / 2
  // may be set later, must be set before phase 3
  nb_of_pools?: number;
  // how many meetings played for each team of every pool
  // may be set later, must be set before phase 3
  nb_of_meetings_per_team?: number;
}

export interface Meeting {
  id: number;
  // invariant: team1_id.value < team2_id.value
  team1_id: number;
  // note that team2_id is undefined if and only if
  // the number of teams is odd and one automatic win
  // per turn of the ranking phase must be registered
  team2_id?: number;
  // null if and only if it is an ongoing meeting
  team1_singles_score?: number;
  // null if and only if it is an ongoing meeting
  team2_singles_score?: number;
  // null if and only if it is an ongoing meeting
  team1_doubles_score?: number;
  // null if and only if it is an ongoing meeting
  team2_doubles_score?: number;
  // date in form of .getTime() ticks for JSON.parse compatibility
  started_at: number;
  // null if and only if it is an ongoing meeting
  // date in form of .getTime() ticks for JSON.parse compatibility
  ended_at?: number;
  // null if and only if it is NOT an ongoing meeting
  field?: Field;
  // null if it is NOT an ongoing meeting
  // for ongoing meetings, this value is optional, used for
  // meetings which require more than one field, which should
  // be an exception
  field_2?: Field;
  // null if it is not a phase 2 meeting
  // turn number of the meeting otherwise (1-based)
  ranking_turn_nb?: number;
}

// Represents how pools are stored in the database:
// a many-to-many relationship between team ids and
// pools. A pool is nothing more than a label, so
// the pool_key has a value of its own and isn't an
// identifier towards another more complex object.
// export interface PoolTeam {
//   team_id: number;
//   pool_id: number;
// }

export const BadmintonScorePointsValues = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23, 24, 25, 26, 27, 28, 29, 30,
] as const;

export type BadmintonScorePoints = (typeof BadmintonScorePointsValues)[number];
