import canMeetingScoreBeModified, { MeetingScoreModificationStatus } from ".";
import {
  Field,
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
} from "../../types";

const default_config: TournamentConfig = {
  current_phase: TournamentPhase.Ranking,
  nb_of_meetings_per_team: undefined,
  nb_of_pools: undefined,
  turn_nb: 2,
};

const default_all_teams: Team[] = [1, 2, 3, 4].map((nb) => ({
  id: nb,
  name: nb.toString(),
  estimated_ranking: nb,
  player1Name: `Player${nb}`,
  player2Name: `Player${nb}`,
  player3Name: `Player${nb}`,
  player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
}));

// teams 1 and 2 meet again, for convenience
const default_ongoing_meeting: Meeting = {
  id: 20,
  field: Field.Field1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting: Meeting = {
  id: 10,
  ranking_turn_nb: 1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_all_meetings: Meeting[] = [
  default_complete_meeting,
  {
    ...default_complete_meeting,
    id: 11,
    team1_id: 3,
    team2_id: 4,
  },
  default_ongoing_meeting,
];

test("Testing getTurnType - ranking phase, turn 1 is ongoing, turn 2 is anticipation", () => {
  const config = { ...default_config, turn_nb: 1 };
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  // meeting[0] is complete and the teams are already in a second meeting
  {
    const expected = MeetingScoreModificationStatus.CannotBeModified;
    const meeting = all_meetings[0];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[1] is complete but the teams are not in a second meeting and turn 1 is still "current"
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[1];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[2] is an anticipation meeting and is ongoing - no score has been input yet
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[2];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
});

test("Testing getTurnType - ranking phase, turn 1 ended, current turn is 2", () => {
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  // meeting[0] is complete in an ended turn and the teams are already in a second meeting
  {
    const expected = MeetingScoreModificationStatus.CannotBeModified;
    const meeting = all_meetings[0];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[1] is complete but the teams are not in a second meeting but turn 1 is now "ended"
  {
    const expected = MeetingScoreModificationStatus.CannotBeModified;
    const meeting = all_meetings[1];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[2] is a current meeting and is ongoing - no score has been input yet
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[2];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
});

test("Testing getTurnType - pools phase, all meetings can be modified, even two meetings for the same team", () => {
  const config: TournamentConfig = {
    ...default_config,
    current_phase: TournamentPhase.Pools,
    nb_of_meetings_per_team: 2,
    nb_of_pools: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [
    { ...default_all_meetings[0], ranking_turn_nb: undefined },
    { ...default_all_meetings[1], ranking_turn_nb: undefined },
    { ...default_all_meetings[2], ranking_turn_nb: undefined },
  ];
  // meeting[0] is complete and the teams are already in a second meeting
  // but this is pools phase, so every meeting can be modified
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[0];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[1] is complete but the teams are not in a second meeting
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[1];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
  // meeting[2] is ongoing - no score has been input yet
  {
    const expected = MeetingScoreModificationStatus.CanBeModified;
    const meeting = all_meetings[2];
    const actual = canMeetingScoreBeModified(
      config,
      meeting,
      all_teams,
      all_meetings
    );
    expect(actual).toEqual(expected);
  }
});
