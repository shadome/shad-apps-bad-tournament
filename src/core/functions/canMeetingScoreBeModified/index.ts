import {
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
  TurnType,
} from "../../types";
import getTurnType from "../getTurnType";

export enum MeetingScoreModificationStatus {
  CanBeModified = "can_be_modified",
  NotApplicable = "not_applicable",
  CannotBeModified = "cannot_be_modified",
}

export default function canMeetingScoreBeModified(
  config: TournamentConfig,
  meeting: Meeting,
  all_teams: Team[],
  all_meetings: Meeting[]
): MeetingScoreModificationStatus {
  const is_pools_phase = config.current_phase === TournamentPhase.Pools;
  const is_ranking_phase = config.current_phase === TournamentPhase.Ranking;
  const meeting_turn_type =
    meeting.ranking_turn_nb !== undefined
      ? getTurnType(config, all_teams, all_meetings, meeting.ranking_turn_nb)
      : undefined;
  return is_pools_phase
    ? MeetingScoreModificationStatus.CanBeModified
    : !is_ranking_phase && !is_pools_phase
    ? MeetingScoreModificationStatus.NotApplicable
    : // at this point, it is the ranking phase, and an invariant tells us the meeting has a defined ranking_turn_nb
    meeting_turn_type === TurnType.Ended
    ? MeetingScoreModificationStatus.CannotBeModified
    : meeting_turn_type === TurnType.Anticipation
    ? MeetingScoreModificationStatus.CanBeModified
    : meeting.team2_id === undefined
    ? MeetingScoreModificationStatus.CanBeModified
    : isThereMoreRecentMeetingsForTeams(
        all_meetings,
        meeting.team1_id,
        meeting.team2_id!,
        meeting.ranking_turn_nb!
      )
    ? MeetingScoreModificationStatus.CannotBeModified
    : MeetingScoreModificationStatus.CanBeModified;
}

function isThereMoreRecentMeetingsForTeams(
  all_meetings: Meeting[],
  team_id_1: number,
  team_id_2: number,
  turn_nb: number
): boolean {
  const all_meetings_with_team_1 = all_meetings.filter(
    (m) => m.team1_id === team_id_1 || m.team2_id === team_id_1
  );
  const all_meetings_with_team_2 = all_meetings.filter(
    (m) => m.team1_id === team_id_2 || m.team2_id === team_id_2
  );
  const more_recent_meetings = all_meetings_with_team_1
    .concat(all_meetings_with_team_2)
    .filter(
      (m) => m.ranking_turn_nb !== undefined && m.ranking_turn_nb > turn_nb
    );
  return more_recent_meetings.length > 0;
}
