// Note: meetings should enforce the invariant `Meeting.team1_id < Meeting.team2_id`
// so this interface enforces the same invariant
class Edge {
  readonly team1_id: number;
  readonly team2_id: number;

  constructor(team_id_1: number, team_id_2: number) {
    this.team1_id = Math.min(team_id_1, team_id_2);
    this.team2_id = Math.max(team_id_1, team_id_2);
  }
}

interface EdgeCounters {
  edge: Edge;
  v1_nb_apparitions: number;
  v1_last_iteration: number;
  v2_nb_apparitions: number;
  v2_last_iteration: number;
}

function sortEdgeCountersList(
  mut_edge_counters_list: EdgeCounters[]
): void {
  function getWeakestVertexNbs(e: EdgeCounters): [number, number] {
    return e.v1_nb_apparitions < e.v2_nb_apparitions || (e.v1_nb_apparitions == e.v2_nb_apparitions && e.v1_last_iteration < e.v2_last_iteration)
      ? [e.v1_nb_apparitions, e.v1_last_iteration]
      : [e.v2_nb_apparitions, e.v2_last_iteration];
  }
  function getStrongestVertexNbs(e: EdgeCounters): [number, number] {
    return e.v1_nb_apparitions > e.v2_nb_apparitions || (e.v1_nb_apparitions == e.v2_nb_apparitions && e.v1_last_iteration > e.v2_last_iteration)
      ? [e.v1_nb_apparitions, e.v1_last_iteration]
      : [e.v2_nb_apparitions, e.v2_last_iteration];
  }
  mut_edge_counters_list
    .sort((a, b) => getWeakestVertexNbs(a)[1] - getWeakestVertexNbs(b)[1])
    .sort((a, b) => getWeakestVertexNbs(a)[0] - getWeakestVertexNbs(b)[0])
    .sort((a, b) => getStrongestVertexNbs(a)[1] - getStrongestVertexNbs(b)[1])
    .sort((a, b) => getStrongestVertexNbs(a)[0] - getStrongestVertexNbs(b)[0]);
}

/// Get a schedule for a given nb of one-way meetings between the given teams while maximising the distance between meetings for a same team
/// If the provided nb_of_meetings is greater than the given team array's length, the function acts as if nb_of_meetings = teams.len - 1
/// Required input invariant: team_ids.length() * nb_of_meetings % 2 === 0
export default function getPoolMeetingSchedule(
  team_ids: number[],
  nb_of_meetings: number
): { team1_id: number; team2_id: number }[] {
  
  if (nb_of_meetings < 1 || (team_ids ?? []).length < 2) {
    return [];
  }

  /*
   Inner function conception note:
   As of now, the best way of thinking of this function
   is to see the problem as a graph, cf. excalidraw file. Each vertex is a
   team_id and each edge is a meeting between two teams. Ordering the edges
   allows to order the meetings, from 1 to `nb_of_meetings*teams.length`.
   This function must order edges so that for every edge, its position
   in the ordered list is as far away as possible to the closest other
   edge linking one of the current edge's vertices. 
  */

  const mut_unsorted_edges: Edge[] = [];
  const vertices = [...team_ids];
  for (let i = 0; i < vertices.length; i++) {
    const vertex = vertices[i];
    for (let j = 0; j < Math.floor(nb_of_meetings / 2); j++) {
      const other_vertex_idx = (i + 1 + j) % vertices.length;
      const other_vertex = vertices[other_vertex_idx];
      mut_unsorted_edges.push(new Edge(vertex, other_vertex));
    }
  }
  if (nb_of_meetings % 2 === 1) {
    // inv: vertices.length is an even number in this case
    for (let i = 0; i < vertices.length / 2; i++) {
      const vertex = vertices[i];
      const other_vertex_idx = i + vertices.length / 2;
      const other_vertex = vertices[other_vertex_idx];
      mut_unsorted_edges.push(new Edge(vertex, other_vertex));
    }
  }

  const mut_edge_counters: EdgeCounters[] = mut_unsorted_edges.map((e) => ({
    edge: e,
    v1_nb_apparitions: 0,
    v1_last_iteration: 0,
    v2_nb_apparitions: 0,
    v2_last_iteration: 0,
  }));
  for (let i = 0; i < mut_edge_counters.length; i++) {
    sortEdgeCountersList(mut_edge_counters);
    const current_edge = mut_edge_counters[i];
    for (let j = i + 1; j < mut_edge_counters.length; j++) {
      const ref_edge_to_update = mut_edge_counters[j];
      for (const current_vertex of [current_edge.edge.team1_id, current_edge.edge.team2_id]) {
        if (ref_edge_to_update.edge.team1_id === current_vertex) {
          ref_edge_to_update.v1_last_iteration = i;
          ref_edge_to_update.v1_nb_apparitions++;
        }
        if (ref_edge_to_update.edge.team2_id === current_vertex) {
          ref_edge_to_update.v2_last_iteration = i;
          ref_edge_to_update.v2_nb_apparitions++;
        }
      }
    }
  }

  const result = mut_edge_counters.map(ec => ec.edge);
  return result;
}
