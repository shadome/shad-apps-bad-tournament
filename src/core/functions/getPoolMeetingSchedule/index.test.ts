import getPoolMeetingSchedule from ".";

test("Testing getPoolMeetingSchedule - k = 5, n = 4", () => {
  const k = 5;
  const n = 4;
  const team_ids = [...Array(k).keys()];
  const expected = [
    { team1_id: 0, team2_id: 1 },
    { team1_id: 2, team2_id: 3 },
    { team1_id: 0, team2_id: 4 },
    { team1_id: 1, team2_id: 2 },
    { team1_id: 3, team2_id: 4 },
    { team1_id: 0, team2_id: 2 },
    { team1_id: 1, team2_id: 3 },
    { team1_id: 2, team2_id: 4 },
    { team1_id: 0, team2_id: 3 },
    { team1_id: 1, team2_id: 4 },
  ];
  const actual = getPoolMeetingSchedule(team_ids, n);
  expect(actual).toEqual(expected);
});

test("Testing getPoolMeetingSchedule - k = 5, n = 2", () => {
  const k = 5;
  const n = 2;
  const team_ids = [...Array(k).keys()];
  const expected = [
    { team1_id: 0, team2_id: 1 },
    { team1_id: 2, team2_id: 3 },
    { team1_id: 0, team2_id: 4 },
    { team1_id: 1, team2_id: 2 },
    { team1_id: 3, team2_id: 4 },
  ];

  const actual = getPoolMeetingSchedule(team_ids, n);
  expect(actual).toEqual(expected);
});

test("Testing getPoolMeetingSchedule - k = 6, n = 5", () => {
  const k = 6;
  const n = 5;
  const team_ids = [...Array(k).keys()];
  const expected = [
    { team1_id: 0, team2_id: 1 },
    { team1_id: 2, team2_id: 3 },
    { team1_id: 4, team2_id: 5 },
    { team1_id: 0, team2_id: 2 },
    { team1_id: 1, team2_id: 3 },
    { team1_id: 0, team2_id: 4 },
    { team1_id: 2, team2_id: 5 },
    { team1_id: 1, team2_id: 4 },
    { team1_id: 3, team2_id: 5 },
    { team1_id: 1, team2_id: 2 },
    { team1_id: 0, team2_id: 3 },
    { team1_id: 2, team2_id: 4 },
    { team1_id: 1, team2_id: 5 },
    { team1_id: 3, team2_id: 4 },
    { team1_id: 0, team2_id: 5 },
  ];
  const actual = getPoolMeetingSchedule(team_ids, n);
  expect(actual).toEqual(expected);
});

test("Testing getPoolMeetingSchedule - k = 6, n = 3", () => {
  const k = 6;
  const n = 3;
  const team_ids = [...Array(k).keys()];
  const expected = [
    { team1_id: 0, team2_id: 1 },
    { team1_id: 2, team2_id: 3 },
    { team1_id: 4, team2_id: 5 },
    { team1_id: 1, team2_id: 2 },
    { team1_id: 0, team2_id: 3 },
    { team1_id: 1, team2_id: 4 },
    { team1_id: 2, team2_id: 5 },
    { team1_id: 3, team2_id: 4 },
    { team1_id: 0, team2_id: 5 },
  ];
  const actual = getPoolMeetingSchedule(team_ids, n);
  expect(actual).toEqual(expected);
});

test("Testing getPoolMeetingSchedule - k = 6, n = 2", () => {
  const k = 6;
  const n = 2;
  const team_ids = [...Array(k).keys()];
  const expected = [
    { team1_id: 0, team2_id: 1 },
    { team1_id: 2, team2_id: 3 },
    { team1_id: 4, team2_id: 5 },
    { team1_id: 1, team2_id: 2 },
    { team1_id: 0, team2_id: 5 },
    { team1_id: 3, team2_id: 4 },
  ];
  const actual = getPoolMeetingSchedule(team_ids, n);
  expect(actual).toEqual(expected);
});
