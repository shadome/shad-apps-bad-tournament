import getPoolsFromRankedTeam from ".";
import { Team } from "../../types";

const default_all_teams: Team[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(
  (nb) => ({
    id: nb,
    name: nb.toString(),
    estimated_ranking: nb,
    player1Name: `Player${nb}`,
    player2Name: `Player${nb}`,
    player3Name: `Player${nb}`,
    player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
  })
);

test("Testing getPoolsFromRankedTeam - nb_of_pools is multiple of teams.length", () => {
  // take the first 8 elements
  const ranked_teams = default_all_teams.filter((_, i) => i < 8);
  const nb_of_pools = 4;
  const expected = [
    {
      pool_id: 1,
      teams: [default_all_teams[0], default_all_teams[1]],
    },
    {
      pool_id: 2,
      teams: [default_all_teams[2], default_all_teams[3]],
    },
    {
      pool_id: 3,
      teams: [default_all_teams[4], default_all_teams[5]],
    },
    {
      pool_id: 4,
      teams: [default_all_teams[6], default_all_teams[7]],
    },
  ];
  const actual = getPoolsFromRankedTeam(ranked_teams, nb_of_pools);
  expect(actual).toEqual(expected);
});

test("Testing getPoolsFromRankedTeam - nb_of_pools is not multiple of teams.length: remainder is 1", () => {
  // take the first 9 elements
  const ranked_teams = default_all_teams.filter((_, i) => i < 9);
  const nb_of_pools = 4;
  const expected = [
    {
      pool_id: 1,
      teams: [default_all_teams[0], default_all_teams[1]],
    },
    {
      pool_id: 2,
      teams: [default_all_teams[2], default_all_teams[3]],
    },
    {
      pool_id: 3,
      teams: [default_all_teams[4], default_all_teams[5]],
    },
    {
      pool_id: 4,
      teams: [default_all_teams[6], default_all_teams[7], default_all_teams[8]],
    },
  ];
  const actual = getPoolsFromRankedTeam(ranked_teams, nb_of_pools);
  expect(actual).toEqual(expected);
});

test("Testing getPoolsFromRankedTeam - nb_of_pools is not multiple of teams.length: remainder is 2", () => {
  // take the first 10 elements
  const ranked_teams = default_all_teams.filter((_, i) => i < 10);
  const nb_of_pools = 4;
  const expected = [
    {
      pool_id: 1,
      teams: [default_all_teams[0], default_all_teams[1]],
    },
    {
      pool_id: 2,
      teams: [default_all_teams[2], default_all_teams[3]],
    },
    {
      pool_id: 3,
      teams: [default_all_teams[4], default_all_teams[5], default_all_teams[6]],
    },
    {
      pool_id: 4,
      teams: [default_all_teams[7], default_all_teams[8], default_all_teams[9]],
    },
  ];
  const actual = getPoolsFromRankedTeam(ranked_teams, nb_of_pools);
  expect(actual).toEqual(expected);
});

test("Testing getPoolsFromRankedTeam - nb_of_pools is not multiple of teams.length: remainder is 3 (max)", () => {
  // take the first 10 elements
  const ranked_teams = default_all_teams.filter((_, i) => i < 11);
  const nb_of_pools = 4;
  const expected = [
    {
      pool_id: 1,
      teams: [default_all_teams[0], default_all_teams[1]],
    },
    {
      pool_id: 2,
      teams: [default_all_teams[2], default_all_teams[3], default_all_teams[4]],
    },
    {
      pool_id: 3,
      teams: [default_all_teams[5], default_all_teams[6], default_all_teams[7]],
    },
    {
      pool_id: 4,
      teams: [
        default_all_teams[8],
        default_all_teams[9],
        default_all_teams[10],
      ],
    },
  ];
  const actual = getPoolsFromRankedTeam(ranked_teams, nb_of_pools);
  expect(actual).toEqual(expected);
});
