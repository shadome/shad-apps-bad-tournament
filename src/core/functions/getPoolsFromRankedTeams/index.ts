import { Team } from "../../types";

interface Pool {
  pool_id: number;
  teams: Team[];
}

export default function getPoolsFromRankedTeam(
  ranked_teams: Team[],
  nb_of_pools: number
): Pool[] {
  const integer_part = Math.floor(ranked_teams.length / nb_of_pools);
  const modulus = ranked_teams.length % nb_of_pools;

  const one_more_element_starting_at_idx = nb_of_pools - modulus;
  const mut_ranked_teams = [...ranked_teams];
  const result: Pool[] = [];
  for (let i = 0; i < nb_of_pools; i++) {
    const one_more_element = one_more_element_starting_at_idx <= i;
    const nb_elements = integer_part + Number(one_more_element);
    const pool_teams = mut_ranked_teams.splice(0, nb_elements);
    result.push({
      pool_id: i + 1,
      teams: pool_teams,
    });
  }

  return result;
}
