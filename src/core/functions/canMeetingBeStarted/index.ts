import { Meeting } from "../../types";
import { isMeetingComplete } from "../utils";

export interface MeetingToStart {
  team1_id: number;
  team2_id?: number;
}

/*
  Checks that a meeting between the given teams can be started, i.e., the teams are not already in a meeting.
  Does not check whether the meeting is relevant.
*/
export default function canMeetingBeStarted(
  all_meetings: Meeting[],
  meeting: MeetingToStart
): boolean {
  const ongoing_meetings_conflicts = all_meetings
    .filter((ongoing_meeting) => !isMeetingComplete(ongoing_meeting))
    .filter((ongoing_meeting) => haveTeamsInCommon(ongoing_meeting, meeting));

  return ongoing_meetings_conflicts.length === 0;
}

function haveTeamsInCommon(m1: Meeting, m2: MeetingToStart): boolean {
  /*
    Note that technically, there should never be an ongoing meeting with an undefined team2_id value.
    Undefined values are used to store meetings for automatic wins or losses which have no duration
    and are never ongoing.
    In any case, for safety, this function will never return true because both meetings' team2_id are undefined.
  */
  return (
    m1.team1_id === m2.team1_id ||
    m1.team2_id === m2.team1_id ||
    m1.team1_id === m2.team2_id ||
    (m1.team2_id === m2.team2_id && m1.team2_id !== undefined)
  );
}
