import canMeetingBeStarted from ".";
import { Field, Meeting } from "../../types";

const default_complete_meeting: Meeting = {
  id: 1,
  ranking_turn_nb: 1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_ongoing_meeting: Meeting = {
  id: 2,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: undefined,
  field: Field.Field1,
  team1_id: 1,
  team2_id: 2,
};

test("Testing canMeetingBeStarted - default", () => {
  const meeting = { team1_id: 2, team2_id: 3 };
  const all_meetings: Meeting[] = [];
  const expected = true;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - one team id is in a complete meeting", () => {
  const meeting = { team1_id: 2, team2_id: 3 };
  const all_meetings: Meeting[] = [default_complete_meeting];
  const expected = true;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - one team id is in an ongoing meeting", () => {
  const meeting = { team1_id: 2, team2_id: 3 };
  const all_meetings: Meeting[] = [default_ongoing_meeting];
  const expected = false;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - one team id is in a complete and an ongoing meeting", () => {
  const meeting = { team1_id: 2, team2_id: 3 };
  const all_meetings: Meeting[] = [
    default_complete_meeting,
    default_ongoing_meeting,
  ];
  const expected = false;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - there are ongoing meetings, but the new meeting teams are not in an ongoing meeting", () => {
  const meeting = { team1_id: 3, team2_id: 4 };
  const all_meetings: Meeting[] = [default_ongoing_meeting];
  const expected = true;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - first team id is in an ongoing meeting, second team id is undefined", () => {
  const meeting = { team1_id: 1, team2_id: undefined };
  const all_meetings: Meeting[] = [default_ongoing_meeting];
  const expected = false;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - there are ongoing meetings with undefined team2 id, but the new meeting teams are not in an ongoing meeting", () => {
  const meeting = { team1_id: 2, team2_id: 3 };
  const all_meetings: Meeting[] = [
    { ...default_ongoing_meeting, team2_id: undefined },
  ];
  const expected = true;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});

test("Testing canMeetingBeStarted - undefined in an ongoing meeting and undefined in a new meeting don't match each other", () => {
  const meeting = { team1_id: 2, team2_id: undefined };
  const all_meetings: Meeting[] = [
    { ...default_ongoing_meeting, team2_id: undefined },
  ];
  const expected = true;
  const actual = canMeetingBeStarted(all_meetings, meeting);
  expect(actual).toEqual(expected);
});
