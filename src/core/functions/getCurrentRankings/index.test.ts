import getCurrentRankings from ".";
import { Field, Meeting, Team } from "../../types";

const default_all_teams: Team[] = [1, 2, 3, 4].map((nb) => ({
  id: nb,
  name: nb.toString(),
  estimated_ranking: nb,
  player1Name: `Player${nb}`,
  player2Name: `Player${nb}`,
  player3Name: `Player${nb}`,
  player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
}));

// teams 1 and 2 meet again, for convenience
const default_ongoing_meeting: Meeting = {
  id: 20,
  field: Field.Field1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting: Meeting = {
  id: 10,
  ranking_turn_nb: 1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_all_meetings: Meeting[] = [
  default_complete_meeting,
  {
    ...default_complete_meeting,
    id: 11,
    team1_id: 3,
    team2_id: 4,
    team1_doubles_score: 21,
    team1_singles_score: 9,
    team2_doubles_score: 15,
    team2_singles_score: 21,
  },
  default_ongoing_meeting,
];

test("Testing getCurrentRankings - testing scores are filled in", () => {
  const turn_nb = 1;
  // sorting meant to disrupt the initial order
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  const expected = [
    {
      team: default_all_teams[0],
      total_score: 178,
      total_offensive_malus: 22,
      total_defensive_bonus: 0,
      total_meetings_played: 1,
      total_matches_won: 2,
      total_matches_lost: 0,
    },
    {
      team: default_all_teams[3],
      total_score: 106,
      total_offensive_malus: 9,
      total_defensive_bonus: 15,
      total_meetings_played: 1,
      total_matches_won: 1,
      total_matches_lost: 1,
    },
    {
      team: default_all_teams[2],
      total_score: 94,
      total_offensive_malus: 15,
      total_defensive_bonus: 9,
      total_meetings_played: 1,
      total_matches_won: 1,
      total_matches_lost: 1,
    },
    {
      team: default_all_teams[1],
      total_score: 22,
      total_offensive_malus: 0,
      total_defensive_bonus: 22,
      total_meetings_played: 1,
      total_matches_won: 0,
      total_matches_lost: 2,
    },
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb);
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - turn 1, no meeting started or complete, testing estimated ranking ordering", () => {
  const turn_nb = 1;
  // sorting meant to disrupt the initial order
  const all_teams = [
    default_all_teams[1],
    default_all_teams[3],
    default_all_teams[0],
    default_all_teams[2],
  ];
  const all_meetings: Meeting[] = [];
  const expected = default_all_teams;
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - turn 1, one meeting complete, some teams not in a meeting, testing estimated ranking ordering after complete meetings ordering", () => {
  const turn_nb = 1;
  const all_meetings: Meeting[] = [
    {
      ...default_complete_meeting,
      team1_id: default_all_teams[3].id,
      team2_id: default_all_teams[2].id,
    },
  ];
  const expected = [
    default_all_teams[3],
    default_all_teams[2],
    default_all_teams[0],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(
    default_all_teams,
    all_meetings,
    turn_nb
  ).map((x) => x.team);
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - turn 1, all meetings complete for an odd number of teams, testing that the last team has the least impactful win, but still a win", () => {
  const turn_nb = 1;
  const all_meetings: Meeting[] = [
    default_complete_meeting,
    {
      ...default_complete_meeting,
      team1_id: default_all_teams[2].id,
      team1_singles_score: 29,
      team1_doubles_score: 30,
      team2_id: undefined,
      team2_singles_score: 30,
      team2_doubles_score: 28,
    },
  ];
  const all_teams = [
    default_all_teams[0],
    default_all_teams[1],
    default_all_teams[2],
  ];
  const expected = [
    default_all_teams[0],
    default_all_teams[2],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - ordering", () => {
  const turn_nb = 1;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  const expected = [
    default_all_teams[0],
    default_all_teams[3],
    default_all_teams[2],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - ordering with equality - override with default rankings", () => {
  // strict equality between team 3 and team 4, the ordering should follow the initial ranking estimation
  const turn_nb = 1;
  const all_teams = default_all_teams;
  const all_meetings = [
    default_complete_meeting,
    {
      ...default_complete_meeting,
      id: 11,
      team1_id: 3,
      team2_id: 4,
      team1_doubles_score: 21,
      team1_singles_score: 15,
      team2_doubles_score: 15,
      team2_singles_score: 21,
    },
  ];
  const expected = [
    default_all_teams[0],
    default_all_teams[2],
    default_all_teams[3],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - turns beyond turn_nb are ignored", () => {
  // adding turn 2 and 3 meetings so that the team number 2 is ranked last
  // at the end of the first turn but ranked first afterwards
  const turn_nb = 1;
  const all_teams = default_all_teams;
  const all_meetings = [
    ...default_all_meetings,
    {
      ...default_complete_meeting,
      id: 21,
      ranking_turn_nb: 2,
      team1_id: 1,
      team2_id: 2,
      team1_doubles_score: 1,
      team1_singles_score: 1,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
    {
      ...default_complete_meeting,
      id: 31,
      ranking_turn_nb: 3,
      team1_id: 1,
      team2_id: 3,
      team1_doubles_score: 1,
      team1_singles_score: 1,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
  ];
  const expected = [
    default_all_teams[0],
    default_all_teams[3],
    default_all_teams[2],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - in ranking phase, meetings without a turn_nb are ignored", () => {
  // adding meetings without a turn_nb so that the team number 2 is ranked last
  // at the end of the first turn but ranked first out of the ranking phase
  // - testing the ranking phase, turn 1
  const turn_nb = 1;
  const all_teams = default_all_teams;
  const all_meetings = [
    ...default_all_meetings,
    {
      ...default_complete_meeting,
      id: 21,
      ranking_turn_nb: undefined,
      team1_id: 1,
      team2_id: 2,
      team1_doubles_score: 1,
      team1_singles_score: 1,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
    {
      ...default_complete_meeting,
      id: 31,
      ranking_turn_nb: undefined,
      team1_id: 1,
      team2_id: 3,
      team1_doubles_score: 1,
      team1_singles_score: 1,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
  ];
  const expected = [
    default_all_teams[0],
    default_all_teams[3],
    default_all_teams[2],
    default_all_teams[1],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - turn_nb > 1 aggregates past turn rankings", () => {
  // adding turn 2 and 3 meetings so that the team number 2 is ranked last
  // at the end of the first turn but ranked first afterwards
  const turn_nb = 3;
  const all_teams = default_all_teams;
  const all_meetings = [
    ...default_all_meetings,
    {
      ...default_complete_meeting,
      id: 21,
      ranking_turn_nb: 2,
      team1_id: 1,
      team2_id: 2,
      team1_doubles_score: 0,
      team1_singles_score: 0,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
    {
      ...default_complete_meeting,
      id: 31,
      ranking_turn_nb: 3,
      team1_id: 2,
      team2_id: 3,
      team1_doubles_score: 21,
      team1_singles_score: 21,
      team2_doubles_score: 0,
      team2_singles_score: 0,
    },
  ];
  const expected = [
    default_all_teams[1],
    default_all_teams[0],
    default_all_teams[3],
    default_all_teams[2],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, turn_nb).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});

test("Testing getCurrentRankings - aggregation out of the ranking phase", () => {
  // adding meetings without a turn_nb so that the team number 2 is ranked last
  // at the end of the first turn but ranked first out of the ranking phase
  // - testing the pools phase: every meeting in the ranking phase is ignored,
  // so apart from team 2, every other team has not scored and is ranked
  // according to the last turn of the ranking phase
  const all_teams = default_all_teams;
  const all_meetings = [
    ...default_all_meetings,
    {
      ...default_complete_meeting,
      id: 21,
      ranking_turn_nb: undefined,
      team1_id: 1,
      team2_id: 2,
      team1_doubles_score: 0,
      team1_singles_score: 0,
      team2_doubles_score: 21,
      team2_singles_score: 21,
    },
    {
      ...default_complete_meeting,
      id: 31,
      ranking_turn_nb: undefined,
      team1_id: 2,
      team2_id: 3,
      team1_doubles_score: 21,
      team1_singles_score: 21,
      team2_doubles_score: 0,
      team2_singles_score: 0,
    },
  ];
  const expected = [
    default_all_teams[1],
    default_all_teams[0],
    default_all_teams[3],
    default_all_teams[2],
  ];
  const actual = getCurrentRankings(all_teams, all_meetings, undefined).map(
    (x) => x.team
  );
  expect(actual).toEqual(expected);
});
