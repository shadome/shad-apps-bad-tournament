import { Meeting, Team } from "../../types";
import getPointsForTeamForMeeting from "../getPointsForTeamForMeeting";

export interface TeamRankingAggregatedInfo {
  team: Team;
  total_score: number;
  total_offensive_malus: number;
  total_defensive_bonus: number;
  total_meetings_played: number;
  total_matches_won: number;
  total_matches_lost: number;
}

/* Results are sorted from best to least ranked. */
export default function getCurrentRankings(
  all_teams: Team[],
  all_meetings: Meeting[],
  opt_turn_nb: number | undefined
): TeamRankingAggregatedInfo[] {
  /* 
    Note that this function is typically called on every re-render
    and once for every turn (current, ancitipated, and all ended turns),
    so its asymptotic complexity needs to be optimised, despite the
    loss in readability.
  */
  if (opt_turn_nb === undefined) {
    // if there is no provided opt_turn_nb, this function should not be based on
    // the initial estimated rankings, but on the rankings after the last turn_nb

    // find the last turn_nb
    const last_turn_nb = all_meetings.reduce(
      (max_turn_nb, meeting) =>
        Math.max(
          max_turn_nb,
          meeting.ranking_turn_nb === undefined ? -1 : meeting.ranking_turn_nb!
        ),
      -1
    );
    if (last_turn_nb > -1) {
      // recursive call, which cannot loop by construction
      const rankings_for_ranking_phase = getCurrentRankings(
        all_teams,
        all_meetings,
        last_turn_nb!
      );
      all_teams = rankings_for_ranking_phase.map((r) => r.team);
    }
  } else {
    // else, it is just based on the initial ranking estimation
    all_teams = all_teams.sort(
      (a, b) => a.estimated_ranking - b.estimated_ranking
    );
  }
  const team_aggregated_ranking_infos: TeamRankingAggregatedInfo[] = Array.from(
    all_meetings
      .reduce(
        (mut_team_id_to_aggregated_ranking_infos, meeting) =>
          add_meeting_to_aggregated_team_ranking_infos(
            mut_team_id_to_aggregated_ranking_infos,
            meeting,
            opt_turn_nb
          ),
        initialise_team_id_to_aggregated_ranking_infos(all_teams)
      )
      .values()
  );
  // sort the previous structure by score, and follow the initial team ranking estimation
  // if two or more teams have the same score
  const team_id_to_order = new Map<number, number>(
    all_teams.map((team, idx) => [team.id, idx])
  );
  const sorted_team_infos = team_aggregated_ranking_infos
    // compareFn(a, b) return value	sort order
    // > 0	sort a after b, e.g. [b, a]
    // < 0	sort a before b, e.g. [a, b]
    .sort(
      (a, b) =>
        team_id_to_order.get(a.team.id)! - team_id_to_order.get(b.team.id)!
    )
    .sort((a, b) => b.total_score - a.total_score);
  return sorted_team_infos;
}

function initialise_team_id_to_aggregated_ranking_infos(
  all_teams: Team[]
): Map<number, TeamRankingAggregatedInfo> {
  const mut_result = new Map<number, TeamRankingAggregatedInfo>();
  for (const team of all_teams) {
    mut_result.set(team.id, {
      team: team,
      total_score: 0,
      total_offensive_malus: 0,
      total_defensive_bonus: 0,
      total_meetings_played: 0,
      total_matches_won: 0,
      total_matches_lost: 0,
    });
  }
  return mut_result;
}

function add_meeting_to_aggregated_team_ranking_infos(
  mut_team_id_to_aggregated_ranking_infos: Map<
    number,
    TeamRankingAggregatedInfo
  >,
  meeting: Meeting,
  opt_turn_nb: number | undefined
) {
  const is_meeting_relevant =
    (meeting.ranking_turn_nb === undefined && opt_turn_nb === undefined) ||
    (meeting.ranking_turn_nb !== undefined &&
      opt_turn_nb !== undefined &&
      meeting.ranking_turn_nb! <= opt_turn_nb!);
  if (!is_meeting_relevant) {
    return mut_team_id_to_aggregated_ranking_infos;
  }
  const team_ids =
    meeting.team2_id !== undefined
      ? [meeting.team1_id, meeting.team2_id]
      : [meeting.team1_id];
  for (const team_id of team_ids) {
    const current_aggregated_team_ranking_infos =
      mut_team_id_to_aggregated_ranking_infos.get(team_id)!;
    const opt_added_ranking_info = getPointsForTeamForMeeting(meeting, team_id);
    if (opt_added_ranking_info !== undefined) {
      mut_team_id_to_aggregated_ranking_infos.set(team_id, {
        team: current_aggregated_team_ranking_infos.team,
        total_score:
          current_aggregated_team_ranking_infos.total_score +
          opt_added_ranking_info!.score,
        total_offensive_malus:
          current_aggregated_team_ranking_infos.total_offensive_malus +
          opt_added_ranking_info!.offensive_malus,
        total_defensive_bonus:
          current_aggregated_team_ranking_infos.total_defensive_bonus +
          opt_added_ranking_info!.defensive_bonus,
        total_meetings_played:
          current_aggregated_team_ranking_infos.total_meetings_played + 1,
        total_matches_won:
          current_aggregated_team_ranking_infos.total_matches_won +
          opt_added_ranking_info!.nb_matches_won,
        total_matches_lost:
          current_aggregated_team_ranking_infos.total_matches_lost +
          2 -
          opt_added_ranking_info!.nb_matches_won,
      });
    }
  }
  return mut_team_id_to_aggregated_ranking_infos;
}
