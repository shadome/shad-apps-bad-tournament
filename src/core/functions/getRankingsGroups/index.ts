import { DEFAULT_TURN_NB, Meeting } from "../../types";
import {
  TeamRankingScore,
  TeamsForNewMeeting,
  makeMeetingsFromTeamSplit,
} from "../utils";

/*
  This function will give back teams split by group split for the given
  turn_nb which is the current turn or an ended turn (NOT anticipation).
  Required:
  - getTurnType(turn_nb) !== Anticipation
  - getTurnType(turn_nb - 1) === Ended || turn_nb === DEFAULT_TURN_NB
  - config.current_phase === TournamentPhase.Ranking
*/
export default function getRankingsGroups(
  turn_nb: number,
  all_meetings: Meeting[],
  all_teams_previous_rankings: TeamRankingScore[],
  group_split_counts: number[]
): (TeamsForNewMeeting | Meeting)[][] | undefined {
  const isTurnNbBelowMinimum = turn_nb < DEFAULT_TURN_NB;
  const lazyNbOfTeamsInSplitCounts = () =>
    group_split_counts.reduce((sum, cur) => sum + cur, 0);
  const lazyAreNbOfTeamsAndNbOfSplitCountsMatching = () =>
    lazyNbOfTeamsInSplitCounts() === all_teams_previous_rankings.length;
  if (isTurnNbBelowMinimum || !lazyAreNbOfTeamsAndNbOfSplitCountsMatching()) {
    return undefined;
  }

  // will be destroyed
  const mut_ranked_teams = [...all_teams_previous_rankings]
    // sorted descendingly by total score, the first elements has the highest score
    .sort((a, b) => b.total_score - a.total_score)
    .map((team_with_score) => team_with_score.team);

  const result = group_split_counts
    // group split counts to group splits
    .map((group_split_count) => mut_ranked_teams.splice(0, group_split_count))
    .map((group_split) =>
      makeMeetingsFromTeamSplit(all_meetings, group_split, turn_nb)
    );

  return result;
}
