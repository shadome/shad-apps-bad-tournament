import getRankingsGroups from ".";
import { Field, Meeting, Team } from "../../types";

const default_all_teams: Team[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(
  (nb) => ({
    id: nb,
    name: nb.toString(),
    estimated_ranking: nb,
    player1Name: `Player${nb}`,
    player2Name: `Player${nb}`,
    player3Name: `Player${nb}`,
    player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
  })
);

const default_complete_meeting_1: Meeting = {
  id: 1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting_2: Meeting = {
  id: 2,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 5,
  team2_singles_score: 4,
  team1_id: 3,
  team2_id: 4,
};
const default_complete_meeting_3: Meeting = {
  id: 3,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 3,
  team2_singles_score: 13,
  team1_id: 5,
  team2_id: 6,
};
const default_complete_meeting_4: Meeting = {
  id: 4,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 1,
  team2_singles_score: 1,
  team1_id: 7,
  team2_id: 8,
};

const default_ongoing_anticipated_meeting: Meeting = {
  id: 5,
  ranking_turn_nb: 3,
  started_at: new Date().getTime(),
  ended_at: undefined,
  field: Field.Field1,
  team1_id: 1,
  team2_id: 2,
};

const default_complete_anticipated_meeting: Meeting = {
  id: 6,
  ranking_turn_nb: 3,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 12,
  team1_id: 1,
  team2_id: 2,
};

test("Testing getRankingsGroups - two meetings already existing and one meeting to create for the current turn, ", () => {
  const turn_nb = 2;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[4],
      total_score: 320,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[2],
      total_score: 0,
    },
    {
      team: default_all_teams[3],
      total_score: 0,
    },
  ];
  const expected = [
    [default_complete_meeting_1],
    [default_complete_meeting_3],
    [
      {
        team1_id: default_all_teams[2].id,
        team2_id: default_all_teams[3].id,
        ranking_turn_nb: turn_nb,
      },
    ],
  ];
  const actual = getRankingsGroups(
    turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsGroups - two meetings already existing and one meeting to create for the current turn - other order, ", () => {
  const turn_nb = 2;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_2,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[4],
      total_score: 320,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[2],
      total_score: 0,
    },
    {
      team: default_all_teams[3],
      total_score: 0,
    },
  ];
  const expected = [
    [default_complete_meeting_1],
    [
      {
        team1_id: default_all_teams[4].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [default_complete_meeting_2],
  ];
  const actual = getRankingsGroups(
    turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsGroups - two meetings already existing and one meeting to create for the current turn - adding parasitic meetings, ", () => {
  const turn_nb = 2;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_2,
    default_complete_meeting_4,
    default_ongoing_anticipated_meeting,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[4],
      total_score: 320,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[2],
      total_score: 0,
    },
    {
      team: default_all_teams[3],
      total_score: 0,
    },
  ];
  const expected = [
    [default_complete_meeting_1],
    [
      {
        team1_id: default_all_teams[4].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [default_complete_meeting_2],
  ];
  const actual = getRankingsGroups(
    turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsGroups - no existing meetings - unordered team_rankings - split counts [2,2,4], ", () => {
  const turn_nb = 1;
  const all_meetings: Meeting[] = [];
  const group_split_counts = [2, 2, 4];
  const team_rankings = [
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[4],
      total_score: 320,
    },
    {
      team: default_all_teams[7],
      total_score: 1,
    },
    {
      team: default_all_teams[2],
      total_score: 4,
    },
    {
      team: default_all_teams[3],
      total_score: 3,
    },
    {
      team: default_all_teams[6],
      total_score: 2,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[1].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[4].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[2].id,
        team2_id: default_all_teams[6].id,
        ranking_turn_nb: turn_nb,
      },
      {
        team1_id: default_all_teams[3].id,
        team2_id: default_all_teams[7].id,
        ranking_turn_nb: turn_nb,
      },
    ],
  ];
  const actual = getRankingsGroups(
    turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsGroups - no existing meetings - unordered team_rankings - split counts [2,2,3]: odd last split count ", () => {
  const turn_nb = 1;
  const all_meetings: Meeting[] = [];
  const group_split_counts = [2, 2, 3];
  const team_rankings = [
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[4],
      total_score: 320,
    },
    {
      team: default_all_teams[2],
      total_score: 4,
    },
    {
      team: default_all_teams[3],
      total_score: 3,
    },
    {
      team: default_all_teams[6],
      total_score: 2,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[1].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[4].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[2].id,
        team2_id: default_all_teams[3].id,
        ranking_turn_nb: turn_nb,
      },
      {
        team1_id: default_all_teams[6].id,
        team2_id: undefined,
        ranking_turn_nb: turn_nb,
      },
    ],
  ];
  const actual = getRankingsGroups(
    turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});
