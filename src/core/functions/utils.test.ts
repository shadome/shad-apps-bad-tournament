import * as Utils from "./utils";

test("Testing isValidBadmintonScore", () => {
  [
    { exp: true, param1: 21, param2: 12 },
    { exp: true, param1: 21, param2: 0 },
    { exp: true, param1: 12, param2: 21 },
    { exp: true, param1: 22, param2: 20 },
    { exp: true, param1: 30, param2: 28 },
    { exp: true, param1: 30, param2: 29 },
    { exp: false, param1: -1, param2: 12 },
    { exp: false, param1: 12, param2: -1 },
    { exp: false, param1: 21, param2: 20 },
    { exp: false, param1: 21, param2: 21 },
    { exp: false, param1: 30, param2: 30 },
    { exp: false, param1: 22, param2: 12 },
    { exp: false, param1: 31, param2: 29 },
  ].forEach((x) => {
    expect(Utils.isValidBadmintonScore(x.param1, x.param2)).toStrictEqual(
      x.exp
    );
  });
});

test("Testing getNextRankingTurnGroupSplit", () => {
  [
    { exp: [1], teams_count: 1, turn_nb: 1 },
    { exp: [2], teams_count: 2, turn_nb: 1 },
    { exp: [4], teams_count: 4, turn_nb: 1 },
    { exp: [2, 2], teams_count: 4, turn_nb: 2 },
    { exp: [2, 4], teams_count: 6, turn_nb: 2 },
    { exp: [2, 3], teams_count: 5, turn_nb: 2 },
    { exp: [2, 4, 4], teams_count: 10, turn_nb: 3 },
    { exp: [2, 4, 3], teams_count: 9, turn_nb: 3 },
    { exp: [4, 4, 3], teams_count: 11, turn_nb: 3 },
    { exp: [4, 4, 6, 6], teams_count: 20, turn_nb: 4 },
    { exp: [4, 4, 6, 5], teams_count: 19, turn_nb: 4 },
  ].forEach((x) => {
    const actual = Utils.getRankingTurnGroupSplit(x.teams_count, x.turn_nb);
    expect(actual).toEqual(x.exp);
  });
});
