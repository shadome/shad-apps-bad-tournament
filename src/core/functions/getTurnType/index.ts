import {
  DEFAULT_TURN_NB,
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
  TurnType,
} from "../../types";
import { isMeetingComplete } from "../utils";

/*
  - if the current phase is not "ranking" => N/A 
  - if turn_nb < config.turn_nb
    - AND turn_nb+1 == config.turn_nb AND there are missing meetings
      or meetings waiting for score input for turn_nb => Current
    - ELSE => Ended
  - if turn_nb == config.turn_nb
    - AND there are missing meetings or meetings waiting for score
      input for turn_nb-1 => Anticipation
    - ELSE => Current
  - if turn_nb > config.turn_nb => N/A
*/
export default function getTurnType(
  config: TournamentConfig,
  all_teams: Team[],
  all_meetings: Meeting[],
  turn_nb: number
): TurnType | undefined {
  if (config.current_phase !== TournamentPhase.Ranking) {
    return undefined;
  } else if (config.turn_nb < turn_nb) {
    return undefined;
  } else if (turn_nb < DEFAULT_TURN_NB) {
    return undefined;
  } else if (turn_nb < config.turn_nb) {
    if (
      turn_nb + 1 === config.turn_nb &&
      !isTurnComplete(all_teams, all_meetings, turn_nb)
    ) {
      return TurnType.Current;
    } else {
      return TurnType.Ended;
    }
  } else if (turn_nb === config.turn_nb) {
    if (
      turn_nb > DEFAULT_TURN_NB &&
      !isTurnComplete(all_teams, all_meetings, turn_nb - 1)
    ) {
      return TurnType.Anticipation;
    } else {
      return TurnType.Current;
    }
  } else {
    return undefined;
  }
}

function isTurnComplete(
  all_teams: Team[],
  all_meetings: Meeting[],
  turn_nb: number
): boolean {
  // if the number of teams is odd, a meeting corresponding to an automatic win is also registered
  const expected_nb_of_meetings =
    (all_teams.length + (all_teams.length % 2)) / 2;
  return (
    getNbOfMeetingsCompleteForTurn(turn_nb, all_meetings) ===
    expected_nb_of_meetings
  );
}
function getNbOfMeetingsCompleteForTurn(
  turn_nb: number,
  all_meetings: Meeting[]
): number {
  const fIsMeetingRelevant = (m: Meeting) => m.ranking_turn_nb === turn_nb;
  const relevant_meetings = all_meetings
    .filter(fIsMeetingRelevant)
    .filter(isMeetingComplete);
  return relevant_meetings.length;
}
