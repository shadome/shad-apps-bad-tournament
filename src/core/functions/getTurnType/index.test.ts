import getTurnType from ".";
import {
  Field,
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
  TurnType,
} from "../../types";

const default_config: TournamentConfig = {
  current_phase: TournamentPhase.Ranking,
  nb_of_meetings_per_team: undefined,
  nb_of_pools: undefined,
  turn_nb: 2,
};

const default_all_teams: Team[] = [1, 2, 3, 4].map((nb) => ({
  id: nb,
  name: nb.toString(),
  estimated_ranking: nb,
  player1Name: `Player${nb}`,
  player2Name: `Player${nb}`,
  player3Name: `Player${nb}`,
  player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
}));

// teams 1 and 2 meet again, for convenience
const default_ongoing_meeting: Meeting = {
  id: 20,
  field: Field.Field1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting: Meeting = {
  id: 10,
  ranking_turn_nb: 1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_all_meetings: Meeting[] = [
  default_complete_meeting,
  {
    ...default_complete_meeting,
    id: 11,
    team1_id: 3,
    team2_id: 4,
  },
  default_ongoing_meeting,
];

test("Testing getTurnType - turn 1, no meeting started or complete", () => {
  const turn_nb = 1;
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [];
  const expected = TurnType.Current;
  const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
  expect(actual).toEqual(expected);
});

test("Testing getTurnType - turn 1 ended, current turn is 2", () => {
  const turn_nb = 1;
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  const expected = TurnType.Ended;
  const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
  expect(actual).toEqual(expected);
});

test("Testing getTurnType - turn 2 current", () => {
  const turn_nb = 2;
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  const expected = TurnType.Current;
  const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
  expect(actual).toEqual(expected);
});

test("Testing getTurnType - turn 2 anticipation - turn 1 has started uncomplete meetings", () => {
  // changing the turn 1's second meeting so that it is ongoing
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  all_meetings[1] = {
    ...all_meetings[1],
    ended_at: undefined,
    field: Field.Field3,
    team1_singles_score: undefined,
    team2_singles_score: undefined,
  };
  // test 1
  {
    const turn_nb = 1;
    const expected = TurnType.Current;
    const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
    expect(actual).toEqual(expected);
  }
  // test 2
  {
    const turn_nb = 2;
    const expected = TurnType.Anticipation;
    const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
    expect(actual).toEqual(expected);
  }
});

test("Testing getTurnType - turn 2 anticipation - turn 1 has meetings not even started yet", () => {
  // removing the meeting at index 1, which should be started for turn 1 (current)
  // even though a next-turn meeting has already been started for turn 2 (anticipation)
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[2]];
  // test 1
  {
    const turn_nb = 1;
    const expected = TurnType.Current;
    const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
    expect(actual).toEqual(expected);
  }
  // test 2
  {
    const turn_nb = 2;
    const expected = TurnType.Anticipation;
    const actual = getTurnType(config, all_teams, all_meetings, turn_nb);
    expect(actual).toEqual(expected);
  }
});
