import getPossibleNbOfMeetings from ".";

test("Testing getPossibleNbOfMeetings", () => {
  [
    { exp: [0, 1, 2, 3, 4, 5, 6, 7], param1: 16, param2: 2 },
    { exp: [0, 2, 4, 6], param1: 17, param2: 2 },
    { exp: [0, 2, 4, 6, 8], param1: 18, param2: 2 },
  ].forEach((x) => {
    expect(getPossibleNbOfMeetings(x.param1, x.param2)).toStrictEqual(x.exp);
  });
});
