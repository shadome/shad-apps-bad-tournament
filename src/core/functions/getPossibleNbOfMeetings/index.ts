// https://fr.wikipedia.org/wiki/Graphe_r%C3%A9gulier
// "Une condition nécessaire et suffisante pour l'existence d'un graphe k-régulier à n sommets est que n * k soit pair et que n ≥ k + 1"
export default function getPossibleNbOfMeetings(
  nb_of_teams: number,
  nb_of_pools: number
): Array<number> {
  // get the pool sizes, up to two if nbOfTeams *div* nbOfPools is not an integer
  const pool_size_float = nb_of_teams / nb_of_pools;
  const are_all_pools_same_size = Number.isInteger(pool_size_float);
  const pool_size_1 = are_all_pools_same_size
    ? pool_size_float
    : Math.floor(pool_size_float);
  let result = getPossibleNbOfMeetingsForPoolSize(pool_size_1);
  if (!are_all_pools_same_size) {
    const pool_size_2 = pool_size_1 + 1;
    const possible_values_for_pool_size_2 =
      getPossibleNbOfMeetingsForPoolSize(pool_size_2);
    result = intersect(result, possible_values_for_pool_size_2);
  }
  return result;
}

function getPossibleNbOfMeetingsForPoolSize(pool_size: number): Array<number> {
  return [...Array(pool_size).keys()].filter((x) => testK(x, pool_size));
}

// with k the tested number of edges in the graph and n the number of vertex
function testK(k: number, n: number): boolean {
  return n > k && (n * k) % 2 === 0;
}

function intersect(a: Array<number>, b: Array<number>): Array<number> {
  var set_a = new Set(a);
  var set_b = new Set(b);
  var intersection = new Set([...set_a].filter((x) => set_b.has(x)));
  return Array.from(intersection);
}
