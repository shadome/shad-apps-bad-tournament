import getMaxNbOfPools from ".";

test("Testing getMaxNbOfPools", () => {
  [
    { exp: 2, param: 5 },
    { exp: 2, param: 4 },
    { exp: 1, param: 3 },
    { exp: 4, param: 8 },
  ].forEach((x) => {
    expect(getMaxNbOfPools(x.param)).toStrictEqual(x.exp);
  });
});
