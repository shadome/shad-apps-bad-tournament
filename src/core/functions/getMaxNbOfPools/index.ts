export default function getMaxNbOfPools(nb_of_teams: number): number {
  return Math.floor(nb_of_teams / 2);
}
