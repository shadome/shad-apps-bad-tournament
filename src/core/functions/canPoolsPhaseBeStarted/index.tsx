/* From conception documentation:
  L'action "Lancer la phase de poules" n'est disponible que si les conditions suivantes sont remplies : 
  - la phase de classement est active
  - tous les scores du tour courant sont saisis et validés
  - "Ajouter un tour" n'a pas été utilisé pour lancer un tour anticipé / passer à un nouveau tour courant
  - l'action "Enregistrer la configuration de la phase de poules" a été activée au moins une fois depuis l'onglet de configuration du tournoi
  Cette action verrouille les autres actions disponibles sur la page "Phase 1 : Classement", sauf pour "Revenir à la phase de classement"
*/

import { Meeting, Team, TournamentConfig, TournamentPhase } from "../../types";
import { isMeetingComplete } from "../utils";

export default function canPoolsPhaseBeStarted(
  config: TournamentConfig,
  all_teams: Team[],
  all_meetings: Meeting[]
): boolean {
  const is_currently_phase_1 = config.current_phase === TournamentPhase.Ranking;
  const is_pools_phase_configured =
    config.nb_of_meetings_per_team !== undefined &&
    config.nb_of_pools !== undefined;
  // lambda function for laziness
  let _last_turn_nb: number | undefined = undefined;
  const l_last_turn_nb = () => {
    return (
      _last_turn_nb ??
      (_last_turn_nb = all_meetings.reduce(
        (max_turn_nb: number, m: Meeting) =>
          m.ranking_turn_nb !== undefined
            ? Math.max(m.ranking_turn_nb, max_turn_nb)
            : max_turn_nb,
        // initial value
        1
      ))
    );
  };
  // lambda function for laziness
  const f_is_last_turn_the_current_turn = () => {
    return l_last_turn_nb() === config.turn_nb;
  };
  const f_are_all_meetings_started_and_complete_for_last_turn = () => {
    // if the number of teams is odd, a meeting corresponding to an automatic win is also registered
    const expected_nb_of_meetings =
      (all_teams.length + (all_teams.length % 2)) / 2;
    const all_meetings_complete_for_last_turn = all_meetings
      // meetings for the last turn
      .filter((m) => m.ranking_turn_nb === l_last_turn_nb())
      // which are complete
      .filter(isMeetingComplete);
    return (
      all_meetings_complete_for_last_turn.length === expected_nb_of_meetings
    );
  };
  return (
    is_currently_phase_1 &&
    is_pools_phase_configured &&
    f_is_last_turn_the_current_turn() &&
    f_are_all_meetings_started_and_complete_for_last_turn()
  );
}
