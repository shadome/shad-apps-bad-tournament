import canPoolsPhaseBeStarted from ".";
import {
  Field,
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
} from "../../types";

const default_config: TournamentConfig = {
  current_phase: TournamentPhase.Ranking,
  nb_of_meetings_per_team: 2,
  nb_of_pools: 1,
  turn_nb: 2,
};

const default_all_teams: Team[] = [1, 2, 3, 4].map((nb) => ({
  id: nb,
  name: nb.toString(),
  estimated_ranking: nb,
  player1Name: `Player${nb}`,
  player2Name: `Player${nb}`,
  player3Name: `Player${nb}`,
  player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
}));

// teams 1 and 2 meet again, for convenience
const default_ongoing_meeting: Meeting = {
  id: 20,
  field: Field.Field1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting: Meeting = {
  id: 10,
  ranking_turn_nb: 1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_all_meetings: Meeting[] = [
  default_complete_meeting,
  {
    ...default_complete_meeting,
    id: 11,
    team1_id: 3,
    team2_id: 4,
  },
  default_ongoing_meeting,
];

test("Testing canRankingPhaseBeStarted - turn 1, no meeting started or complete", () => {
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - turn 1, one meeting started", () => {
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [
    { ...default_all_meetings[2], ranking_turn_nb: 1 },
  ];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - turn 1, one meeting complete and one started", () => {
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [
    default_all_meetings[1],
    { ...default_all_meetings[2], ranking_turn_nb: 1 },
  ];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - turn 1, one meeting complete and one not even started", () => {
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings: Meeting[] = [default_all_meetings[0]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - turn 1 ended, current turn is 2 with an ongoing meeting", () => {
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = default_all_meetings;
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - turn 1 ended, current turn is 2 without ongoing meetings", () => {
  const config = default_config;
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - current turn is 1, every meeting is complete", () => {
  const config = {
    ...default_config,
    turn_nb: 1,
  };
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = true;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - everything is ok but pools phase is not configured (nb_of_meetings_per_team)", () => {
  const config: TournamentConfig = {
    ...default_config,
    turn_nb: 1,
    nb_of_meetings_per_team: undefined,
  };
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - everything is ok but pools phase is not configured (nb_of_pools)", () => {
  const config: TournamentConfig = {
    ...default_config,
    turn_nb: 1,
    nb_of_pools: undefined,
  };
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - everything is ok but the current phase is Setup", () => {
  const config: TournamentConfig = {
    ...default_config,
    turn_nb: 1,
    current_phase: TournamentPhase.Setup,
  };
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});

test("Testing canRankingPhaseBeStarted - everything is ok but the current phase is alreay Pools", () => {
  const config: TournamentConfig = {
    ...default_config,
    turn_nb: 1,
    current_phase: TournamentPhase.Pools,
  };
  const all_teams = default_all_teams;
  const all_meetings = [default_all_meetings[0], default_all_meetings[1]];
  const expected = false;
  const actual = canPoolsPhaseBeStarted(config, all_teams, all_meetings);
  expect(actual).toEqual(expected);
});
