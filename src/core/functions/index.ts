import getPossibleNbOfMeetings from "./getPossibleNbOfMeetings";
import getMaxNbOfPools from "./getMaxNbOfPools";

export { getMaxNbOfPools, getPossibleNbOfMeetings };
