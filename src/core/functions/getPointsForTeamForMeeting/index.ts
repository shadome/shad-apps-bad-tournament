import * as Utils from "../utils";
import { Meeting } from "../../types";

interface TeamRankingInfo {
  team_id: number;
  nb_matches_won: 0 | 1 | 2;
  offensive_malus: number;
  defensive_bonus: number;
  score: number;
}

export default function getPointsForTeamForMeeting(
  meeting: Meeting,
  team_id: number
): TeamRankingInfo | undefined {
  const bad_team_id =
    meeting.team1_id !== team_id && meeting.team2_id !== team_id;
  const no_score =
    meeting.team1_singles_score === undefined ||
    meeting.team1_doubles_score === undefined ||
    meeting.team2_singles_score === undefined ||
    meeting.team2_doubles_score === undefined;
  // lazy so that the ! operator is safe
  const f_bad_scores = () =>
    !Utils.isValidBadmintonScore(
      meeting.team1_singles_score!,
      meeting.team2_singles_score!
    ) ||
    !Utils.isValidBadmintonScore(
      meeting.team1_doubles_score!,
      meeting.team2_doubles_score!
    );
  if (bad_team_id || no_score || f_bad_scores()) {
    return undefined;
  }
  const is_team_1 = meeting.team1_id === team_id;
  const team_scores = is_team_1
    ? {
        singles: meeting.team1_singles_score!,
        doubles: meeting.team1_doubles_score!,
      }
    : {
        singles: meeting.team2_singles_score!,
        doubles: meeting.team2_doubles_score!,
      };
  const opponent_scores = is_team_1
    ? {
        singles: meeting.team2_singles_score!,
        doubles: meeting.team2_doubles_score!,
      }
    : {
        singles: meeting.team1_singles_score!,
        doubles: meeting.team1_doubles_score!,
      };
  const has_won_singles = team_scores.singles > opponent_scores.singles;
  const has_won_doubles = team_scores.doubles > opponent_scores.doubles;
  const nb_of_games_won =
    has_won_singles && has_won_doubles
      ? 2
      : has_won_doubles || has_won_singles
      ? 1
      : 0;
  const games_won_score = 100 * nb_of_games_won;
  const offensive_malus =
    (has_won_singles ? opponent_scores.singles : 0) +
    (has_won_doubles ? opponent_scores.doubles : 0);
  const defensive_bonus =
    (has_won_singles ? 0 : team_scores.singles) +
    (has_won_doubles ? 0 : team_scores.doubles);
  const score = games_won_score - offensive_malus + defensive_bonus;

  return {
    team_id: team_id,
    defensive_bonus: defensive_bonus,
    nb_matches_won: nb_of_games_won,
    offensive_malus: offensive_malus,
    score: score,
  };
}
