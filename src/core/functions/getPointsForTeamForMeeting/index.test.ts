import getPointsForTeamForMeeting from ".";
import { Field } from "../../types";

const meeting_default_values = {
  id: 0,
  team1_id: 1,
  team2_id: 2,
  field: Field.Field1,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  ranking_turn_nb: 2,
};

test("Testing getPointsForTeamForMeeting - happy path", () => {
  [
    {
      exp: {
        team_id: meeting_default_values.team1_id,
        nb_matches_won: 2,
        score: 165,
        offensive_malus: 35,
        defensive_bonus: 0,
      },
      param1: {
        ...meeting_default_values,
        team1_singles_score: 21,
        team2_singles_score: 15,
        team1_doubles_score: 22,
        team2_doubles_score: 20,
      },
      param2: meeting_default_values.team1_id,
    },
    {
      exp: {
        team_id: meeting_default_values.team2_id,
        nb_matches_won: 0,
        score: 35,
        offensive_malus: 0,
        defensive_bonus: 35,
      },
      param1: {
        ...meeting_default_values,
        team1_singles_score: 21,
        team2_singles_score: 15,
        team1_doubles_score: 22,
        team2_doubles_score: 20,
      },
      param2: meeting_default_values.team2_id,
    },
  ].forEach((x) => {
    expect(getPointsForTeamForMeeting(x.param1, x.param2)).toEqual(x.exp);
  });
});

test("Testing getPointsForTeamForMeeting - equality", () => {
  const param1 = {
    ...meeting_default_values,
    team1_singles_score: 21,
    team2_singles_score: 12,
    team1_doubles_score: 12,
    team2_doubles_score: 21,
  };
  [
    {
      exp: {
        team_id: meeting_default_values.team1_id,
        nb_matches_won: 1,
        score: 100,
        offensive_malus: 12,
        defensive_bonus: 12,
      },
      param1,
      param2: meeting_default_values.team1_id,
    },
    {
      exp: {
        team_id: meeting_default_values.team2_id,
        nb_matches_won: 1,
        score: 100,
        offensive_malus: 12,
        defensive_bonus: 12,
      },
      param1,
      param2: meeting_default_values.team2_id,
    },
  ].forEach((x) => {
    expect(getPointsForTeamForMeeting(x.param1, x.param2)).toEqual(x.exp);
  });
});

test("Testing getPointsForTeamForMeeting - no score", () => {
  [
    {
      exp: undefined,
      param1: {
        ...meeting_default_values,
        team1_singles_score: 21,
        team2_singles_score: 15,
        team1_doubles_score: 22,
        team2_doubles_score: undefined,
      },
      param2: meeting_default_values.team1_id,
    },
  ].forEach((x) => {
    expect(getPointsForTeamForMeeting(x.param1, x.param2)).toEqual(x.exp);
  });
});

test("Testing getPointsForTeamForMeeting - bad team_id", () => {
  [
    {
      exp: undefined,
      param1: {
        ...meeting_default_values,
        team1_singles_score: 21,
        team2_singles_score: 15,
        team1_doubles_score: 21,
        team2_doubles_score: 12,
      },
      param2: 129048357348,
    },
  ].forEach((x) => {
    expect(getPointsForTeamForMeeting(x.param1, x.param2)).toEqual(x.exp);
  });
});

test("Testing getPointsForTeamForMeeting - bad score", () => {
  [
    {
      exp: undefined,
      param1: {
        ...meeting_default_values,
        team1_singles_score: 22,
        team2_singles_score: 15,
        team1_doubles_score: 21,
        team2_doubles_score: 12,
      },
      param2: meeting_default_values.team1_id,
    },
  ].forEach((x) => {
    expect(getPointsForTeamForMeeting(x.param1, x.param2)).toEqual(x.exp);
  });
});
