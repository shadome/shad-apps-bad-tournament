import {
  BadmintonScorePoints,
  DEFAULT_TURN_NB,
  Meeting,
  Team,
  TournamentConfig,
  TournamentPhase,
} from "../types";

export function toHhMm(date: Date): string {
  return date.toLocaleTimeString("fr-FR", {
    hour: "2-digit",
    minute: "2-digit",
  });
}

export function getPossibleSecondHalfOfBadmintonScore(
  score_half: BadmintonScorePoints
): BadmintonScorePoints[] {
  switch (score_half as BadmintonScorePoints) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
      return [21];
    case 20:
      return [22];
    case 22:
    case 23:
    case 24:
    case 25:
    case 26:
    case 27:
    case 28:
      return [
        (score_half - 2) as BadmintonScorePoints,
        (score_half + 2) as BadmintonScorePoints,
      ];
    case 29:
      return [27, 30];
    case 30:
      return [28, 29];
    case 21:
      return [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        23,
      ];
  }
}

export function isValidBadmintonScore(score1: number, score2: number): boolean {
  const higher_score = Math.max(score1, score2);
  const lower_score = Math.min(score1, score2);
  return (
    higher_score !== lower_score &&
    higher_score < 31 &&
    lower_score > -1 &&
    ((higher_score === 21 && lower_score < 20) ||
      higher_score - lower_score === 2 ||
      (higher_score === 30 && lower_score === 29))
  );
}

export function isMeetingComplete(meeting: Meeting) {
  return (
    meeting.field === undefined &&
    meeting.ended_at !== undefined &&
    meeting.team1_doubles_score !== undefined &&
    meeting.team1_singles_score !== undefined &&
    meeting.team2_doubles_score !== undefined &&
    meeting.team2_singles_score !== undefined
  );
}

/*
  During the ranking phase, get the threshold of groups for the given (typically upcoming) turn_nb.
  If teams_count is not a multiple of the number of groups expected for turn_nb, lower ranked teams
  will be put in a larger group.
  E.g., for teams.len = 16 and turn_nb = 3, return [4, 6, 6].
  Note that if the teams_count is odd, the last team would register an automatic win,
  so the group split should return an odd number for the last group (minus one).
  E.g., for teams.len = 16 and turn_nb = 3, return [4, 6, 5].
*/
export function getRankingTurnGroupSplit(
  teams_count: number,
  turn_nb: number
): number[] | undefined {
  if (teams_count < turn_nb || teams_count < 1 || turn_nb < 1) {
    return undefined;
  }
  /*
    Algorithm: for every two teams in rounded_team_count, loop through the array indexes and increment the value by 2.
    At the end, reverse the array to get bigger numbers trailing. If the given team number was odd, remove 1 from the
    last value of the array.
  */
  // it happens, conveniently, that the number of splits is equal to the turn_nb,
  // because turn numbers are 1-based indexes, but it could change in the future
  const nb_of_splits = turn_nb;
  // add one to the given teams_count if it is odd
  const rounded_teams_count = teams_count + (teams_count % 2);

  if (rounded_teams_count < nb_of_splits * 2) {
    // at least two teams per split are required
    return undefined;
  }

  const mut_result: number[] = Array(nb_of_splits).fill(0);

  for (let i = 0; i < rounded_teams_count / 2; i++) {
    const idx = i % nb_of_splits;
    mut_result[idx] += 2;
  }
  mut_result.reverse();

  if (teams_count % 2 === 1) {
    mut_result[mut_result.length - 1] -= 1;
  }
  return mut_result;
}

// core.functions internal use only
export interface TeamRankingScore {
  team: Team;
  total_score: number;
}

// core.functions internal use only
export interface TeamsForNewMeeting {
  team1_id: number;
  team2_id?: number;
  ranking_turn_nb: number;
}

// core.functions internal use only
/*
  Creates empty meetings or fetches existing meetings, if any, for the given team split.

  Note that this function will not create an empty meeting for the last team
  in split if the number of teams is odd. It will only try to fetch an existing
  meeting for it. An automatic win should be registered the instant the turn
  becomes "Current", which is out of this scope.
*/
export function makeMeetingsFromTeamSplit(
  all_meetings: Meeting[],
  teams_in_split: Team[],
  turn_nb: number
): (TeamsForNewMeeting | Meeting)[] {
  const mut_result: (TeamsForNewMeeting | Meeting)[] = [];

  const already_existing_meetings_for_split = all_meetings.filter(
    (m) => m.ranking_turn_nb === turn_nb
  );

  const team_split_first_half = teams_in_split.slice(
    0,
    Math.floor(teams_in_split.length / 2)
  );
  const team_split_second_half = teams_in_split.slice(
    team_split_first_half.length,
    team_split_first_half.length * 2
  );
  for (let i = 0; i < team_split_first_half.length; i++) {
    const team_1 = team_split_first_half[i];
    const team_2 = team_split_second_half[i];
    const new_meeting = makeNewMeeting(team_1, team_2, turn_nb);
    const existing_meeting = already_existing_meetings_for_split.find(
      (meeting) => areMeetingsEqual(meeting, new_meeting, turn_nb)
    );
    if (existing_meeting !== undefined) {
      mut_result.push(existing_meeting);
    } else {
      mut_result.push(new_meeting);
    }
  }

  const opt_last_odd_team =
    teams_in_split.length % 2 === 1
      ? teams_in_split[teams_in_split.length - 1]
      : undefined;
  if (opt_last_odd_team) {
    const opt_existing_meeting_for_odd_team = all_meetings.find(
      (m) =>
        m.ranking_turn_nb === turn_nb &&
        m.team1_id === opt_last_odd_team!.id &&
        m.team2_id === undefined
    );
    if (opt_existing_meeting_for_odd_team !== undefined) {
      mut_result.push(opt_existing_meeting_for_odd_team!);
    } else {
      mut_result.push({
        team1_id: opt_last_odd_team!.id,
        team2_id: undefined,
        ranking_turn_nb: turn_nb,
      });
    }
  }

  return mut_result;

  function makeNewMeeting(
    t1: Team,
    t2: Team,
    turn_nb: number
  ): TeamsForNewMeeting {
    return {
      team1_id: Math.min(t1.id, t2.id),
      team2_id: Math.max(t1.id, t2.id),
      ranking_turn_nb: turn_nb,
    };
  }

  function areMeetingsEqual(
    m1: Meeting,
    m2: TeamsForNewMeeting,
    turn_nb: number
  ): boolean {
    return (
      m1.ranking_turn_nb === turn_nb &&
      m2.ranking_turn_nb === turn_nb &&
      m1.team1_id == m2.team1_id &&
      m1.team2_id == m2.team2_id
    );
  }
}

export function canNewRankingTurnBeStarted(
  config: TournamentConfig,
  all_teams: Team[],
  all_meetings: Meeting[]
): boolean {
  const expected_nb_of_meetings_for_current_turn = Math.floor(
    all_teams.length / 2
  );
  const meetings_for_current_turn = all_meetings.filter(
    (meeting) => meeting.ranking_turn_nb === config.turn_nb
  );

  const is_ranking_phase = config.current_phase === TournamentPhase.Ranking;
  const are_meetings_missing =
    meetings_for_current_turn.length < expected_nb_of_meetings_for_current_turn;
  const are_all_meetings_complete_for_current_turn =
    meetings_for_current_turn.find((meeting) => !isMeetingComplete(meeting)) ===
    undefined;
  return (
    is_ranking_phase &&
    !are_meetings_missing &&
    are_all_meetings_complete_for_current_turn
  );
}

export function canNewRankingTurnBeCancelled(
  config: TournamentConfig,
  all_meetings: Meeting[]
): boolean {
  const meetings_for_current_turn = all_meetings.filter(
    (meeting) => meeting.ranking_turn_nb === config.turn_nb
  );

  const is_ranking_phase = config.current_phase === TournamentPhase.Ranking;
  const is_current_turn_first_turn = config.turn_nb === DEFAULT_TURN_NB;
  const has_current_turn_any_started_meetings =
    meetings_for_current_turn.length > 0;
  return (
    is_ranking_phase &&
    !is_current_turn_first_turn &&
    !has_current_turn_any_started_meetings
  );
}
