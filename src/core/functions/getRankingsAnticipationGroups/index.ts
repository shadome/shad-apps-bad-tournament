import { DEFAULT_TURN_NB, Meeting } from "../../types";
import getPointsForTeamForMeeting from "../getPointsForTeamForMeeting";
import {
  TeamRankingScore,
  TeamsForNewMeeting,
  makeMeetingsFromTeamSplit,
} from "../utils";

/*
  This function will give back teams split by group split for the config's current
  turn_nb which can be started in anticipation.
  Required:
  - getTurnType(anticipation_turn_nb) === Anticipation
  - config.current_phase === TournamentPhase.Ranking
*/
export default function getRankingsAnticipationGroups(
  anticipation_turn_nb: number,
  all_meetings: Meeting[],
  all_teams_current_rankings: TeamRankingScore[],
  group_split_counts: number[]
): ((TeamsForNewMeeting | Meeting)[] | undefined)[] | undefined {
  /*
    Algorithm:
    - get the current rankings for config.ranking_turn_nb-1
    - in the rankings, identify teams which have already played for turn_nb-1 (R1)
      and teams which still need to play (R2)
    - for (R2): keep the team with the maximum number of points (Tmax)
    - for (R2): keep the team with the minimum number of points (Tmin)
    - add to Tmax the number of points for a perfect victory
    - add to Tmin the number of points for an utter loss
    - get the group splits for turn_nb
    - insert the updated Tmax in the ranking (R1): 
      - for every split starting at index 0:
        - if Tmax's ranked position is not good enough to be in that split:
          - create the meetings for this split
          - remove from those meetings those which already exist for turn_nb
          - add them to the result
    - insert the updated Tmin in the ranking (R1): 
      - for every split in reverse order starting from the last position:
        - if Tmin's ranked position is too high to be in that split:
          - create the meetings for this split
          - remove from those meetings those which already exist for turn_nb
          - add them to the result
  */

  // This check is defensive programming: it is a function requirement,
  // and can prove heavy to call too often.
  // It and can be reactivated or deactivated following needs

  // const lazyIsAnticipationTurn = () => {
  //   const turn_type = getTurnType(
  //     config,
  //     all_teams,
  //     all_meetings,
  //     config.turn_nb
  //   );
  //   return turn_type === TurnType.Anticipation;
  // };

  // if (anticipation_turn_nb <= DEFAULT_TURN_NB || !lazyIsAnticipationTurn()) {
  if (anticipation_turn_nb <= DEFAULT_TURN_NB) {
    return undefined;
  }

  const mut_result: ((TeamsForNewMeeting | Meeting)[] | undefined)[] = [];

  const reversed_all_teams_current_rankings = [
    ...all_teams_current_rankings,
  ].reverse();

  const team_ids_which_have_played_for_previous_turn = all_meetings
    .filter((m) => m.ranking_turn_nb === anticipation_turn_nb - 1)
    .reduce((teams, m) => {
      teams.add(m.team1_id);
      if (m.team2_id !== undefined) {
        teams.add(m.team2_id!);
      }
      return teams;
    }, new Set<number>());

  const t_max_ranking = all_teams_current_rankings.reduce(
    (opt_tmax_ranking, ranking) => {
      // current_rankings is sorted descendingly, so the first encounter is t_max
      const must_break = opt_tmax_ranking !== undefined;
      const has_current_team_played_last_turn =
        team_ids_which_have_played_for_previous_turn.has(ranking.team.id);
      if (!must_break && !has_current_team_played_last_turn) {
        opt_tmax_ranking = ranking;
      }
      return opt_tmax_ranking;
    },
    undefined as TeamRankingScore | undefined
  );

  const t_min_ranking = reversed_all_teams_current_rankings.reduce(
    (opt_tmin_ranking, ranking) => {
      // reversed current_rankings is sorted ascendingly, so the first encounter is t_min
      const must_break = opt_tmin_ranking !== undefined;
      const has_current_team_played_last_turn =
        team_ids_which_have_played_for_previous_turn.has(ranking.team.id);
      if (!must_break && !has_current_team_played_last_turn) {
        opt_tmin_ranking = ranking;
      }
      return opt_tmin_ranking;
    },
    undefined as TeamRankingScore | undefined
  );

  if (t_max_ranking === undefined || t_min_ranking === undefined) {
    return undefined;
  }

  const group_splits_with_anticipation_marker = group_split_counts.map(
    (split_count) => ({ split_count, can_be_anticipated: false })
  );

  // make a team meeting so that team_1 has a perfect victory and team_2 has a total loss
  const team_id_won = 10;
  const team_id_lost = 20;
  const fake_meeting: Meeting = {
    id: 0,
    started_at: new Date().getTime(),
    team1_id: team_id_won,
    team1_doubles_score: 21,
    team1_singles_score: 21,
    team2_id: team_id_lost,
    team2_doubles_score: 0,
    team2_singles_score: 0,
  };
  const score_perfect_win = getPointsForTeamForMeeting(
    fake_meeting,
    team_id_won
  )!.score;
  const score_total_loss = getPointsForTeamForMeeting(
    fake_meeting,
    team_id_lost
  )!.score;
  const t_min_ranking_worst_score =
    t_min_ranking.total_score + score_total_loss;
  const t_max_ranking_best_score =
    t_max_ranking.total_score + score_perfect_win;
  const t_min_ranking_worst_score_reversed_idx =
    reversed_all_teams_current_rankings.findIndex(
      (ranking) => ranking.total_score >= t_min_ranking_worst_score
    );
  const t_max_ranking_best_score_idx = all_teams_current_rankings.findIndex(
    (ranking) => ranking.total_score < t_max_ranking_best_score
  );
  if (
    t_max_ranking_best_score_idx === -1 &&
    t_min_ranking_worst_score_reversed_idx === -1
  ) {
    return undefined;
  }

  // Note that by construction, the two iterations though group_splits_which_can_be_anticipated
  // should break before crossing one another
  let split_nb_sum = 0;
  if (t_max_ranking_best_score_idx !== -1) {
    for (const split of group_splits_with_anticipation_marker) {
      split_nb_sum += split.split_count;
      const can_be_anticipated =
        split_nb_sum < t_max_ranking_best_score_idx + 1; // + 1: indexes are 0-based, split counts are 1-based
      split.can_be_anticipated = can_be_anticipated;
      if (!can_be_anticipated) {
        break;
      }
    }
  }
  split_nb_sum = 0;
  if (t_min_ranking_worst_score_reversed_idx !== -1) {
    for (
      let i = group_splits_with_anticipation_marker.length - 1;
      i >= 0;
      i--
    ) {
      const split = group_splits_with_anticipation_marker[i];
      split_nb_sum += split.split_count;
      const can_be_anticipated =
        split_nb_sum < t_min_ranking_worst_score_reversed_idx + 1; // + 1: indexes are 0-based, split counts are 1-based
      split.can_be_anticipated = can_be_anticipated;
      if (!can_be_anticipated) {
        break;
      }
    }
  }

  let current_start_idx = 0;
  let current_end_idx = 0;
  for (const group_split of group_splits_with_anticipation_marker) {
    current_end_idx += group_split.split_count;
    if (group_split.can_be_anticipated) {
      const teams_in_split = all_teams_current_rankings
        .slice(current_start_idx, current_end_idx)
        .map((ranking_info) => ranking_info.team);
      const new_or_existing_meetings = makeMeetingsFromTeamSplit(
        all_meetings,
        teams_in_split,
        anticipation_turn_nb
      );
      mut_result.push(new_or_existing_meetings);
    } else {
      mut_result.push(undefined);
    }
    current_start_idx = current_end_idx;
  }

  return mut_result;
}
