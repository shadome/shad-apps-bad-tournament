import getRankingsAnticipationGroups from ".";
import { Field, Meeting, Team } from "../../types";

const default_all_teams: Team[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(
  (nb) => ({
    id: nb,
    name: nb.toString(),
    estimated_ranking: nb,
    player1Name: `Player${nb}`,
    player2Name: `Player${nb}`,
    player3Name: `Player${nb}`,
    player4Name: nb % 2 === 1 ? `Player${nb}` : undefined,
  })
);

const default_complete_meeting_1: Meeting = {
  id: 1,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 10,
  team1_id: 1,
  team2_id: 2,
};
const default_complete_meeting_2: Meeting = {
  id: 2,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 5,
  team2_singles_score: 4,
  team1_id: 3,
  team2_id: 4,
};
const default_complete_meeting_3: Meeting = {
  id: 3,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 3,
  team2_singles_score: 13,
  team1_id: 5,
  team2_id: 6,
};
const default_complete_meeting_4: Meeting = {
  id: 4,
  ranking_turn_nb: 2,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 1,
  team2_singles_score: 1,
  team1_id: 7,
  team2_id: 8,
};

const default_ongoing_anticipated_meeting: Meeting = {
  id: 5,
  ranking_turn_nb: 3,
  started_at: new Date().getTime(),
  ended_at: undefined,
  field: Field.Field1,
  team1_id: 1,
  team2_id: 2,
};

const default_complete_anticipated_meeting: Meeting = {
  id: 6,
  ranking_turn_nb: 3,
  started_at: new Date().getTime(),
  ended_at: new Date().getTime(),
  team1_doubles_score: 21,
  team1_singles_score: 21,
  team2_doubles_score: 12,
  team2_singles_score: 12,
  team1_id: 1,
  team2_id: 2,
};

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not catch up to the other four, splits: [2,2,2]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[2],
      total_score: 0,
    },
    {
      team: default_all_teams[3],
      total_score: 0,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    undefined,
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not catch up to the other four, splits: [2,4,4]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_2,
    default_complete_meeting_3,
    default_complete_meeting_4,
  ];
  const group_split_counts = [2, 4, 4];
  const team_rankings = [
    {
      team: default_all_teams[2],
      total_score: 612,
    },
    {
      team: default_all_teams[3],
      total_score: 604,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[6],
      total_score: 20,
    },
    {
      team: default_all_teams[7],
      total_score: 15,
    },
    {
      team: default_all_teams[8],
      total_score: 10,
    },
    {
      team: default_all_teams[9],
      total_score: 5,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[2].id,
        team2_id: default_all_teams[3].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    undefined,
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not be caught up by the other four, splits: [2,2,2]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[3],
      total_score: 999,
    },
    {
      team: default_all_teams[2],
      total_score: 998,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
  ];
  const expected = [
    undefined,
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not be caught up by the other four, splits: [2,4,4]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_2,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 4, 4];
  const team_rankings = [
    {
      team: default_all_teams[6],
      total_score: 997,
    },
    {
      team: default_all_teams[7],
      total_score: 996,
    },
    {
      team: default_all_teams[8],
      total_score: 995,
    },
    {
      team: default_all_teams[9],
      total_score: 994,
    },
    {
      team: default_all_teams[10],
      total_score: 993,
    },
    {
      team: default_all_teams[11],
      total_score: 992,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
  ];
  const expected = [
    undefined,
    undefined,
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not catch up to the other four, splits: [2,2,1]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 1];
  const team_rankings = [
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
    {
      team: default_all_teams[2],
      total_score: 0,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    undefined,
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - one meeting missing/ongoing for the current turn, whose teams will not be caught up by the other four, splits: [2,2,1]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 1];
  const team_rankings = [
    {
      team: default_all_teams[2],
      total_score: 998,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 378,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
  ];
  const expected = [
    undefined,
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[1].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [
      {
        team1_id: default_all_teams[5].id,
        team2_id: undefined,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - undefined in the middle, splits: [2,2,2]", () => {
  const anticipation_turn_nb = 3;
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[3],
      total_score: 322,
    },
    {
      team: default_all_teams[2],
      total_score: 316,
    },
    {
      team: default_all_teams[1],
      total_score: 118,
    },
    {
      team: default_all_teams[5],
      total_score: 97,
    },
  ];
  const expected = [
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[4].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    undefined,
    [
      {
        team1_id: default_all_teams[1].id,
        team2_id: default_all_teams[5].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - undefined in the middle, fetching existing ongoing anticipated meetings, splits: [2,2,2]", () => {
  const anticipation_turn_nb = 3;
  const anticipated_meeting_1 = {
    ...default_ongoing_anticipated_meeting,
    team1_id: default_all_teams[0].id,
    team2_id: default_all_teams[4].id,
  };
  const anticipated_meeting_2 = {
    ...default_ongoing_anticipated_meeting,
    team1_id: default_all_teams[1].id,
    team2_id: default_all_teams[5].id,
  };
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
    anticipated_meeting_1,
    anticipated_meeting_2,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[3],
      total_score: 322,
    },
    {
      team: default_all_teams[2],
      total_score: 316,
    },
    {
      team: default_all_teams[1],
      total_score: 118,
    },
    {
      team: default_all_teams[5],
      total_score: 97,
    },
  ];
  const expected = [
    [anticipated_meeting_1],
    undefined,
    [anticipated_meeting_2],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - undefined in the middle, fetching existing complete anticipated meetings, splits: [2,2,2]", () => {
  const anticipation_turn_nb = 3;
  const anticipated_meeting_1 = {
    ...default_complete_anticipated_meeting,
    team1_id: default_all_teams[0].id,
    team2_id: default_all_teams[4].id,
  };
  const anticipated_meeting_2 = {
    ...default_complete_anticipated_meeting,
    team1_id: default_all_teams[1].id,
    team2_id: default_all_teams[5].id,
  };
  const all_meetings: Meeting[] = [
    default_complete_meeting_1,
    default_complete_meeting_3,
    anticipated_meeting_1,
    anticipated_meeting_2,
  ];
  const group_split_counts = [2, 2, 2];
  const team_rankings = [
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 578,
    },
    {
      team: default_all_teams[3],
      total_score: 322,
    },
    {
      team: default_all_teams[2],
      total_score: 316,
    },
    {
      team: default_all_teams[1],
      total_score: 118,
    },
    {
      team: default_all_teams[5],
      total_score: 97,
    },
  ];
  const expected = [
    [anticipated_meeting_1],
    undefined,
    [anticipated_meeting_2],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - fetching existing ongoing anticipated meetings, splits: [2,2,1]", () => {
  const anticipation_turn_nb = 3;
  const anticipated_meeting = {
    ...default_ongoing_anticipated_meeting,
    team1_id: default_all_teams[5].id,
    team2_id: undefined,
  };
  const all_meetings: Meeting[] = [
    anticipated_meeting,
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 1];
  const team_rankings = [
    {
      team: default_all_teams[2],
      total_score: 998,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 378,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
  ];
  const expected = [
    undefined,
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[1].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [anticipated_meeting],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});

test("Testing getRankingsAnticipationGroups - fetching existing complete anticipated meetings, splits: [2,2,1]", () => {
  const anticipation_turn_nb = 3;
  const anticipated_meeting = {
    ...default_complete_anticipated_meeting,
    team1_id: default_all_teams[5].id,
    team2_id: undefined,
  };
  const all_meetings: Meeting[] = [
    anticipated_meeting,
    default_complete_meeting_1,
    default_complete_meeting_3,
  ];
  const group_split_counts = [2, 2, 1];
  const team_rankings = [
    {
      team: default_all_teams[2],
      total_score: 998,
    },
    {
      team: default_all_teams[4],
      total_score: 584,
    },
    {
      team: default_all_teams[0],
      total_score: 378,
    },
    {
      team: default_all_teams[1],
      total_score: 322,
    },
    {
      team: default_all_teams[5],
      total_score: 316,
    },
  ];
  const expected = [
    undefined,
    [
      {
        team1_id: default_all_teams[0].id,
        team2_id: default_all_teams[1].id,
        ranking_turn_nb: anticipation_turn_nb,
      },
    ],
    [anticipated_meeting],
  ];
  const actual = getRankingsAnticipationGroups(
    anticipation_turn_nb,
    all_meetings,
    team_rankings,
    group_split_counts
  );
  expect(actual).toEqual(expected);
});
