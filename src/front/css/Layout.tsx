import { Property } from "csstype";
import React from "react";

export function size(
  width: Property.Width | number,
  height?: Property.Height | number
): React.CSSProperties {
  const _height = height === undefined ? width : height;
  return { width, height: _height };
}

/**
 * @param flexDirection
 * @param size width (or height, depending on the flex flow axis), which should be specified as a layout parameter considering what is asked of the flex behaviour
 * @param gap
 * @param alignItems
 * @param justifyContent
 * @param flexWrap
 * @returns
 */
export function flex(
  flexDirection: Property.FlexDirection,
  size?: Property.Width | Property.Height | number,
  gap?: Property.Gap | number,
  alignItems?: Property.AlignItems,
  justifyContent?: Property.JustifyContent,
  flexWrap?: Property.FlexWrap
): React.CSSProperties {
  const isXAxis = ["initial", "row", "row-reverse"].includes(flexDirection);
  const isYAxis = ["column", "column-reverse"].includes(flexDirection);
  const width = isXAxis && size !== undefined ? { width: size } : {};
  const height = isYAxis && size !== undefined ? { height: size } : {};
  return {
    ...width,
    ...height,
    display: "flex",
    flexDirection,
    gap,
    alignItems,
    justifyContent,
    flexWrap,
  };
}
