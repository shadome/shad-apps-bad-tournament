import { Property } from "csstype";
import React from "react";

/**
 * If the max value is not specified, it will be the same as min.
 */
export function boundHeight(
  minHeight: Property.Height | number,
  maxHeight?: Property.Height | number
): React.CSSProperties {
  const _maxHeight = maxHeight === undefined ? minHeight : maxHeight;
  return { minHeight, maxHeight: _maxHeight };
}

/**
 * If the max value is not specified, it will be the same as min.
 */
export function boundWidth(
  minWidth: Property.MinWidth | number,
  maxWidth?: Property.MaxWidth | number
): React.CSSProperties {
  const _maxWidth = maxWidth === undefined ? minWidth : maxWidth;
  return { minWidth, maxWidth: _maxWidth };
}

/**
 * If minHeight is not specified, the height and width of the element will be the same.
 * If the max values are not specified, it will be the same as min.
 */
export function boundSize(
  minWidth: Property.MinWidth | number,
  minHeight?: Property.MinHeight | number,
  maxWidth?: Property.MaxWidth | number,
  maxHeight?: Property.MaxHeight | number
): React.CSSProperties {
  const _minHeight = minHeight === undefined ? minWidth : minHeight;
  return {
    ...boundWidth(minWidth, maxWidth),
    ...boundHeight(_minHeight, maxHeight),
  };
}
