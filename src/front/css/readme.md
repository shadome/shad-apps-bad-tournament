# Notes about styling

240410 - UNDER WORK

## Taxinomy

"Styling" may imply distinct things:

- the layout, or how something is positioned/sized/etc. relatively to many other things (flex attributes, max sizes, margins, etc.),
- the appearance, or how something should look like independently of anything else (fixed sizes, min sizes, colours, paddings, fonts, etc.),
- the behaviour, or how something should behave in certains states (changing colours on hover, etc.), which is usually appearance-related.

By convention, the generated HTML code should be clean, so there should be no elements used only for the layout if they can be avoided, e.g.:

```html
<div style="width: 100px; height: 50px">
  <button
    style="unset: all; background-color: blue; width: 100%; height: 100%;"
  >
    Click me!
  </button>
</div>
```

should become:

```html
<button style="unset: all; background-color: blue; width: 100px; height: 50px;">
  Click me!
</button>
```

## When using native components

This case is simmple. The "caller" of the native components, i.e., the closest enclosing layout component, can mix layout and appearance just as in the example above.

## When using react components

The "caller" of the react component should be able to extend the native react component of higher order and specify styling and class names. The props of the react component can make sure of that.
Example of a toggle react element which is in fact a label enclosing some other native components in order to display a slider which can go from left to right with an obvious "checked" and "unchecked" state:

```ts
interface ToggleProps extends React.HTMLProps<HTMLLabelElement> {
  id: string | undefined;
  isChecked: boolean;
  onToggle: (checked: boolean) => void;
}
// ...
export default function Toggle({
  id,
  isChecked,
  onToggle,
  ...props
}: ToggleProps) {
  // ...
  return (
    <label
      htmlFor={id}
      {...props}
      style={{
        // ...
        ...props.style,
        // required styling which cannot
        // be overriden by the caller,
        // typically "appearance-related
        position: "relative",
        height: `${toggleHeight}px`,
        padding: `${togglePadding}px`,
      }}
      className={`
        default-layout-class1
        default-layout-class2
        ${props?.className}
        cannot-override-appearance-class1
        cannot-override-appearance-class2
      `}
      // ...
    >
      // ...
    </label>
  );
}
```

## Native components versus React overridden components

TODO

- should i rewrite my own `<Button />` component or use `<button />` ?
- should i use an external library such as MUI?
  - cons: heavy in size, generates big bundles and html files, maintainability costs when migrating versions, asks the developers to know about the specific way those frameworks are designed (props, implementation choices, etc.)
  - pros: quick to yield a stylish result, usually good to give the same rendering across all browsers

## CSS classes versus inline styling

TODO

in a few words, i prefer inline styling,

inline-styling pros:

- there is no crossover between two languages, the code is cleaner
- everything is at the same place
- javascript's expressivity usually better than css's (conditionals, spread operator, etc.)
- the styling of every component can be custom, when a css class usually brings more than one rule

css pros:

- smaller sized files sent from the server to the client's browser
- most people still use css or css-based frameworks, with or without react
- some things exist in css3 and not in inline-styling, e.g., hover modifiers,
