export enum ElementSize {
  xt = "xt", // extra tiny
  tn = "tn", // tiny
  xs = "xs", // extra small
  sm = "sm", // small
  md = "md", // medium
  lg = "lg", // large
  xl = "xl", // extra large
}

export const breakpointsToPx: Readonly<Record<ElementSize, number>> = {
  xt: 0,
  tn: 0,
  xs: 500,
  sm: 600,
  md: 900,
  lg: 1200,
  xl: 1536,
};

export const fromPx = (px: number) =>
  px >= breakpointsToPx.xl ? breakpointsToPx.xl :
  px >= breakpointsToPx.lg ? breakpointsToPx.lg :
  px >= breakpointsToPx.md ? breakpointsToPx.md :
  px >= breakpointsToPx.sm ? breakpointsToPx.sm :
  px >= breakpointsToPx.xs ? breakpointsToPx.xs :
  /* default */ breakpointsToPx.tn;

export default ElementSize;
