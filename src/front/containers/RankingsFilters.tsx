import { fontSize } from "../CssInterop";
import RadioButtons, { Direction } from "../components/RadioButtons";
import Toggle from "../components/Toggle";

export enum FilterMeetingStatus {
  All = 0,
  ToStart = 1,
  Ongoing = 2,
}

function filterMeetingStatusToString(status: FilterMeetingStatus): string {
  switch (status) {
    case FilterMeetingStatus.All:
      return "Tous";
    case FilterMeetingStatus.ToStart:
      return "À lancer";
    case FilterMeetingStatus.Ongoing:
      return "En cours";
  }
}

interface Props {
  isOnlyCurrentTurn: boolean;
  setIsOnlyCurrentTurn: (value: boolean) => void;
  selectedMeetingStatus: FilterMeetingStatus;
  setSelectedMeetingStatus: (value: FilterMeetingStatus) => void;
}

export default function RankingsFilters({
  isOnlyCurrentTurn,
  setIsOnlyCurrentTurn,
  selectedMeetingStatus,
  setSelectedMeetingStatus,
}: Props) {
  return (
    <fieldset className="flexcol x-start bbox g16 p16 border fitcontent-x">
      <legend className="px8">Filtres d'affichage</legend>

      <div className="flexrow g16 ml8" style={{ alignItems: "center" }}>
        <Toggle
          id="current_turn_toggle"
          isChecked={isOnlyCurrentTurn}
          onToggle={setIsOnlyCurrentTurn}
        />
        <label htmlFor="current_turn_toggle">Tour courant</label>
      </div>

      <fieldset className="bbox p8 fitcontent-x">
        <legend
          style={{
            fontSize: fontSize.sm,
            fontWeight: 600,
          }}
        >
          Statut des rencontres
        </legend>

        <RadioButtons
          direction={Direction.Vertical}
          name="meetings_statuses"
          selectedValue={selectedMeetingStatus}
          onSelect={setSelectedMeetingStatus}
          values={Object.values(FilterMeetingStatus)
            .filter((fms) => !isNaN(Number(fms)))
            .map((fms) => ({
              key: Number(fms),
              displayValue: filterMeetingStatusToString(
                fms as FilterMeetingStatus
              ),
            }))}
        />
      </fieldset>
    </fieldset>
  );
}
