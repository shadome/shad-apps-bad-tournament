import React from "react";
import { isMeetingComplete } from "../../core/functions/utils";
import { Field, Meeting, Team } from "../../core/types";
import {
  backgroundColour,
  borderColour,
  colours,
  fontSize,
  textColour,
} from "../CssInterop";
import * as Fields from "../../repositories/fields";

enum MeetingStatus {
  ToStart = 1,
  Ongoing = 2,
  Complete = 3,
}

function fieldToString(field: Field): string {
  switch (field) {
    case Field.Field1:
      return "Terrain 1";
    case Field.Field2:
      return "Terrain 2";
    case Field.Field3:
      return "Terrain 3";
    case Field.Field4:
      return "Terrain 4";
    case Field.Field5:
      return "Terrain 5";
    case Field.Field6:
      return "Terrain 6";
    case Field.Field7:
      return "Terrain 7";
  }
}

function meetingStatusToString(meetingStatus: MeetingStatus): string {
  switch (meetingStatus) {
    case MeetingStatus.ToStart:
      return "À lancer";
    case MeetingStatus.Ongoing:
      return "En cours";
    case MeetingStatus.Complete:
      return "Terminée";
  }
}

function meetingStatusToColour(meetingStatus: MeetingStatus): string {
  switch (meetingStatus) {
    case MeetingStatus.ToStart:
      return colours.error.average;
    case MeetingStatus.Ongoing:
      return colours.info.background;
    case MeetingStatus.Complete:
      return colours.success.background;
  }
}

export interface MeetingToStart {
  team1Id: number;
  team2Id: number;
}

interface Props {
  // either is required, but not both
  meetingToStart?: MeetingToStart;
  existingMeetingId?: number;
}

export default function MeetingCard({
  existingMeetingId,
  meetingToStart,
}: Props): JSX.Element {
  function doStartMeeting(meeting: MeetingToStart, field: Field): void {
    // TODO start a new meeting
  }
  function doCancelOngoingMeeting(meetingId: number): void {
    // TODO cancel a meeting
  }
  function goSetMeetingScores(meetingId?: number): void {
    // TODO open a modal or something to set a meeting's scores and save it in the database
    // can be used to set the scores for the first time or modify existing scores
  }

  const [selectedField1, _setSelectedField1] = React.useState<string>("");
  function setSelectedField1(field: string): void {
    _setSelectedField1(field);
    setSelectedField2("");
  }
  const [selectedField2, setSelectedField2] = React.useState<string>("");

  // TODO get a meeting from its ID using the repository
  const optMeeting: Meeting | undefined =
    existingMeetingId !== undefined
      ? {
          id: 42,
          started_at: new Date().getTime(),
          // ended_at: new Date().getTime(),
          team1_id: 1,
          team2_id: 2,
          ranking_turn_nb: 2,
          // team1_doubles_score: 21,
          // team1_singles_score: 30,
          // team2_doubles_score: 1,
          // team2_singles_score: 29,
          field: Field.Field7,
          field_2: Field.Field5,
        }
      : undefined;

  const meetingStatus =
    optMeeting !== undefined && isMeetingComplete(optMeeting!)
      ? MeetingStatus.Complete
      : optMeeting !== undefined
      ? MeetingStatus.Ongoing
      : MeetingStatus.ToStart;

  const team1: Team = {
    id: 1,
    estimated_ranking: 2,
    name: "Équipe numéro 2",
    player1Name: `Player1`,
    player2Name: `Player2`,
    player3Name: `Player3`,
  };

  const team2: Team = {
    id: 2,
    estimated_ranking: 5,
    name: "Nom de l'équipe 2, un peu plus long que les autres, devrait être sous ellipse",
    player1Name: `Joueur numéro 1`,
    player2Name: `Joueur numéro 2`,
    player3Name: `Joueur numéro 3`,
    player4Name: `Joueur numéro 4`,
  };

  const optPoolName: string | undefined = "Poule 1";
  // const optPoolName: string | undefined = undefined;

  // if true, a complete meeting displays a button for modifying its score
  const canStillModifyScore = meetingStatus === MeetingStatus.Complete && false; //TODO

  // if true, a meeting "to start" cannot be started and needs a text warning
  const areTeamsInOtherOngoingMeetings =
    meetingStatus === MeetingStatus.ToStart && false; // TODO

  const gridColNameFixedWidth =
    meetingStatus === MeetingStatus.ToStart ? "470px" : "350px";
  const gridColumnTitleFixedWidth = optPoolName !== undefined ? "40px" : "auto";
  return (
    <div
      className="border fitcontent-y"
      style={{
        width: "492px",
        display: "inline-grid",
        gridTemplateColumns: `min-content auto min-content min-content`,
        gridTemplateAreas: `
          "title status singles_title doubles_title"
          "title team1_name team1_singles_score team1_doubles_score"
          "title team2_name team2_singles_score team2_doubles_score"
          "title actions actions actions"
          `,
      }}
    >
      {optPoolName && (
        <div
          className="p8"
          style={{
            gridArea: "title",
            backgroundColor: backgroundColour.dp1,
            writingMode: "sideways-lr",
            borderRight: `1px solid ${borderColour}`,
          }}
        >
          {optPoolName!}
        </div>
      )}
      <div
        className="flexrow p8 g8"
        style={{ gridArea: "status", textAlign: "left" }}
      >
        {meetingStatus !== MeetingStatus.Ongoing && (
          <span
            className="px8 py4"
            style={{
              backgroundColor: meetingStatusToColour(meetingStatus),
              borderRadius: 8,
            }}
          >
            {meetingStatusToString(meetingStatus)}
          </span>
        )}
        {optMeeting?.field !== undefined && (
          <span
            className="px8 py4"
            style={{
              backgroundColor: meetingStatusToColour(meetingStatus),
              borderRadius: 8,
            }}
          >
            {fieldToString(optMeeting!.field!)}
          </span>
        )}
        {optMeeting?.field_2 !== undefined && (
          <span
            className="px8 py4"
            style={{
              backgroundColor: meetingStatusToColour(meetingStatus),
              borderRadius: 8,
            }}
          >
            {fieldToString(optMeeting!.field_2!)}
          </span>
        )}
      </div>
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="p8"
          style={{
            gridArea: "singles_title",
            color: backgroundColour.dp1,
            fontSize: fontSize.sm,
            fontWeight: 600,
          }}
        >
          S
        </div>
      )}
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="p8"
          style={{
            gridArea: "doubles_title",
            color: backgroundColour.dp1,
            fontSize: fontSize.sm,
            fontWeight: 600,
          }}
        >
          D
        </div>
      )}
      <div
        className="px8 py4"
        style={{ gridArea: "team1_name", textAlign: "left" }}
      >
        Nom de l'équipe 1
      </div>
      <div
        className="px8 py4"
        style={{
          gridArea: "team2_name",
          whiteSpace: "nowrap",
          overflow: "hidden",
          textOverflow: "ellipsis",
          textAlign: "left",
        }}
      >
        Nom de l'équipe 2, un peu plus long que les autres, devrait être sous
        ellipse
      </div>
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="px8 py4"
          style={{ gridArea: "team1_singles_score", textAlign: "right" }}
        >
          {optMeeting?.team1_singles_score}
        </div>
      )}
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="px8 py4"
          style={{ gridArea: "team1_doubles_score", textAlign: "right" }}
        >
          {optMeeting?.team1_doubles_score}
        </div>
      )}
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="px8 py4"
          style={{
            gridArea: "team2_singles_score",
            textAlign: "right",
            color: textColour.regular,
          }}
        >
          {optMeeting?.team2_singles_score}
        </div>
      )}
      {meetingStatus === MeetingStatus.Complete && (
        <div
          className="px8 py4"
          style={{
            gridArea: "team2_doubles_score",
            textAlign: "right",
            color: textColour.regular,
          }}
        >
          {optMeeting?.team2_doubles_score}
        </div>
      )}
      <div className="flexrow mt4" style={{ gridArea: "actions" }}>
        {meetingStatus === MeetingStatus.Complete && canStillModifyScore ? (
          <button
            className="btn grow"
            style={{
              justifyContent: "center",
              borderLeft: "none",
              borderBottom: "none",
              borderTop: `1px solid ${borderColour.default}`,
              borderRight: `1px solid ${borderColour.default}`,
            }}
            onClick={() => goSetMeetingScores(existingMeetingId!)}
          >
            Modifier les scores
          </button>
        ) : meetingStatus === MeetingStatus.Complete && !canStillModifyScore ? (
          <span
            className="p8"
            style={{ fontStyle: "italic", color: textColour.regular }}
          >
            Les scores ne sont plus modifiables
          </span>
        ) : meetingStatus === MeetingStatus.Ongoing ? (
          <>
            <button
              className="btn btn-danger grow w100"
              style={{
                justifyContent: "center",
                borderLeft: "none",
                borderBottom: "none",
                borderTop: `1px solid ${borderColour}`,
                borderRight: `1px solid ${borderColour}`,
              }}
            >
              Annuler
            </button>
            <button
              className="btn grow w100 btn-submit"
              onClick={() => goSetMeetingScores()}
              style={{
                justifyContent: "center",
                borderLeft: "none",
                borderBottom: "none",
                borderRight: "none",
                borderTop: `1px solid ${borderColour}`,
              }}
            >
              Entrer scores
            </button>
          </>
        ) : MeetingStatus.ToStart && areTeamsInOtherOngoingMeetings ? (
          <span
            className="p8"
            style={{ fontStyle: "italic", color: colours.warning.foreground }}
          >
            Une des équipes est en cours de rencontre
          </span>
        ) : MeetingStatus.ToStart && !areTeamsInOtherOngoingMeetings ? (
          <div className="flexcol w100">
            <div className="flexrow">
              <select
                value={selectedField1}
                onChange={(evt) => {
                  setSelectedField1(evt.target.value);
                }}
                className="unset p8 btn grow w100"
                style={{
                  fontSize: fontSize.sm,
                  height: "48px",
                  borderLeft: "none",
                }}
              >
                <option value={""}>Terrain principal</option>
                {Fields.getAllStr().map((field) => (
                  <option key={field} value={field}>
                    {fieldToString(Fields.fromStr(field))}
                  </option>
                ))}
              </select>
              <select
                value={selectedField2}
                disabled={!Boolean(selectedField1)}
                onChange={(evt) => {
                  setSelectedField2(evt.target.value);
                }}
                className="unset p8 btn grow w100"
                style={{
                  fontSize: fontSize.sm,
                  height: "48px",
                  borderRight: "none",
                  borderLeft: "none",
                  borderTop: `1px solid ${borderColour}`,
                  borderBottom: `1px solid ${borderColour}`,
                }}
              >
                <option value={""}>Terrain optionnel</option>
                {Fields.getAllStr().map((field) => (
                  <option
                    key={field}
                    value={field}
                    disabled={selectedField1 === field}
                  >
                    {fieldToString(Fields.fromStr(field))}
                  </option>
                ))}
              </select>
            </div>
            <button
              className="btn btn-submit grow"
              disabled={!Boolean(selectedField1)}
              onClick={() =>
                doStartMeeting(meetingToStart!, Fields.fromStr(selectedField1!))
              }
              style={{ justifyContent: "center", border: "none" }}
            >
              Lancer la rencontre
            </button>
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
