import { iconSize } from "../CssInterop";
import {
  ArrowLeftIcon,
  CheckmarkIcon,
  CrossIcon,
  PlusIcon,
} from "../components/Icons";

interface Props {}

export default function RankingsActions({}: Props): JSX.Element {
  const canAddNewTurn = true;
  const canCancelLastTurn = false;
  const canLockRankings = true;
  const canUnlockRankings = false;
  function doAddNewTurn() {
    // TODO config.current_turn++
  }
  function doCancelLastTurn() {
    // TODO config.current_turn--
  }
  function doLockRankings() {
    // TODO config.current_phase = 3
  }
  function doUnlockRankings() {
    // TODO config.current_phase = 2
  }
  return (
    <fieldset className={`unset flexcol fitcontent-x g8 bbox p8 border grow`}>
      <legend className="px8">Actions</legend>

      <button
        className="btn btn-link g8"
        disabled={!canAddNewTurn}
        onClick={doAddNewTurn}
      >
        <PlusIcon style={{ width: iconSize.md, height: iconSize.md }} />
        Ajouter un tour
      </button>

      <button
        className="btn btn-link g8"
        disabled={!canCancelLastTurn}
        onClick={doCancelLastTurn}
      >
        <ArrowLeftIcon style={{ width: iconSize.md, height: iconSize.md }} />
        Annuler le dernier tour
      </button>

      <button
        className="btn btn-link g8"
        disabled={!canLockRankings}
        onClick={doLockRankings}
      >
        <CheckmarkIcon style={{ width: iconSize.md, height: iconSize.md }} />
        Verrouiller le classement et lancer la phase de poules
      </button>

      <button
        className="btn btn-link g8"
        disabled={!canUnlockRankings}
        onClick={doUnlockRankings}
      >
        <CrossIcon style={{ width: iconSize.md, height: iconSize.md }} />
        Revenir à la phase de classement
      </button>
    </fieldset>
  );
}
