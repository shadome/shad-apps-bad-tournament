import { TeamRankingAggregatedInfo } from "../../core/functions/getCurrentRankings";
import { fontSize, textColour } from "../CssInterop";

interface Props {
  rankingInfos: TeamRankingAggregatedInfo[];
}

export default function DisplayRankings({ rankingInfos }: Props) {
  return (
    <>
      <table
        className="bbox border"
        style={{
          textAlign: "left",
          width: "fit-content",
        }}
      >
        <thead
          style={{
            color: textColour.regular,
            fontSize: fontSize.sm,
          }}
        >
          <tr>
            <th style={{ padding: "8px 8px 0px 8px" }}>#</th>
            <th style={{ padding: "8px 8px 0px 8px" }}>Nom</th>
            <th style={{ padding: "4px 4px 0px 4px" }}>NR</th>
            <th style={{ padding: "4px 4px 0px 4px" }}>MG</th>
            <th style={{ padding: "4px 4px 0px 4px" }}>BD</th>
            <th style={{ padding: "4px 4px 0px 4px" }}>MO</th>
            <th style={{ padding: "4px 4px 0px 4px" }}>Pts</th>
          </tr>
        </thead>
        <tbody>
          {rankingInfos.map((rankingInfo, idx) => (
            <tr key={rankingInfo.team.id}>
              <td
                className="p8"
                style={{ fontWeight: 600, textAlign: "right" }}
              >
                {idx + 1}
              </td>
              <td className="p8">{rankingInfo.team.name}</td>
              <td className="p4 color-disabled" style={{ textAlign: "right" }}>
                {rankingInfo.total_meetings_played}
              </td>
              <td className="p4 color-disabled" style={{ textAlign: "right" }}>
                {rankingInfo.total_matches_won}
              </td>
              <td className="p4 color-disabled" style={{ textAlign: "right" }}>
                {rankingInfo.total_defensive_bonus}
              </td>
              <td className="p4 color-disabled" style={{ textAlign: "right" }}>
                {rankingInfo.total_offensive_malus}
              </td>
              <td className="p4 color-disabled" style={{ textAlign: "right" }}>
                {rankingInfo.total_score}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
