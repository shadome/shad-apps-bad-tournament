import { palette } from "../CssInterop";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";
import * as FieldRepository from "../../repositories/fields";
import * as TeamRepository from "../../repositories/teams";
import * as MeetingRepository from "../../repositories/meetings";
import * as ConfigRepository from "../../repositories/tournamentConfig";
import {
  BadmintonScorePoints,
  Field,
  Meeting,
  TournamentConfig,
} from "../../core/types";
import RankingsTurn from "./ranking_phase_page/RankingsTurn";
import getRankingsGroups from "../../core/functions/getRankingsGroups";
import getCurrentRankings from "../../core/functions/getCurrentRankings";
import {
  TeamsForNewMeeting,
  canNewRankingTurnBeCancelled,
  canNewRankingTurnBeStarted,
  getRankingTurnGroupSplit,
} from "../../core/functions/utils";
import React from "react";
import getRankingsAnticipationGroups from "../../core/functions/getRankingsAnticipationGroups";
import MeetingScoreSetter from "./ranking_phase_page/meeting_item/MeetingScoreSetter";
import Modal from "../components/Modal";
import RankingsPageFilters from "./ranking_phase_page/RankingsPageFilters";
import HorizontalSeparator from "../components/HorizontalSeparator";
import RankingsPageActions from "./ranking_phase_page/RankingsPageActions";
import canPoolsPhaseBeStarted from "../../core/functions/canPoolsPhaseBeStarted";
import {
  LocalStorageEvent,
  persistState,
} from "../../repositories/localstorage";

export enum FilterMeetingStatus {
  All = 0,
  ToStart = 1,
  Ongoing = 2,
  Complete = 3,
}

export const RANKINGS_PHASE_PATH: string = "/rankings_phase";

type TurnNbToMeetingInfo = (TeamsForNewMeeting | Meeting)[][] & {
  turnNb?: number; // marked as nullable for construction purposes but is never null
};

export default function RankingsPhasePage(): JSX.Element {
  const [selectedMeetingStatusFilter, selectMeetingStatusFilter] =
    React.useState<FilterMeetingStatus>(FilterMeetingStatus.All);

  const [meetingIsWhoseScoresAreBeingSet, setMeetingIsWhoseScoresAreBeingSet] =
    React.useState<number | undefined>(undefined);

  const [availableFields, setAvailableFields] = React.useState<Field[]>(
    FieldRepository.getAvailableFields()
  );
  const [allMeetings, setAllMeetings] = React.useState<Meeting[]>(
    MeetingRepository.getAll()
  );
  const [isCurrentTurnOnyChecked, setIsCurrentTurnOnyChecked] =
    React.useState<boolean>(false);

  const [config, setConfig] = React.useState<TournamentConfig>(
    ConfigRepository.get()
  );

  // No need for a state: the teams will never change due to an action on this page
  const allTeams = TeamRepository.getAll();

  // dimensions: meetings (active or to create) for one of the phase of one of the turn
  const turnNbToMeetings: TurnNbToMeetingInfo[] = [
    ...Array(config.turn_nb).keys(),
  ]
    // turn numbers are 1-based, indices are 0-based
    .map((idx) => idx + 1)
    .filter((turnNb) =>
      isCurrentTurnOnyChecked ? turnNb === config.turn_nb : true
    )
    .map((turnNb) => {
      const rankingsForTurnNb = getCurrentRankings(
        allTeams,
        allMeetings,
        turnNb - 1
      );
      const groupSplitsForTurnNb = getRankingTurnGroupSplit(
        allTeams.length,
        turnNb
      )!;
      const result: TurnNbToMeetingInfo = getRankingsGroups(
        turnNb,
        allMeetings,
        rankingsForTurnNb,
        groupSplitsForTurnNb
      )!;
      result.turnNb = turnNb;
      return result;
    });

  // const anticipationMeetings = isCurrentTurnOnyChecked
  //   ? undefined
  //   : getRankingsAnticipationGroups(
  //       config.turn_nb + 1,
  //       allMeetings,
  //       getCurrentRankings(allTeams, allMeetings, config.turn_nb),
  //       getRankingTurnGroupSplit(allTeams.length, config.turn_nb + 1)!
  //     );

  // const areThereAnyMeetingsToAnticipate =
  //   anticipationMeetings !== undefined &&
  //   anticipationMeetings.find((x) => x !== undefined) !== undefined;

  const canStartNewTurn = canNewRankingTurnBeStarted(
    config,
    allTeams,
    allMeetings
  );

  const canCancelNewTurn = canNewRankingTurnBeCancelled(config, allMeetings);

  const canStartPoolsPhase = canPoolsPhaseBeStarted(
    config,
    allTeams,
    allMeetings
  );

  return (
    <>
      <Modal
        onClose={doCloseScoreSetterModal}
        isOpen={meetingIsWhoseScoresAreBeingSet !== undefined}
      >
        {meetingIsWhoseScoresAreBeingSet !== undefined && (
          <MeetingScoreSetter
            meetingId={meetingIsWhoseScoresAreBeingSet!}
            allTeams={allTeams}
            allMeetings={allMeetings}
            onSubmit={doSetMeetingScore}
            onClose={doCloseScoreSetterModal}
          />
        )}
      </Modal>
      <div
        className="bbox"
        style={{
          padding: 16,
          gridArea: "content",
          overflowY: "scroll",
          ...Appearance.boundHeight("100vh"),
          ...Layout.flex("column", "100vh", 16),
        }}
      >
        {/* {areThereAnyMeetingsToAnticipate && (
          <RankingsTurn
            style={{ ...Layout.size("100%", "fit-content") }}
            startsCollapsed={false}
            config={config}
            turnNb={config.turn_nb + 1}
            allTeams={allTeams}
            allMeetings={allMeetings}
            meetingsInfoPerSplit={anticipationMeetings!}
            availableFields={availableFields}
            selectedMeetingStatusFilter={selectedMeetingStatusFilter}
            onSetMeetingScores={doOpenScoreSetterModal}
            onStartMeeting={doStartMeeting}
            onCancelMeeting={doCancelMeeting}
          />
        )} */}
        {turnNbToMeetings.reverse().map((meetingSplitsInfo) => (
          <RankingsTurn
            key={meetingSplitsInfo.turnNb!}
            style={{ ...Layout.size("100%", "fit-content") }}
            startsCollapsed={meetingSplitsInfo.turnNb! !== config.turn_nb}
            config={config}
            turnNb={meetingSplitsInfo.turnNb!}
            allTeams={allTeams}
            allMeetings={allMeetings}
            meetingsInfoPerSplit={meetingSplitsInfo}
            availableFields={availableFields}
            selectedMeetingStatusFilter={selectedMeetingStatusFilter}
            onSetMeetingScores={doOpenScoreSetterModal}
            onStartMeeting={doStartMeeting}
            onCancelMeeting={doCancelMeeting}
          />
        ))}
      </div>
      <div
        className="bbox"
        style={{
          padding: 16,
          gridArea: "rightpane",
          ...Layout.flex("column", "100%", 16),
          ...Appearance.boundSize(450, "100vh"),
          overflowY: "scroll",
          backgroundColor: palette.background.dp04,
          borderLeft: `2px solid ${palette.background.dp05}`,
        }}
      >
        <RankingsPageFilters
          isCurrentTurnChecked={isCurrentTurnOnyChecked}
          onToggleCurrentTurn={doToggleCurrentTurnOnly}
          selectedMeetingStatusFilter={selectedMeetingStatusFilter}
          onSelectMeetingStatusFilter={selectMeetingStatusFilter}
        />
        <HorizontalSeparator />
        <RankingsPageActions
          canStartNewTurn={canStartNewTurn}
          onStartNewTurn={doStartNewTurn}
          canCancelNewTurn={canCancelNewTurn}
          onCancelNewTurn={doCancelNewTurn}
          canStartPoolsPhase={canStartPoolsPhase}
          onStartPoolsPhase={doStartPoolsPhase}
        />
      </div>
    </>
  );

  function doToggleCurrentTurnOnly(): void {
    setIsCurrentTurnOnyChecked(!isCurrentTurnOnyChecked);
  }

  function doStartNewTurn(): void {
    ConfigRepository.incrementTurn();
    _reloadRepositoryState();
    persistState(LocalStorageEvent.doStartNewTurn, `Ajout d'un nouveau tour`);
  }

  function doStartPoolsPhase(): void {
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doStartPoolsPhase,
      `Lancement de la phase de poules`
    );
  }

  function doCancelNewTurn(): void {
    ConfigRepository.decrementTurn();
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doCancelNewTurn,
      `Annulation d'un nouveau tour`
    );
  }

  function doStartMeeting(
    team1Id: number,
    team2Id: number,
    field1: Field,
    optField2: Field | undefined,
    turnNb: number
  ): void {
    const team1Name = TeamRepository.getById(team1Id).name;
    const team2Name = TeamRepository.getById(team2Id).name;
    MeetingRepository.startNew(team1Id, team2Id, field1, optField2, turnNb);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doStartMeeting,
      `Début de la rencontre ${team1Name} vs ${team2Name}`
    );
  }

  function doSetMeetingScore(
    meetingId: number,
    team1SinglesScore: BadmintonScorePoints,
    team2SinglesScore: BadmintonScorePoints,
    team1DoublesScore: BadmintonScorePoints,
    team2DoublesScore: BadmintonScorePoints
  ): void {
    const meeting = MeetingRepository.get(meetingId);
    const team1Name = TeamRepository.getById(meeting.team1_id).name;
    const team2Name =
      meeting.team2_id !== undefined
        ? TeamRepository.getById(meeting.team2_id).name
        : "W/O";

    setMeetingIsWhoseScoresAreBeingSet(undefined);
    MeetingRepository.setScores(
      meetingId,
      team1SinglesScore,
      team1DoublesScore,
      team2SinglesScore,
      team2DoublesScore
    );
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doSetMeetingScore,
      `Saisie du score pour la rencontre ${team1Name} vs ${team2Name}: ${team1SinglesScore}-${team2SinglesScore}, ${team1DoublesScore}-${team2DoublesScore}`
    );
  }

  function doOpenScoreSetterModal(meetingId: number): void {
    setMeetingIsWhoseScoresAreBeingSet(meetingId);
  }

  function doCloseScoreSetterModal(): void {
    setMeetingIsWhoseScoresAreBeingSet(undefined);
  }

  function doCancelMeeting(meetingId: number): void {
    const meeting = MeetingRepository.get(meetingId);
    const team1Name = TeamRepository.getById(meeting.team1_id).name;
    const team2Name =
      meeting.team2_id !== undefined
        ? TeamRepository.getById(meeting.team2_id).name
        : "W/O";
    MeetingRepository.cancelMeeting(meetingId);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doCancelMeeting,
      `Annulation de la rencontre ${team1Name} vs ${team2Name}`
    );
  }

  /**
   * Modifying the application state via repository RW functions does not trigger a React component re-render, because states
   * are not used in the Repository layer.
   * <br><br><br>
   * Call this function whenever an action method uses repository function which modifies the application state
   * to re-fetch the repository data and update the associated React state variables, which in turn provokes
   * a component re-render. */
  function _reloadRepositoryState(): void {
    setAllMeetings(MeetingRepository.getAll());
    setAvailableFields(FieldRepository.getAvailableFields());
    setConfig(ConfigRepository.get());
  }
}
