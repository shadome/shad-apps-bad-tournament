import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";

export const POOLS_PHASE_PATH: string = "/pools_phase";

export default function PoolsPhasePage(): JSX.Element {
  return (
    <div
      className="bbox"
      style={{
        padding: 16,
        gridArea: "content",
        overflowY: "scroll",
        ...Appearance.boundHeight("100vh"),
        ...Layout.flex("row", "100%", 16, undefined, undefined, "wrap"),
      }}
    >
      Pools phase page (TODO)
    </div>
  );
}
