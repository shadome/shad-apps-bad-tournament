import React, { ComponentProps } from "react";
import { textColour } from "../CssInterop";
import { CONFIG_PAGE_PATH } from "./ConfigPage";
import { RANKINGS_PHASE_PATH } from "./RankingsPhasePage";
import { POOLS_PHASE_PATH } from "./PoolsPhasePage";
import { ADMIN_PAGE_PATH } from "./AdminPage";
import * as Router from "react-router-dom";
import Logo from "./menu_vertical_bar/Logo";
import TabConfig from "./menu_vertical_bar/TabConfig";
import TabPhasePools from "./menu_vertical_bar/TabPhasePools";
import TabPhaseRankings from "./menu_vertical_bar/TabPhaseRankings";
import TabAdmin from "./menu_vertical_bar/TabAdmin";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";
import * as ConfigRepository from "../../repositories/tournamentConfig";
import { TournamentPhase } from "../../core/types";

const MENU_ENTRY_VALUES = [
  "configuration",
  "ranking",
  "pools",
  "admin",
] as const;

function pathNameToMenuEntryValue(pathName: string): MenuEntry {
  switch (pathName) {
    case RANKINGS_PHASE_PATH:
      return "ranking";
    case POOLS_PHASE_PATH:
      return "pools";
    case ADMIN_PAGE_PATH:
      return "admin";
    case CONFIG_PAGE_PATH:
    default:
      return "configuration";
  }
}

export type MenuEntry = (typeof MENU_ENTRY_VALUES)[number];

interface Props extends ComponentProps<"div"> {}

export default function MenuVerticalBar({ ...props }: Props): JSX.Element {
  const navigate = Router.useNavigate();
  const location = Router.useLocation();

  // Dev note: not sure about the component state lifecycle but having an
  // initial state value depending on a hook seems to work
  const [selectedMenuEntry, setSelectedMenuEntry] = React.useState<MenuEntry>(
    pathNameToMenuEntryValue(location.pathname)
  );

  const isConfigSelected = selectedMenuEntry === "configuration";
  const isPhase1Selected = selectedMenuEntry === "ranking";
  const isPhase2Selected = selectedMenuEntry === "pools";
  const isAdminSelected = selectedMenuEntry === "admin";

  const config = ConfigRepository.get();

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", undefined, 16, "center", "start"),
        ...Appearance.boundSize("100%", "100vh"),
        boxSizing: "border-box",
        color: textColour.soft,
        paddingLeft: 4,
        overflowY: "scroll",
      }}
    >
      <Logo
        style={{
          alignSelf: "start",
          marginBottom: 16,
        }}
      />
      <TabPhaseRankings
        isSelected={isPhase1Selected}
        disabled={config.current_phase === TournamentPhase.Setup}
        style={{ width: "100%" }}
        onClick={doPhase1Click}
      />
      <TabPhasePools
        isSelected={isPhase2Selected}
        disabled={config.current_phase !== TournamentPhase.Pools}
        style={{ width: "100%" }}
        onClick={doPhase2Click}
      />
      <TabConfig
        isSelected={isConfigSelected}
        style={{ width: "100%", marginTop: "auto" }}
        onClick={doConfigClick}
      />
      <TabAdmin
        isSelected={isAdminSelected}
        style={{ width: "100%", marginBottom: 4 }}
        onClick={doAdminClick}
      />
    </div>
  );

  function doConfigClick(): void {
    if (selectedMenuEntry !== "configuration") {
      navigate(CONFIG_PAGE_PATH);
      setSelectedMenuEntry("configuration");
    }
  }

  function doPhase1Click(): void {
    if (selectedMenuEntry !== "ranking") {
      navigate(RANKINGS_PHASE_PATH);
      setSelectedMenuEntry("ranking");
    }
  }

  function doPhase2Click(): void {
    if (selectedMenuEntry !== "pools") {
      navigate(POOLS_PHASE_PATH);
      setSelectedMenuEntry("pools");
    }
  }

  function doAdminClick(): void {
    if (selectedMenuEntry !== "admin") {
      navigate(ADMIN_PAGE_PATH);
      setSelectedMenuEntry("admin");
    }
  }
}
