import { ComponentProps } from "react";
import { colours, palette } from "../../CssInterop";
import { SettingsKnobsIcon } from "../../components/Icons";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";

interface Props extends ComponentProps<"button"> {
  isSelected: boolean;
}

export default function TabConfig({
  isSelected,
  ...props
}: Props): JSX.Element {
  return (
    <button
      {...props}
      className={`${props?.className} unset disable`}
      style={{
        ...props?.style,
        ...Layout.flex("column", undefined, 4, "center"),
        ...Appearance.boundSize("100%", "fit-content"),
        boxSizing: "border-box",
        ...(isSelected
          ? {
              cursor: "default",
              padding: "8px 16px 8px 12px",
              borderLeft: `solid 4px ${colours.info.foreground}`,
              borderRadius: "4px 0px 0px 4px",
              color: colours.info.foreground,
              backgroundColor: palette.background.dp05,
            }
          : {
              cursor: "pointer",
              padding: "8px 16px",
            }),
      }}
    >
      <SettingsKnobsIcon
        style={{
          ...Appearance.boundSize(80),
          marginBottom: 4,
        }}
      />
      <span
        style={{
          fontWeight: 800,
          textTransform: "uppercase",
          fontSize: 14,
        }}
      >
        Mise en place
      </span>
    </button>
  );
}
