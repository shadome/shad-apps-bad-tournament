import { ComponentProps } from "react";
import { colours } from "../../CssInterop";
import { BadmintonShuttlecockIcon } from "../../components/Icons";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";

interface Props extends ComponentProps<"div"> {}

export default function Logo({ ...props }: Props): JSX.Element {
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", undefined, 0, "center"),
        ...Appearance.boundSize("fit-content"),
        boxSizing: "border-box",
        padding: "8px 16px 8px 16px",
        color: colours.info.foreground,
      }}
    >
      <BadmintonShuttlecockIcon
        style={{
          ...Appearance.boundSize(80),
          marginBottom: 4,
        }}
      />
      <span
        style={{
          fontWeight: 800,
          textTransform: "uppercase",
          fontSize: 14,
          position: "relative",
          top: -30,
          left: 40,
          maxHeight: 0,
          overflow: "visible",
          transform: "rotate(-30deg)",
          cursor: "default",
        }}
      >
        U.S.E.
      </span>
    </div>
  );
}
