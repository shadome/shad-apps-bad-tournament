import { ComponentProps } from "react";
import { colours, palette } from "../../CssInterop";
import { AutoRepairIcon } from "../../components/Icons";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";

interface Props extends ComponentProps<"button"> {
  isSelected: boolean;
}

export default function TabAdmin({ isSelected, ...props }: Props): JSX.Element {
  return (
    <button
      {...props}
      className={`${props?.className} unset disable`}
      style={{
        ...props?.style,
        ...Layout.flex("column", undefined, 4, "center", undefined),
        ...Appearance.boundSize("100%", "fit-content"),
        boxSizing: "border-box",
        ...(isSelected
          ? {
              cursor: "default",
              padding: "8px 16px 8px 12px",
              borderLeft: `solid 4px ${colours.warning.foreground}`,
              borderRadius: "4px 0px 0px 4px",
              color: colours.warning.foreground,
              backgroundColor: palette.background.dp05,
            }
          : {
              cursor: "pointer",
              padding: "8px 16px",
            }),
      }}
    >
      <AutoRepairIcon
        style={{
          ...Appearance.boundSize(80),
          marginBottom: 4,
          transform: "rotate(180deg)",
        }}
      />
      <span
        style={{
          fontWeight: 800,
          textTransform: "uppercase",
          fontSize: 12,
        }}
      >
        Administration
      </span>
    </button>
  );
}
