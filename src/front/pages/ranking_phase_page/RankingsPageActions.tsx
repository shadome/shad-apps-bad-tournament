import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  AddTaskIconOutlined,
  ChevronDownIcon,
  ChevronUpIcon,
  CircledCrossIcon,
  LockOpenIconOutlined,
} from "../../components/Icons";
import HorizontalSeparator from "../../components/HorizontalSeparator";

interface Props extends ComponentProps<"div"> {
  canStartNewTurn: boolean;
  canCancelNewTurn: boolean;
  canStartPoolsPhase: boolean;
  onStartNewTurn: () => void;
  onCancelNewTurn: () => void;
  onStartPoolsPhase: () => void;
}

export default function RankingsPageActions({
  canStartNewTurn,
  canCancelNewTurn,
  canStartPoolsPhase,
  onStartNewTurn,
  onCancelNewTurn,
  onStartPoolsPhase,
  ...props
}: Props): JSX.Element {
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(false);

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        onClick={doToggleCollapse}
        style={{
          ...Layout.flex("row", "100%", 8, "start"),
          boxSizing: "border-box",
          cursor: "pointer",
        }}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Actions</span>
      </div>
      {!isCollapsed && (
        <div
          style={{
            ...Layout.flex("column", "fit-content", 16, "left"),
            boxSizing: "border-box",
            width: "100%",
          }}
        >
          <button
            onClick={doStartNewTurn}
            disabled={!canStartNewTurn}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.success.foreground,
            }}
          >
            <AddTaskIconOutlined side={iconSize.sm} />
            Lancer un nouveau tour
          </button>
          <button
            onClick={doCancelNewTurn}
            disabled={!canCancelNewTurn}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.error.foreground,
            }}
          >
            <CircledCrossIcon side={iconSize.sm} />
            Annuler le lancement du dernier tour
          </button>
          <HorizontalSeparator />
          <button
            onClick={doLockAndStartPoolsPhase}
            disabled={!canStartPoolsPhase}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.warning.foreground,
            }}
          >
            <LockOpenIconOutlined side={iconSize.sm} />
            Verrouiller et lancer les poules
          </button>
        </div>
      )}
    </div>
  );

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }

  function doCancelNewTurn(): void {
    if (!canCancelNewTurn) {
      return;
    }
    onCancelNewTurn();
  }

  function doStartNewTurn(): void {
    if (!canStartNewTurn) {
      return;
    }
    onStartNewTurn();
  }

  function doLockAndStartPoolsPhase(): void {
    const confirmationMessage =
      "Voulez-vous verrouiller la phase de classement et utiliser le classement actuel pour démarrer la phase de poules ? Cette action est irréversible.";

    if (!window.confirm(confirmationMessage)) {
      return;
    }
    onStartPoolsPhase();
  }
}
