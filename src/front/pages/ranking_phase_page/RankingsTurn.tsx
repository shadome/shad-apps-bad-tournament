import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  FormatListNumberedIcon,
} from "../../components/Icons";
import { Field, Meeting, Team, TournamentConfig } from "../../../core/types";
import {
  TeamsForNewMeeting,
  isMeetingComplete,
} from "../../../core/functions/utils";
import MeetingItemComplete from "./MeetingItemComplete";
import MeetingItemOngoing from "./MeetingItemOngoing";
import MeetingItemReadyToStart from "./MeetingItemReadyToStart";
import { FilterMeetingStatus } from "../RankingsPhasePage";
import Modal from "../../components/Modal";
import RankingsDisplay from "./RankingsDisplay";
import getCurrentRankings from "../../../core/functions/getCurrentRankings";

const LETTERS = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"];

function isExistingMeeting(
  meetingInfo: TeamsForNewMeeting | Meeting
): meetingInfo is Meeting {
  // TeamsForNewMeeting has no field called "id"
  return "id" in meetingInfo;
}

interface Props extends ComponentProps<"div"> {
  startsCollapsed: boolean;
  allTeams: Team[];
  turnNb: number;
  config: TournamentConfig;
  allMeetings: Meeting[];
  availableFields: Field[];
  selectedMeetingStatusFilter: FilterMeetingStatus;
  meetingsInfoPerSplit: ((TeamsForNewMeeting | Meeting)[] | undefined)[];
  onSetMeetingScores: (meetingId: number) => void;
  onCancelMeeting: (meetingId: number) => void;
  onStartMeeting: (
    team1Id: number,
    team2Id: number,
    field1: Field,
    optField2: Field | undefined,
    turnNb: number
  ) => void;
}

function getTeamFromId(allTeams: Team[], id: number): Team {
  return allTeams.find((x) => x.id === id)!;
}

export default function RankingsTurn({
  startsCollapsed,
  allTeams,
  turnNb,
  config,
  availableFields,
  selectedMeetingStatusFilter,
  meetingsInfoPerSplit,
  allMeetings,
  onStartMeeting,
  onSetMeetingScores,
  onCancelMeeting,
  ...props
}: Props) {
  const [isCollapsed, setIsCollapsed] =
    React.useState<boolean>(startsCollapsed);

  const [isRankingDisplayed, setIsRankingDisplayed] =
    React.useState<boolean>(false);

  const caption =
    config.turn_nb === turnNb
      ? `(en cours)`
      : config.turn_nb > turnNb
      ? `(terminé)`
      : config.turn_nb < turnNb
      ? `(anticipation)`
      : "";

  // whenever the startsCollapsed props changes value, e.g. when adding or removing
  // a rankings turn, change the collapsed value
  React.useEffect(() => {
    setIsCollapsed(startsCollapsed);
  }, [startsCollapsed]);

  const rankings = getCurrentRankings(allTeams, allMeetings, turnNb);

  return (
    <>
      <Modal onClose={doCloseRankingsModal} isOpen={isRankingDisplayed}>
        {isRankingDisplayed && (
          <RankingsDisplay
            turnNb={turnNb}
            rankings={rankings}
            onClose={doCloseRankingsModal}
          />
        )}
      </Modal>
      <div
        {...props}
        style={{
          ...props?.style,
          ...Layout.flex("column", "fit-content", 16, "start"),
          boxSizing: "border-box",
          color: palette.foreground.dp04,
          padding: 16,
          borderRadius: 4,
        }}
      >
        <div
          style={{
            ...Layout.flex("row", "100%", 8, "start"),
            height: "fit-content",
            boxSizing: "border-box",
            cursor: "pointer",
          }}
        >
          {isCollapsed ? (
            <ChevronUpIcon onClick={doToggleCollapse} side={iconSize.md} />
          ) : (
            <ChevronDownIcon onClick={doToggleCollapse} side={iconSize.md} />
          )}
          <span
            onClick={doToggleCollapse}
            style={{ ...Layout.flex("row", "100%", 8), flexGrow: 1 }}
          >
            {`Tour ${turnNb}`}
            <span style={{ color: colours.neutral.foreground }}>{caption}</span>
          </span>
          <button
            onClick={doOpenRankingsModal}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "fit-content", 8, "center", "left"),
              ...Appearance.boundSize("fit-content", 48),
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: palette.foreground.dp06,
              alignSelf: "flex-end",
              marginLeft: "auto",
            }}
          >
            <FormatListNumberedIcon side={iconSize.sm} />
            Afficher le classement suite à ce tour
          </button>
        </div>
        {!isCollapsed && (
          <div
            style={{
              ...Layout.flex("column", "fit-content", 16, "left"),
              boxSizing: "border-box",
              width: "100%",
            }}
          >
            {meetingsInfoPerSplit.map(
              (meetingInfos, idx) =>
                meetingInfos !== undefined && (
                  // Note that the Idx can be used in the key prop because it has a functional meaning: it is the turn nb
                  <fieldset
                    key={`Groupe ${LETTERS[idx % LETTERS.length]} ${idx}`}
                    style={{
                      boxSizing: "border-box",
                      border: `1px solid ${palette.background.dp06}`,
                      width: "100%",
                      padding: 4,
                    }}
                  >
                    <legend
                      style={{
                        padding: "0px 8px",
                        textTransform: "uppercase",
                        fontSize: fontSize.tn,
                        color: palette.foreground.dp06,
                        fontWeight: "bolder",
                      }}
                    >{`Groupe ${LETTERS[idx % LETTERS.length]}`}</legend>
                    <div
                      style={{
                        boxSizing: "border-box",
                        padding: 16,
                        gridArea: "content",
                        overflowY: "scroll",
                        width: "100%",
                        display: "grid",
                        gap: 16,
                        gridTemplateColumns:
                          "repeat(auto-fill,minmax(350px, 1fr))",
                      }}
                    >
                      {meetingInfos.map((meetingInfo) =>
                        mustDisplayCompleteMeetingCard(meetingInfo) ? (
                          <MeetingItemComplete
                            key={(meetingInfo as Meeting).id}
                            style={{ flexGrow: 1, flexBasis: 0 }}
                            meeting={meetingInfo as Meeting}
                            canMeetingScoreBeModified={
                              config.turn_nb === turnNb
                            }
                            allTeams={allTeams}
                            onModifyMeetingScores={onSetMeetingScores}
                          />
                        ) : mustDisplayOngoingMeetingCard(meetingInfo) ? (
                          <MeetingItemOngoing
                            key={(meetingInfo as Meeting).id}
                            style={{ flexGrow: 1, flexBasis: 0 }}
                            allTeams={allTeams}
                            meeting={meetingInfo as Meeting}
                            onSetMeetingScores={onSetMeetingScores}
                            onCancelMeeting={onCancelMeeting}
                          />
                        ) : mustDisplayReadyToStartMeetingCard(meetingInfo) ? (
                          <MeetingItemReadyToStart
                            key={`${meetingInfo.team1_id} ${meetingInfo.team2_id} ${meetingInfo.ranking_turn_nb}`}
                            style={{ flexGrow: 1, flexBasis: 0 }}
                            availableFields={availableFields}
                            turnNb={meetingInfo.ranking_turn_nb!}
                            team1={getTeamFromId(
                              allTeams,
                              meetingInfo.team1_id
                            )}
                            team2={getTeamFromId(
                              allTeams,
                              meetingInfo.team2_id!
                            )}
                            onStartMeeting={onStartMeeting}
                          />
                        ) : (
                          <></>
                        )
                      )}
                    </div>
                  </fieldset>
                )
            )}
          </div>
        )}
      </div>
    </>
  );

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }

  function mustDisplayCompleteMeetingCard(
    meetingInfo: TeamsForNewMeeting | Meeting
  ): boolean {
    const doesSelectedMeetingStatusMatch = [
      FilterMeetingStatus.All,
      FilterMeetingStatus.Complete,
    ].includes(selectedMeetingStatusFilter);
    return (
      doesSelectedMeetingStatusMatch &&
      isExistingMeeting(meetingInfo) &&
      isMeetingComplete(meetingInfo)
    );
  }

  function mustDisplayOngoingMeetingCard(
    meetingInfo: TeamsForNewMeeting | Meeting
  ): boolean {
    const doesSelectedMeetingStatusMatch = [
      FilterMeetingStatus.All,
      FilterMeetingStatus.Ongoing,
    ].includes(selectedMeetingStatusFilter);
    return (
      doesSelectedMeetingStatusMatch &&
      isExistingMeeting(meetingInfo) &&
      !isMeetingComplete(meetingInfo)
    );
  }

  function mustDisplayReadyToStartMeetingCard(
    meetingInfo: TeamsForNewMeeting | Meeting
  ): boolean {
    const doesSelectedMeetingStatusMatch = [
      FilterMeetingStatus.All,
      FilterMeetingStatus.ToStart,
    ].includes(selectedMeetingStatusFilter);
    return (
      doesSelectedMeetingStatusMatch &&
      meetingInfo.team2_id !== undefined &&
      !isExistingMeeting(meetingInfo)
    );
  }

  function doOpenRankingsModal(): void {
    setIsRankingDisplayed(true);
  }

  function doCloseRankingsModal(): void {
    setIsRankingDisplayed(false);
  }
}
