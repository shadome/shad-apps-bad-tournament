import React, { ComponentProps } from "react";
import { Field, Team } from "../../../core/types";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import { RacketIconOutlined, SportCourtIcon } from "../../components/Icons";
import * as FieldRepository from "../../../repositories/fields";
import VersusTag from "./meeting_item/VersusTag";
import TeamNameWithSeeding from "./meeting_item/TeamNameWithSeeding";

const SELECTED_FIELD_DEFAULT_VALUE: string = "";

interface Props extends ComponentProps<"div"> {
  team1: Team;
  team2: Team;
  turnNb: number;
  availableFields: Field[];
  onStartMeeting: (
    team1Id: number,
    team2Id: number,
    field1: Field,
    optField2: Field | undefined,
    turnNb: number
  ) => void;
}

interface FieldInfo {
  isAvailable: boolean;
  key: string;
  displayString: string;
}

function areFieldsAvailable(
  availableFields: Field[],
  ...fields: (Field | undefined)[]
) {
  for (const field of fields) {
    if (field !== undefined && !availableFields.includes(field)) {
      return false;
    }
  }
  return true;
}

export default function MeetingItemReadyToStart({
  team1,
  team2,
  turnNb,
  availableFields,
  onStartMeeting,
  ...props
}: Props): JSX.Element {
  const [selectedField1, setSelectedField1] = React.useState<string>(
    SELECTED_FIELD_DEFAULT_VALUE
  );
  const [selectedField2, setSelectedField2] = React.useState<string>(
    SELECTED_FIELD_DEFAULT_VALUE
  );

  const fieldInfos: FieldInfo[] = FieldRepository.getAll().map((field) => ({
    isAvailable: availableFields.includes(field),
    key: field as string,
    displayString: FieldRepository.toString(field),
  }));

  const isOptionalFieldDropdownDisabled = !Boolean(selectedField1);
  const canStartMeeting = Boolean(selectedField1);

  React.useEffect(() => {
    const field1 = FieldRepository.fromStr(selectedField1);
    const optField2 =
      selectedField2 === SELECTED_FIELD_DEFAULT_VALUE
        ? undefined
        : FieldRepository.fromStr(selectedField2);
    if (!areFieldsAvailable(availableFields, field1, optField2)) {
      setSelectedField1(SELECTED_FIELD_DEFAULT_VALUE);
      setSelectedField2(SELECTED_FIELD_DEFAULT_VALUE);
    }
  }, [availableFields]);

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Appearance.boundWidth(350, 500),
        height: "fit-content",
        display: "inline-grid",
        gridTemplate: `
          "tags tags" min-content
          "vs teamname1" min-content
          "vs teamname2" min-content
          "fields fields" min-content
          "actions actions" min-content
          / min-content 1fr`,
        justifyItems: "left",
        boxSizing: "border-box",
        backgroundColor: palette.background.dp04,
        border: `2px solid ${palette.background.dp05}`,
        color: palette.foreground.dp04,
        padding: 10,
        borderRadius: 4,
        rowGap: 8,
        columnGap: 16,
        cursor: "default",
      }}
    >
      <div
        style={{
          ...Appearance.boundSize("fit-content"),
          ...Layout.size("fit-content"),
          gridArea: "tags",
          padding: "2px 4px",
          borderRadius: 4,
          fontSize: fontSize.xs,
          fontWeight: "bolder",
          color: colours.success.foreground,
          backgroundColor: colours.success.background,
        }}
      >
        À lancer
      </div>
      <VersusTag
        style={{
          placeSelf: "center start",
          gridArea: "vs",
        }}
      />
      <TeamNameWithSeeding style={{ gridArea: "teamname1" }} optTeam={team1} />
      <TeamNameWithSeeding style={{ gridArea: "teamname2" }} optTeam={team2} />
      <div
        style={{
          ...Layout.flex("row", "100%", 8, "center"),
          marginTop: 4,
          gridArea: "fields",
          fontSize: fontSize.xs,
          color: palette.foreground.dp05,
        }}
      >
        <SportCourtIcon side={24} />
        <select
          value={selectedField1}
          onChange={doSelectField1}
          className="unset bbox focusable-borderbot"
          style={{
            width: "100%",
            flexGrow: 1,
            height: "fit-content",
            textAlign: "left",
            fontWeight: "bolder",
            padding: "6px 8px",
            backgroundColor: palette.background.dp05,
            cursor: "pointer",
          }}
        >
          <option value={""}>Terrain principal...</option>
          {fieldInfos.map((fieldInfo) => (
            <option
              key={fieldInfo.key}
              value={fieldInfo.key}
              disabled={!fieldInfo.isAvailable}
            >
              {fieldInfo.displayString}
            </option>
          ))}
        </select>
        <select
          value={selectedField2}
          disabled={isOptionalFieldDropdownDisabled}
          onChange={doSelectField2}
          className="unset bbox focusable-borderbot"
          style={{
            width: "100%",
            flexGrow: 1,
            height: "fit-content",
            textAlign: "left",
            padding: "6px 8px",
            backgroundColor: isOptionalFieldDropdownDisabled
              ? undefined
              : palette.background.dp05,
            color: isOptionalFieldDropdownDisabled
              ? palette.background.dp08
              : undefined,
            cursor: isOptionalFieldDropdownDisabled ? "default" : "pointer",
          }}
        >
          <option value={""}>Terrain optionnel...</option>
          {fieldInfos.map((fieldInfo) => (
            <option
              key={fieldInfo.key}
              value={fieldInfo.key}
              disabled={
                !fieldInfo.isAvailable || selectedField1 === fieldInfo.key
              }
            >
              {fieldInfo.displayString}
            </option>
          ))}
        </select>
      </div>
      <div
        style={{
          ...Layout.flex("row", "100%", undefined, "center", "left"),
          width: "100%",
          gridArea: "actions",
        }}
      >
        <button
          onClick={doStartMeeting}
          disabled={!canStartMeeting}
          className="unset bbox hoverable disable"
          style={{
            ...Layout.flex("row", "100%", 8, "center", "left"),
            height: "fit-content",
            alignSelf: "end",
            fontSize: fontSize.sm,
            padding: 8,
            cursor: canStartMeeting ? "pointer" : "default",
            fontWeight: "bold",
            color: colours.success.foreground,
          }}
        >
          <RacketIconOutlined side={iconSize.sm} />
          Lancer la rencontre sur ces terrains
        </button>
      </div>
    </div>
  );

  function doSelectField1(evt: React.ChangeEvent<HTMLSelectElement>): void {
    const field1SelectionNewValue = evt.target.value;
    setSelectedField1(field1SelectionNewValue);
    setSelectedField2(SELECTED_FIELD_DEFAULT_VALUE);
  }

  function doSelectField2(evt: React.ChangeEvent<HTMLSelectElement>): void {
    const field2SelectionNewValue = evt.target.value;
    if (selectedField1 === field2SelectionNewValue) {
      return;
    }
    setSelectedField2(field2SelectionNewValue);
  }

  function doStartMeeting(): void {
    if (!canStartMeeting || selectedField1 === SELECTED_FIELD_DEFAULT_VALUE) {
      return;
    }
    const field1 = FieldRepository.fromStr(selectedField1);
    const optField2 =
      selectedField2 === SELECTED_FIELD_DEFAULT_VALUE
        ? undefined
        : FieldRepository.fromStr(selectedField2);
    if (!areFieldsAvailable(availableFields, field1, optField2)) {
      setSelectedField1(SELECTED_FIELD_DEFAULT_VALUE);
      setSelectedField2(SELECTED_FIELD_DEFAULT_VALUE);
      return;
    }
    onStartMeeting(team1.id, team2.id, field1, optField2, turnNb);
  }
}
