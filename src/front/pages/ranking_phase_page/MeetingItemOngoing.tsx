import React, { ComponentProps } from "react";
import { Field, Meeting, Team } from "../../../core/types";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import { CheckmarkIcon, CrossIcon, EditIcon } from "../../components/Icons";
import VersusTag from "./meeting_item/VersusTag";
import { toHhMm } from "../../../core/functions/utils";
import TeamNameWithSeeding from "./meeting_item/TeamNameWithSeeding";
import * as FieldRepository from "../../../repositories/fields";

interface Props extends ComponentProps<"div"> {
  allTeams: Team[];
  meeting: Meeting;
  onSetMeetingScores: (meetingId: number) => void;
  onCancelMeeting: (meetingId: number) => void;
}

export default function MeetingItemOngoing({
  allTeams,
  meeting,
  onSetMeetingScores,
  onCancelMeeting,
  ...props
}: Props): JSX.Element {
  const team1 = allTeams.find((team) => team.id === meeting.team1_id)!;
  const optTeam2 = allTeams.find((team) => team.id === meeting.team2_id);
  const usedFields: Field[] = [meeting.field, meeting.field_2]
    .filter((x) => x !== undefined)
    .map((x) => x!)
    .sort((a, b) => (a as string).localeCompare(b as string));

  const startedSinceInMinutes = Math.round(
    (new Date().getTime() - meeting.started_at) / 60000
  );

  return (
    <>
      <div
        {...props}
        style={{
          ...props?.style,
          ...Appearance.boundWidth(350, 500),
          height: "fit-content",
          display: "inline-grid",
          gridTemplate: `
          "tags-topleft tags-topleft tags-topright" min-content
          "vs teamname1 teamname1" min-content
          "vs teamname2 teamname2" min-content
          "fields fields fields" min-content
          "actions actions actions" min-content
          / min-content 1fr min-content`,
          justifyItems: "left",
          boxSizing: "border-box",
          backgroundColor: palette.background.dp04,
          border: `2px solid ${palette.background.dp05}`,
          color: palette.foreground.dp04,
          padding: 10,
          borderRadius: 4,
          rowGap: 8,
          columnGap: 16,
          cursor: "default",
        }}
      >
        <div
          style={{
            gridArea: "tags-topleft",
            ...Layout.flex("row", undefined, 8),
          }}
        >
          {usedFields.map((field) => (
            <span
              key={field as string}
              style={{
                ...Appearance.boundSize("fit-content"),
                ...Layout.size("fit-content"),
                padding: "2px 4px",
                borderRadius: 4,
                fontSize: fontSize.xs,
                fontWeight: "bolder",
                color: colours.warning.foreground,
                backgroundColor: colours.warning.background,
              }}
            >
              {FieldRepository.toString(field)}
            </span>
          ))}
        </div>
        <span
          style={{
            ...Appearance.boundSize("fit-content"),
            ...Layout.size("fit-content"),
            gridArea: "tags-topright",
            padding: "2px 4px",
            borderRadius: 4,
            fontWeight: "bolder",
            fontSize: fontSize.tn,
            color: palette.foreground.dp06,
            backgroundColor: palette.background.dp06,
            whiteSpace: "nowrap",
          }}
        >
          {`Début à ${toHhMm(
            new Date(meeting.started_at)
          )} (${startedSinceInMinutes} min)`}
        </span>
        <VersusTag
          style={{
            placeSelf: "center start",
            gridArea: "vs",
          }}
        />
        <TeamNameWithSeeding
          style={{ gridArea: "teamname1" }}
          optTeam={team1}
        />
        <TeamNameWithSeeding
          style={{ gridArea: "teamname2" }}
          optTeam={optTeam2}
        />
        <div
          style={{
            ...Layout.flex("column", "100%", undefined, "center", "left"),
            width: "100%",
            gridArea: "actions",
          }}
        >
          <button
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              height: "fit-content",
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: 8,
              cursor: "pointer",
              color: colours.error.foreground,
            }}
            onClick={doCancelMeeting}
          >
            <CrossIcon side={iconSize.sm} />
            Annuler le lancement de la rencontre
          </button>
          <button
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              height: "fit-content",
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: 8,
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.success.foreground,
            }}
            onClick={doSetMeetingScores}
          >
            <CheckmarkIcon side={iconSize.sm} />
            Saisir les scores pour cette rencontre
          </button>
        </div>
      </div>
    </>
  );

  function doSetMeetingScores(): void {
    onSetMeetingScores(meeting.id);
  }

  function doCancelMeeting(): void {
    const confirmationMessage =
      "Voulez-vous annuler le lancement de la rencontre ? Elle pourra être lancée à nouveau par la suite sur un autre terrain.";
    if (!window.confirm(confirmationMessage)) {
      return;
    }
    onCancelMeeting(meeting.id);
  }
}
