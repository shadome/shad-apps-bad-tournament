import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import { TeamRankingAggregatedInfo } from "../../../core/functions/getCurrentRankings";
import { CrossIcon } from "../../components/Icons";

function hasPlayedEveryMeetingForTheCurrentTurn(
  turnNb: number,
  rankingInfo: TeamRankingAggregatedInfo
): boolean {
  return rankingInfo.total_meetings_played === turnNb;
}

interface Props extends ComponentProps<"div"> {
  // undefined if the rankings are displayed for the pools phase (take every meeting into account)
  turnNb?: number;
  rankings: TeamRankingAggregatedInfo[];
  onClose: () => void;
}

export default function RankingsDisplay({
  turnNb = undefined,
  rankings,
  onClose,
  ...props
}: Props): JSX.Element {
  const [highlightedIdx, setHighlightedIdx] = React.useState<number>(-1);

  const orderedRankings = rankings
    // ordered descendingly
    .sort((a, b) => b.total_score - a.total_score);
  return (
    <div
      style={{
        ...Appearance.boundSize("100%"),
        width: "100%",
        ...Layout.flex("column", "100%", 8),
        backgroundColor: palette.background.dp04,
        border: `2px solid ${palette.background.dp05}`,
        color: palette.foreground.dp04,
        borderRadius: 4,
        padding: "16px 32px 16px 32px",
        cursor: "default",
        alignItems: "center",
        justifyContent: "center",
        boxSizing: "border-box",
      }}
    >
      <table
        style={{
          ...Layout.size("100%"),
          border: `1px solid ${palette.background.dp05}`,
          fontSize: fontSize.xs,
        }}
        align="left"
        cellPadding={6}
        cellSpacing={0}
      >
        <caption
          style={{
            paddingBottom: 16,
            textTransform: "uppercase",
            fontSize: fontSize.sm,
            color: palette.foreground.dp06,
            fontWeight: "bolder",
            textAlign: "left",
          }}
        >{`Classement pour le tour ${turnNb}`}</caption>
        <colgroup>
          <col style={{ width: "fit-content" }} />
          <col style={{ width: "100%" }} />
          <col style={{ width: "fit-content" }} />
          <col style={{ width: "fit-content" }} />
          <col style={{ width: "fit-content" }} />
        </colgroup>
        <tbody>
          <tr>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              #
            </th>
            <th
              style={{ border: `1px solid ${palette.background.dp05}` }}
              align="left"
            >
              Équipe
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Rencontres jouées
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Matchs gagnés
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Matchs perdus
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Points totaux
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Bonus défensif
            </th>
            <th style={{ border: `1px solid ${palette.background.dp05}` }}>
              Malus offensif
            </th>
          </tr>
          {orderedRankings.map((ranking, idx) => (
            <tr
              key={ranking.team.id}
              style={{
                backgroundColor:
                  highlightedIdx === idx ? palette.background.dp04 : "inherit",
              }}
              onMouseEnter={() => {
                setHighlightedIdx(idx);
              }}
              onMouseLeave={() => {
                setHighlightedIdx(-1);
              }}
            >
              <td style={{ border: `1px solid ${palette.background.dp05}` }}>
                {`${idx + 1}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  background: "inherit",
                  fontStyle:
                    turnNb !== undefined &&
                    !hasPlayedEveryMeetingForTheCurrentTurn(turnNb!, ranking)
                      ? "italic"
                      : undefined,
                  fontWeight:
                    turnNb !== undefined &&
                    hasPlayedEveryMeetingForTheCurrentTurn(turnNb!, ranking)
                      ? "bolder"
                      : undefined,
                }}
                align="left"
              >
                {`[${ranking.team.estimated_ranking}] ${ranking.team.name}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  color: colours.info.foreground,
                  fontStyle:
                    turnNb !== undefined &&
                    !hasPlayedEveryMeetingForTheCurrentTurn(turnNb!, ranking)
                      ? "italic"
                      : undefined,
                  fontWeight:
                    turnNb !== undefined &&
                    hasPlayedEveryMeetingForTheCurrentTurn(turnNb!, ranking)
                      ? "bolder"
                      : undefined,
                }}
              >
                {`${ranking.total_meetings_played}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  color: colours.success.foreground,
                }}
              >
                {`${ranking.total_matches_won}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  color: colours.error.foreground,
                }}
              >
                {`${ranking.total_matches_lost}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  fontWeight: "bolder",
                }}
              >
                {`${ranking.total_score}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                }}
              >
                {`+${ranking.total_defensive_bonus}`}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                }}
              >
                {`-${ranking.total_offensive_malus}`}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div
        style={{
          ...Layout.flex("row-reverse", "100%", 16),
          boxSizing: "border-box",
          marginTop: 32,
          gridArea: "buttons",
        }}
      >
        <button
          onClick={onClose}
          className="unset bbox hoverable disable"
          style={{
            ...Layout.flex("row", "fit-content", 12, "center", "left"),
            height: "fit-content",
            alignSelf: "end",
            fontSize: fontSize.md,
            padding: 12,
            cursor: "pointer",
          }}
        >
          <CrossIcon side={iconSize.md} />
          Retour
        </button>
      </div>
    </div>
  );
}
