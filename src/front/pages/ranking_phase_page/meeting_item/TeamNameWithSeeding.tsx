import React, { ComponentProps } from "react";
import * as Appearance from "../../../css/Appearance";
import * as Layout from "../../../css/Layout";
import { colours, fontSize, palette } from "../../../CssInterop";
import { Team } from "../../../../core/types";

interface Props extends ComponentProps<"div"> {
  optTeam: Team | undefined;
}

export default function TeamNameWithSeeding({
  optTeam,
  ...props
}: Props): JSX.Element {
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        fontWeight: "bold",
        color: palette.foreground.dp06,
        fontSize: fontSize.sm,
        textAlign: "left",
        whiteSpace: "nowrap",
        textOverflow: "ellipsis",
        overflow: "hidden",
        ...Appearance.boundWidth("100%"),
        ...Layout.flex("row", undefined, 6, "flex-start"),
      }}
    >
      {optTeam !== undefined && (
        <span
          style={{
            color: palette.background.dp06,
            backgroundColor: palette.foreground.dp06,
            fontWeight: "bolder",
            borderRadius: 4,
            padding: "0px 3px",
            ...Appearance.boundSize("fit-content"),
            ...Layout.size("fit-content"),
            fontSize: fontSize.xt,
          }}
        >
          {`${optTeam!.estimated_ranking}`}
        </span>
      )}
      {optTeam?.name ?? "W/O"}
    </div>
  );
}
