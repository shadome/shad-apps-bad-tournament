import React from "react";
import {
  colours,
  fontSize,
  iconSize,
  palette,
  textColour,
} from "../../../CssInterop";
import {
  BadmintonScorePoints,
  BadmintonScorePointsValues,
  Meeting,
  Team,
} from "../../../../core/types";
import {
  getPossibleSecondHalfOfBadmintonScore,
  isValidBadmintonScore,
} from "../../../../core/functions/utils";
import * as Appearance from "../../../css/Appearance";
import * as Layout from "../../../css/Layout";
import VersusTag from "./VersusTag";
import {
  CircularArrowLeftIcon,
  CrossIcon,
  SaveIcon,
} from "../../../components/Icons";

enum TeamAndCategory {
  // the string litterals should match the field keys of the interface Scores
  Team1Singles = "team1Singles",
  Team1Doubles = "team1Doubles",
  Team2Singles = "team2Singles",
  Team2Doubles = "team2Doubles",
}

interface Scores {
  team1Singles?: BadmintonScorePoints;
  team2Singles?: BadmintonScorePoints;
  team1Doubles?: BadmintonScorePoints;
  team2Doubles?: BadmintonScorePoints;
}

// used to cycle through the different enum values for the currently selected team and category
const ORDERED_TEAMS_AND_CATEGORIES: TeamAndCategory[] = [
  TeamAndCategory.Team1Singles,
  TeamAndCategory.Team2Singles,
  TeamAndCategory.Team1Doubles,
  TeamAndCategory.Team2Doubles,
];

const NO_SCORE_PLACEHOLDER = "??";

const SELECTED_BUTTON_ADDITIONAL_PROPS: React.CSSProperties = {
  fontWeight: 800,
  backgroundColor: palette.foreground.dp05,
  color: textColour.reverse,
  cursor: "default",
};

function getTeamAndCategoryCounterpart(
  teamAndCategory: TeamAndCategory
): TeamAndCategory {
  switch (teamAndCategory) {
    case TeamAndCategory.Team1Singles:
      return TeamAndCategory.Team2Singles;
    case TeamAndCategory.Team1Doubles:
      return TeamAndCategory.Team2Doubles;
    case TeamAndCategory.Team2Singles:
      return TeamAndCategory.Team1Singles;
    case TeamAndCategory.Team2Doubles:
      return TeamAndCategory.Team1Doubles;
  }
}

interface Props {
  meetingId: number;
  allTeams: Team[];
  allMeetings: Meeting[];
  onSubmit: (
    meetingId: number,
    team1SinglesScore: BadmintonScorePoints,
    team2SinglesScore: BadmintonScorePoints,
    team1DoublesScore: BadmintonScorePoints,
    team2DoublesScore: BadmintonScorePoints
  ) => void;
  onClose: () => void;
}

export default function MeetingScoreSetter({
  meetingId,
  allMeetings,
  allTeams,
  onSubmit,
  onClose,
}: Props): JSX.Element {
  // state
  const [scores, setScores] = React.useState<Scores>({});
  const [selectedTeamAndCategoryIdx, setSelectedTeamAndCategoryIdx] =
    React.useState<number>(0);

  // const
  const meeting = allMeetings.find((meeting) => meeting.id === meetingId)!;
  const team1Name = allTeams.find((x) => x.id === meeting.team1_id)!.name;
  const team2Name =
    allTeams.find((x) => x.id === meeting.team2_id)?.name ?? "W/O";

  const selectedTeamAndCategory =
    ORDERED_TEAMS_AND_CATEGORIES[selectedTeamAndCategoryIdx];
  const selectedTeamAndCategoryCounterpart = getTeamAndCategoryCounterpart(
    selectedTeamAndCategory
  );
  const possibleScorePointsForSelection: readonly BadmintonScorePoints[] =
    scores[selectedTeamAndCategoryCounterpart] === undefined
      ? BadmintonScorePointsValues
      : getPossibleSecondHalfOfBadmintonScore(
          scores[selectedTeamAndCategoryCounterpart]!
        );
  const isSubmissionDisabled =
    scores.team1Doubles === undefined ||
    scores.team1Singles === undefined ||
    scores.team2Doubles === undefined ||
    scores.team2Singles === undefined;

  return (
    <div
      style={{
        maxWidth: 900,
        backgroundColor: palette.background.dp04,
        border: `2px solid ${palette.background.dp05}`,
        color: palette.foreground.dp04,
        borderRadius: 4,
        padding: "48px 32px 16px 32px",
        cursor: "default",
        gap: 16,
        display: "inline-grid",
        gridTemplate: `
              "name1 versus name2" min-content
              "singles singles singles" min-content
              "score11 score1spacer score12" min-content
              "doubles doubles doubles " min-content
              "score21 score2spacer score22" min-content
              "score-buttons score-buttons score-buttons" min-content
              "buttons buttons buttons" min-content
              / 1fr min-content 1fr
            `,
        alignItems: "center",
        justifyContent: "center",
        boxSizing: "border-box",
      }}
    >
      <span
        style={{
          marginRight: 32,
          flexGrow: 1,
          flexBasis: 0,
          fontSize: 40,
          textAlign: "end",
          gridArea: "name1",
          color: palette.foreground.dp06,
          fontWeight: "bold",
        }}
      >
        {team1Name}
      </span>
      <VersusTag size="big" style={{ gridArea: "versus" }} />
      <span
        style={{
          marginLeft: 32,
          flexGrow: 1,
          flexBasis: 0,
          fontSize: 40,
          textAlign: "start",
          gridArea: "name2",
          color: palette.foreground.dp06,
          fontWeight: "bold",
        }}
      >
        {team2Name}
      </span>
      <span
        style={{
          gridArea: "singles",
          justifySelf: "start",
          marginLeft: 180,
          marginTop: 16,
          textTransform: "uppercase",
          fontSize: fontSize.sm,
          color: palette.foreground.dp06,
          fontWeight: 800,
        }}
      >
        Simple
      </span>
      <button
        className="unset"
        style={{
          ...Appearance.boundSize(40),
          ...Layout.flex("row", undefined, undefined, "center", "center"),
          cursor: "pointer",
          justifySelf: "end",
          padding: 32,
          fontSize: 70,
          gridArea: "score11",
          ...(selectedTeamAndCategory === TeamAndCategory.Team1Singles
            ? SELECTED_BUTTON_ADDITIONAL_PROPS
            : {}),
        }}
        onClick={() => {
          setSelectedTeamAndCategoryIdx(0);
        }}
      >
        {scores.team1Singles?.toString() ?? NO_SCORE_PLACEHOLDER}
      </button>
      <span
        style={{
          fontSize: fontSize.lg,
          color: colours.info.foreground,
          fontWeight: 800,
          gridArea: "score1spacer",
        }}
      >
        -
      </span>
      <button
        className="unset"
        style={{
          ...Appearance.boundSize(40),
          ...Layout.flex("row", undefined, undefined, "center", "center"),
          cursor: "pointer",
          padding: 32,
          fontSize: 70,
          gridArea: "score12",
          ...(selectedTeamAndCategory === TeamAndCategory.Team2Singles
            ? SELECTED_BUTTON_ADDITIONAL_PROPS
            : {}),
        }}
        onClick={() => {
          setSelectedTeamAndCategoryIdx(1);
        }}
      >
        {scores.team2Singles?.toString() ?? NO_SCORE_PLACEHOLDER}
      </button>
      <span
        style={{
          gridArea: "doubles",
          justifySelf: "start",
          marginTop: 16,
          marginLeft: 180,
          textTransform: "uppercase",
          fontSize: fontSize.sm,
          color: palette.foreground.dp06,
          fontWeight: 800,
        }}
      >
        Double
      </span>
      <button
        className="unset"
        style={{
          ...Appearance.boundSize(40),
          ...Layout.flex("row", undefined, undefined, "center", "center"),
          cursor: "pointer",
          justifySelf: "end",
          padding: 32,
          fontSize: 70,
          gridArea: "score21",
          ...(selectedTeamAndCategory === TeamAndCategory.Team1Doubles
            ? SELECTED_BUTTON_ADDITIONAL_PROPS
            : {}),
        }}
        onClick={() => {
          setSelectedTeamAndCategoryIdx(2);
        }}
      >
        {scores.team1Doubles?.toString() ?? NO_SCORE_PLACEHOLDER}
      </button>
      <span
        style={{
          fontSize: fontSize.lg,
          color: colours.info.foreground,
          fontWeight: 800,
          gridArea: "score2spacer",
        }}
      >
        -
      </span>
      <button
        className="unset"
        style={{
          ...Appearance.boundSize(40),
          ...Layout.flex("row", undefined, undefined, "center", "center"),
          cursor: "pointer",
          padding: 32,
          fontSize: 70,
          gridArea: "score22",
          ...(selectedTeamAndCategory === TeamAndCategory.Team2Doubles
            ? SELECTED_BUTTON_ADDITIONAL_PROPS
            : {}),
        }}
        onClick={() => {
          setSelectedTeamAndCategoryIdx(3);
        }}
      >
        {scores.team2Doubles?.toString() ?? NO_SCORE_PLACEHOLDER}
      </button>
      <div
        style={{
          gridArea: "score-buttons",
          width: "fit-content",
          justifySelf: "center",
          gap: 8,
          marginTop: 16,
          alignItems: "center",
          display: "inline-grid",
          gridTemplateColumns: "1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr 1fr",
        }}
      >
        {BadmintonScorePointsValues.map((points) => (
          <button
            key={points}
            className="unset hoverable disable"
            disabled={!possibleScorePointsForSelection.includes(points)}
            onClick={() => {
              doSelectScore(points);
            }}
            style={{
              ...Appearance.boundSize(64, 48),
              boxSizing: "border-box",
              fontSize: fontSize.sm,
              cursor: "pointer",
              border: `1px solid ${
                points === 21
                  ? palette.foreground.dp06
                  : palette.background.dp08
              }`,
            }}
          >
            {points}
          </button>
        ))}
      </div>
      <div
        style={{
          ...Layout.flex("row-reverse", "100%", 16),
          boxSizing: "border-box",
          marginTop: 32,
          gridArea: "buttons",
        }}
      >
        <button
          onClick={doSubmit}
          disabled={isSubmissionDisabled}
          className="unset bbox hoverable disable"
          style={{
            ...Layout.flex("row", "fit-content", 12, "center", "left"),
            height: "fit-content",
            alignSelf: "end",
            fontSize: fontSize.md,
            padding: 12,
            cursor: "pointer",
            color: colours.success.foreground,
          }}
        >
          <SaveIcon side={iconSize.md} />
          Enregistrer
        </button>
        <button
          onClick={doReset}
          className="unset bbox hoverable disable"
          style={{
            ...Layout.flex("row", "fit-content", 12, "center", "left"),
            height: "fit-content",
            alignSelf: "end",
            fontSize: fontSize.md,
            padding: 12,
            cursor: "pointer",
          }}
        >
          <CircularArrowLeftIcon side={iconSize.md} />
          Réinitialiser
        </button>
        <button
          onClick={goBack}
          className="unset bbox hoverable disable"
          style={{
            ...Layout.flex("row", "fit-content", 12, "center", "left"),
            height: "fit-content",
            alignSelf: "end",
            fontSize: fontSize.md,
            padding: 12,
            cursor: "pointer",
          }}
        >
          <CrossIcon side={iconSize.md} />
          Retour
        </button>
      </div>
    </div>
  );

  function doSelectScore(score: BadmintonScorePoints): void {
    const nextSelectedIdx =
      (selectedTeamAndCategoryIdx + 1) % ORDERED_TEAMS_AND_CATEGORIES.length;
    const canSetCounterpartAutomatically =
      // the next selection is the current team and category counterpart
      ORDERED_TEAMS_AND_CATEGORIES[nextSelectedIdx] ===
        selectedTeamAndCategoryCounterpart &&
      // there is only one possible score for this next selection
      getPossibleSecondHalfOfBadmintonScore(score).length === 1;
    if (canSetCounterpartAutomatically) {
      setScores({
        ...scores,
        [selectedTeamAndCategory]: score,
        [selectedTeamAndCategoryCounterpart]:
          getPossibleSecondHalfOfBadmintonScore(score)[0],
      });
      const nextNextSelectedIdx =
        (selectedTeamAndCategoryIdx + 2) % ORDERED_TEAMS_AND_CATEGORIES.length;
      setSelectedTeamAndCategoryIdx(nextNextSelectedIdx);
    } else {
      setScores({
        ...scores,
        [selectedTeamAndCategory]: score,
      });
      setSelectedTeamAndCategoryIdx(nextSelectedIdx);
    }
  }

  function doReset(): void {
    setScores({});
    setSelectedTeamAndCategoryIdx(0);
  }

  function doSubmit(): void {
    const isAnyScoreUndefined =
      scores.team1Singles === undefined ||
      scores.team1Doubles === undefined ||
      scores.team2Singles === undefined ||
      scores.team2Doubles === undefined;
    if (isAnyScoreUndefined) {
      return;
    }
    const isValidBadmintonScore1 = isValidBadmintonScore(
      scores.team1Singles!,
      scores.team2Singles!
    );
    const isValidBadmintonScore2 = isValidBadmintonScore(
      scores.team1Doubles!,
      scores.team2Doubles!
    );
    if (!isValidBadmintonScore1 || !isValidBadmintonScore2) {
      return;
    }
    onSubmit(
      meetingId,
      scores.team1Singles!,
      scores.team2Singles!,
      scores.team1Doubles!,
      scores.team2Doubles!
    );
  }

  function goBack(): void {
    doReset();
    onClose();
  }
}
