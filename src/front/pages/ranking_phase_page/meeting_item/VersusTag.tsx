import React, { ComponentProps } from "react";
import * as Appearance from "../../../css/Appearance";
import { fontSize, palette } from "../../../CssInterop";
import { BlastIcon } from "../../../components/Icons";

type Size = "small" | "big";

interface Props extends ComponentProps<"div"> {
  size?: Size;
}

export default function VersusTag({
  size = "small",
  ...props
}: Props): JSX.Element {
  const side = size === "big" ? 60 : 40;
  const _fontSize = size === "big" ? fontSize.xl : fontSize.md;
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Appearance.boundSize("fit-content"),
        display: "grid",
        gridTemplateAreas: "stack",
        justifyContent: "center",
        alignItems: "center",
        color: palette.foreground.dp05,
      }}
    >
      <BlastIcon
        side={side}
        style={{
          zIndex: 0,
          gridArea: "stack",
        }}
      />
      <span
        style={{
          position: "relative",
          top: -2,
          left: 1,
          zIndex: 1,
          gridArea: "stack",
          fontWeight: "bold",
          fontSize: _fontSize,
          color: palette.background.dp03,
        }}
      >
        vs
      </span>
    </div>
  );
}
