import React, { ComponentProps } from "react";
import { fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import { ChevronDownIcon, ChevronUpIcon } from "../../components/Icons";
import Toggle from "../../components/Toggle";
import RadioButtons, { Direction } from "../../components/RadioButtons";
import { FilterMeetingStatus } from "../RankingsPhasePage";

function filterMeetingStatusToString(status: FilterMeetingStatus): string {
  switch (status) {
    case FilterMeetingStatus.All:
      return "Toutes";
    case FilterMeetingStatus.ToStart:
      return "À lancer";
    case FilterMeetingStatus.Ongoing:
      return "En cours";
    case FilterMeetingStatus.Complete:
      return "Terminées";
  }
}

interface Props extends ComponentProps<"div"> {
  isCurrentTurnChecked: boolean;
  onToggleCurrentTurn: () => void;
  selectedMeetingStatusFilter: FilterMeetingStatus;
  onSelectMeetingStatusFilter: (status: FilterMeetingStatus) => void;
}

export default function RankingsPageFilters({
  isCurrentTurnChecked,
  onToggleCurrentTurn,
  selectedMeetingStatusFilter,
  onSelectMeetingStatusFilter,
  ...props
}: Props): JSX.Element {
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(false);

  const meetingStatusValues = Object.values(FilterMeetingStatus)
    .filter((meetingStatus) => !isNaN(Number(meetingStatus)))
    .map((meetingStatus) => ({
      key: Number(meetingStatus),
      displayValue: filterMeetingStatusToString(
        meetingStatus as FilterMeetingStatus
      ),
    }));

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        onClick={doToggleCollapse}
        style={{
          ...Layout.flex("row", "100%", 8, "start"),
          boxSizing: "border-box",
          cursor: "pointer",
        }}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Filtres d'affichage</span>
      </div>
      {!isCollapsed && (
        <div
          style={{
            ...Layout.flex("column", "fit-content", 16, "left"),
            boxSizing: "border-box",
            width: "100%",
          }}
        >
          <div
            style={{
              ...Layout.flex("row", "100%", 16, "center"),
              paddingLeft: 16,
            }}
          >
            <Toggle
              id="current_turn_toggle"
              isChecked={isCurrentTurnChecked}
              onToggle={onToggleCurrentTurn}
            />
            <label htmlFor="current_turn_toggle" style={{ cursor: "pointer" }}>
              Tour courant
            </label>
          </div>
          <fieldset
            style={{
              boxSizing: "border-box",
              border: `1px solid ${palette.background.dp06}`,
              width: "100%",
              padding: 16,
            }}
          >
            <legend
              style={{
                padding: "0px 8px",
                textTransform: "uppercase",
                fontSize: fontSize.tn,
                color: palette.foreground.dp06,
                fontWeight: "bolder",
              }}
            >
              Statut des rencontres
            </legend>
            <RadioButtons
              direction={Direction.Vertical}
              name="meetings_statuses"
              selectedValue={selectedMeetingStatusFilter as number}
              onSelect={doSelectMeetingStatusFilter}
              values={meetingStatusValues}
            />
          </fieldset>
        </div>
      )}
    </div>
  );

  function doSelectMeetingStatusFilter(key: number): void {
    onSelectMeetingStatusFilter(key as FilterMeetingStatus);
  }
  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }
}
