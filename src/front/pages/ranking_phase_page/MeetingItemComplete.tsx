import React, { ComponentProps } from "react";
import { Meeting, Team } from "../../../core/types";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import { fontSize, iconSize, palette, textColour } from "../../CssInterop";
import {
  EditIcon,
  PeopleIconOutlined,
  TwoPeopleIconOutlined,
} from "../../components/Icons";
import VersusTag from "./meeting_item/VersusTag";
import { toHhMm } from "../../../core/functions/utils";
import TeamNameWithSeeding from "./meeting_item/TeamNameWithSeeding";

interface Props extends ComponentProps<"div"> {
  meeting: Meeting;
  canMeetingScoreBeModified: boolean;
  allTeams: Team[];
  onModifyMeetingScores: (meetingId: number) => void;
}

export default function MeetingItemComplete({
  meeting,
  canMeetingScoreBeModified,
  allTeams,
  onModifyMeetingScores,
  ...props
}: Props): JSX.Element {
  const team1 = allTeams.find((team) => team.id === meeting.team1_id)!;
  const optTeam2 = allTeams.find((team) => team.id === meeting.team2_id);

  const isTeam1DoublesScoreBold =
    (meeting.team1_doubles_score ?? 0) > (meeting.team2_doubles_score ?? 0);
  const isTeam2DoublesScoreBold =
    (meeting.team2_doubles_score ?? 0) > (meeting.team1_doubles_score ?? 0);
  const isTeam1SinglesScoreBold =
    (meeting.team1_singles_score ?? 0) > (meeting.team2_singles_score ?? 0);
  const isTeam2SinglesScoreBold =
    (meeting.team2_singles_score ?? 0) > (meeting.team1_singles_score ?? 0);

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Appearance.boundWidth(350, 500),
        height: "fit-content",
        display: "inline-grid",
        gridTemplate: `
          "tags tags singles-icon doubles-icon" min-content
          "vs teamname1 score-singles-team1 score-doubles-team1" min-content
          "vs teamname2 score-singles-team2 score-doubles-team2" min-content
          "footer footer footer footer" min-content
          / min-content 1fr min-content min-content`,
        justifyItems: "left",
        boxSizing: "border-box",
        backgroundColor: palette.background.dp04,
        border: `2px solid ${palette.background.dp05}`,
        color: palette.foreground.dp04,
        padding: 10,
        borderRadius: 4,
        rowGap: 8,
        columnGap: 16,
        cursor: "default",
      }}
    >
      <div
        style={{
          ...Appearance.boundSize("fit-content"),
          ...Layout.size("fit-content"),
          gridArea: "tags",
          padding: "2px 4px",
          borderRadius: 4,
          fontSize: fontSize.tn,
          fontWeight: "bolder",
          color: palette.foreground.dp06,
          backgroundColor: palette.background.dp06,
        }}
      >
        {meeting.ended_at !== undefined &&
          `Fin à ${toHhMm(new Date(meeting.ended_at!))}`}
      </div>
      <PeopleIconOutlined
        side={20}
        style={{
          color: palette.foreground.dp05,
          gridArea: "singles-icon",
        }}
      />
      <TwoPeopleIconOutlined
        side={20}
        style={{
          color: palette.foreground.dp05,
          gridArea: "doubles-icon",
        }}
      />
      <VersusTag
        style={{
          placeSelf: "center start",
          gridArea: "vs",
        }}
      />
      <TeamNameWithSeeding style={{ gridArea: "teamname1" }} optTeam={team1} />
      <TeamNameWithSeeding
        style={{ gridArea: "teamname2" }}
        optTeam={optTeam2}
      />
      <span
        style={{
          gridArea: "score-singles-team1",
          fontWeight: isTeam1SinglesScoreBold ? "bold" : "default",
          fontSize: fontSize.xs,
          color: palette.foreground.dp03,
        }}
      >
        {meeting.team1_singles_score}
      </span>
      <span
        style={{
          gridArea: "score-singles-team2",
          fontWeight: isTeam2SinglesScoreBold ? "bold" : "default",
          fontSize: fontSize.xs,
          color: palette.foreground.dp03,
        }}
      >
        {meeting.team2_singles_score}
      </span>
      <span
        style={{
          gridArea: "score-doubles-team1",
          fontWeight: isTeam1DoublesScoreBold ? "bold" : "default",
          fontSize: fontSize.xs,
          color: palette.foreground.dp03,
        }}
      >
        {meeting.team1_doubles_score}
      </span>
      <span
        style={{
          gridArea: "score-doubles-team2",
          fontWeight: isTeam2DoublesScoreBold ? "bold" : "default",
          fontSize: fontSize.xs,
          color: palette.foreground.dp03,
        }}
      >
        {meeting.team2_doubles_score}
      </span>
      <div
        style={{
          ...Layout.flex("row", "100%", undefined, "center", "left"),
          width: "100%",
          gridArea: "footer",
        }}
      >
        {canMeetingScoreBeModified && (
          <button
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              height: "fit-content",
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: 8,
              cursor: "pointer",
              color: palette.foreground.dp05,
            }}
            onClick={doModifyMeetingScores}
          >
            <EditIcon side={iconSize.sm} />
            Modifier les scores de la rencontre
          </button>
        )}
        {!canMeetingScoreBeModified && (
          <span
            style={{
              paddingTop: 4,
              fontSize: fontSize.xs,
              fontStyle: "italic",
              fontWeight: "bold",
              color: textColour.disabled,
            }}
          >
            Les scores de la rencontre ne sont plus modifiables.
          </span>
        )}
      </div>
    </div>
  );

  function doModifyMeetingScores(): void {
    onModifyMeetingScores(meeting.id);
  }
}
