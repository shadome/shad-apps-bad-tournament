import CssInteropInitialiser, { palette } from "../CssInterop";
import MenuVerticalBar from "./MenuVerticalBar";
import * as Router from "react-router-dom";
import ConfigPage, { CONFIG_PAGE_PATH } from "./ConfigPage";
import RankingsPhasePage, { RANKINGS_PHASE_PATH } from "./RankingsPhasePage";
import PoolsPhasePage, { POOLS_PHASE_PATH } from "./PoolsPhasePage";
import AdminPage, { ADMIN_PAGE_PATH } from "./AdminPage";
import * as Appearance from "../css/Appearance";
import { restoreLastState } from "../../repositories/localstorage";

/*
TODO rework every TSX file and distinguish layout and appearance (esp. for min/max/regular width/height)
TODO make action methods functions inside of the main component functions below the return statement
TODO add more css classes than layout and appearance, in order to group css properties which come together, e.g. for fonts
TODO remove useless CSS rules
TODO rework every HTML or TSX tags so that className and style attributes are at the end of the attributes list
TODO use the palettes everywhere
TODO remove obsolete "side" Icon custom attribute
TODO use bbox and other classes like unset everywhere it is required, and make other very very usual css rules available, if any
TODO change relative to absolute import paths when it is relevant
*/
function App() {
  restoreLastState();
  return (
    <Router.BrowserRouter>
      <CssInteropInitialiser />
      <div
        style={{
          display: "grid",
          gridTemplateAreas: `"menu separator content rightpane"`,
          gridTemplateColumns: "min-content min-content 1fr",
        }}
      >
        <MenuVerticalBar style={{ gridArea: "menu" }} />

        <div
          style={{
            boxSizing: "border-box",
            gridArea: "separator",
            backgroundColor: palette.background.dp05,
            ...Appearance.boundSize(4, "100%"),
          }}
        />

        <Router.Routes>
          <Router.Route path={CONFIG_PAGE_PATH} element={<ConfigPage />} />
          <Router.Route
            path={RANKINGS_PHASE_PATH}
            element={<RankingsPhasePage />}
          />
          <Router.Route path={POOLS_PHASE_PATH} element={<PoolsPhasePage />} />
          <Router.Route path={ADMIN_PAGE_PATH} element={<AdminPage />} />
        </Router.Routes>
        {/* </div> */}
      </div>
    </Router.BrowserRouter>
  );
}

export default App;
