import React from "react";
import * as Teams from "../../repositories/teams";
import Modal from "../components/Modal";
import RadioButtons, { Direction } from "../components/RadioButtons";
import RadioButtonsArray from "../components/RadioButtonsArray";
import Toggle from "../components/Toggle";
import DisplayRankings from "../containers/DisplayRankings";
import MeetingCard from "../containers/MeetingCard";
import MeetingScoreSet from "./ranking_phase_page/meeting_item/MeetingScoreSetter";
import RankingsActions from "../containers/RankingsActions";
import RankingsFilters, {
  FilterMeetingStatus,
} from "../containers/RankingsFilters";
import getCurrentRankings from "../../core/functions/getCurrentRankings";
import { textColour } from "../CssInterop";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";

export const ADMIN_PAGE_PATH: string = "/admin";

export default function AdminPage(): JSX.Element {
  // const all_teams = Teams.getAll();
  // const rankings = getCurrentRankings(all_teams, [], undefined);

  // const [isChecked, setChecked] = React.useState<boolean>(false);
  // const [selectedRadioBtn, setSelectedRadioBtn] =
  //   React.useState<FilterMeetingStatus>(FilterMeetingStatus.All);

  // const [isOpen, setIsOpen] = React.useState<boolean>(false);
  return <></>;
  //   <div
  //     style={{
  //       padding: 16,
  //       gridArea: "content",
  //       overflowY: "scroll",
  //       ...Appearance.boundHeight("100vh"),
  //       ...Layout.flex("row", "100%", 16, undefined, undefined, "wrap"),
  //     }}
  //   >
  //     {/* <Modal
  //       isOpen={isOpen}
  //       onClose={() => {
  //         setIsOpen(false);
  //       }}
  //     >
  //       <MeetingScoreSet onClose={() => {}} onSubmit={() => {}} />
  //     </Modal> */}

  //     <button
  //       className="btn"
  //       onClick={() => {
  //         setIsOpen(!isOpen);
  //       }}
  //     >
  //       Open test modal
  //     </button>

  //     <div className="flexrow g16">
  //       <MeetingCard
  //       // existingMeetingId={1}
  //       />
  //       <MeetingCard existingMeetingId={1} />
  //     </div>
  //     <div className="flexrow g16">
  //       <RankingsFilters
  //         isOnlyCurrentTurn={isChecked}
  //         setIsOnlyCurrentTurn={setChecked}
  //         selectedMeetingStatus={selectedRadioBtn}
  //         setSelectedMeetingStatus={setSelectedRadioBtn}
  //       />

  //       <RankingsActions />
  //     </div>
  //     <DisplayRankings rankingInfos={rankings} />

  //     <fieldset
  //       className="flexcol x-start bbox g16 w100 p16 border"
  //       style={{ borderColor: textColour.regular }}
  //     >
  //       <legend className="px8">Radio buttons test</legend>

  //       <RadioButtonsArray
  //         selectedValue={(selectedRadioBtn as number).toString()}
  //         onSelectValue={(key) => {
  //           setSelectedRadioBtn(Number(key));
  //         }}
  //         values={[
  //           { key: "0", displayValue: "Tout" },
  //           { key: "1", displayValue: "À lancer" },
  //           { key: "2", displayValue: "En cours" },
  //         ]}
  //       />

  //       <RadioButtons
  //         direction={Direction.Horizontal}
  //         name="test_radio_buttons"
  //         selectedValue={selectedRadioBtn}
  //         onSelect={(key) => {
  //           setSelectedRadioBtn(key);
  //         }}
  //         values={[
  //           { key: 0, displayValue: "Tout" },
  //           { key: 1, displayValue: "À lancer" },
  //           { key: 2, displayValue: "En cours" },
  //         ]}
  //       />
  //     </fieldset>
  //     <Toggle
  //       isChecked={isChecked}
  //       onToggle={(checked) => setChecked(checked)}
  //     />
  //     <div className="flexrow g16">
  //       <button className="btn">test</button>
  //       <button className="btn btn-danger">test</button>
  //     </div>
  //   </div>
  // );
}
