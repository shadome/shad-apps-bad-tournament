import React from "react";
import { fontSize, palette } from "../CssInterop";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";
import {
  LocalStorageItem,
  getAllItems,
  purgeStatesBeyondKey,
  restoreStateFromKey,
} from "../../repositories/localstorage";
import { toHhMm } from "../../core/functions/utils";

export const ADMIN_PAGE_PATH: string = "/admin";

export default function AdminPage(): JSX.Element {
  const [events, setEvents] = React.useState<LocalStorageItem[]>(getAllItems());
  const [highlightedIdx, setHighlightedIdx] = React.useState<number>(-1);

  return (
    <div
      className="bbox"
      style={{
        padding: 16,
        gridArea: "content",
        overflowY: "scroll",
        ...Appearance.boundHeight("100vh"),
        ...Layout.flex("column", "100%", 16, undefined, undefined, "wrap"),
      }}
    >
      <table
        style={{
          border: `1px solid ${palette.background.dp05}`,
          fontSize: fontSize.xs,
        }}
        align="left"
        cellPadding={6}
        cellSpacing={0}
      >
        <colgroup>
          <col style={{ width: "fit-content" }} />
          <col style={{ width: "100%" }} />
          <col style={{ width: "fit-content" }} />
        </colgroup>
        <tbody>
          <tr>
            <th
              style={{ border: `1px solid ${palette.background.dp05}` }}
              align="left"
            >
              Heure
            </th>
            <th
              style={{ border: `1px solid ${palette.background.dp05}` }}
              align="left"
            >
              Description de la modification
            </th>
            <th
              style={{ border: `1px solid ${palette.background.dp05}` }}
              align="left"
            >
              Actions
            </th>
          </tr>
          {events.map((event, idx) => (
            <tr
              key={`${event.evt}-${event.evt_date}`}
              style={{
                backgroundColor:
                  highlightedIdx === idx ? palette.background.dp04 : "inherit",
              }}
              onMouseEnter={() => {
                setHighlightedIdx(idx);
              }}
              onMouseLeave={() => {
                setHighlightedIdx(-1);
              }}
            >
              <td
                style={{ border: `1px solid ${palette.background.dp05}` }}
                align="left"
              >
                {toHhMm(new Date(event.evt_date))}
              </td>
              <td
                style={{
                  border: `1px solid ${palette.background.dp05}`,
                  background: "inherit",
                }}
                align="left"
              >
                {event.evt_description}
              </td>
              <td
                style={{ border: `1px solid ${palette.background.dp05}` }}
                align="left"
              >
                {idx > 0 && (
                  <button
                    className="unset"
                    style={{
                      textDecoration: "underline",
                      cursor: "pointer",
                      color: palette.foreground.dp06,
                      fontWeight: "bold",
                      whiteSpace: "nowrap",
                    }}
                    onClick={() => doRestorePreviousState(event.key)}
                  >
                    Revenir à cet état
                  </button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );

  function doRestorePreviousState(key: string): void {
    const confirmation_message =
      "Voulez-vous revenir à l'état sélectionné ? Toutes les modifications ultérieures à cet état seront irrémédiablement et définitivement perdues.";
    if (!window.confirm(confirmation_message)) {
      return;
    }
    restoreStateFromKey(key);
    purgeStatesBeyondKey(key);
    setEvents(getAllItems());
  }
}
