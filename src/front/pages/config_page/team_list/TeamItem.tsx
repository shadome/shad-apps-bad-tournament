import { ComponentProps } from "react";
import { Team } from "../../../../core/types";
import { fontSize, iconSize, palette } from "../../../CssInterop";
import * as Appearance from "../../../css/Appearance";
import * as Layout from "../../../css/Layout";
import {
  ArrowBottomIcon,
  ArrowTopIcon,
  InkSwirlIcon,
} from "../../../components/Icons";

interface Props extends ComponentProps<"div"> {
  team: Team;
  areActionsDisabled: boolean;
  canMoveTeamUp: boolean;
  canMoveTeamDown: boolean;
  onMoveTeamUp: () => void;
  onMoveTeamDown: () => void;
}

export default function TeamItem({
  team,
  areActionsDisabled,
  canMoveTeamUp,
  canMoveTeamDown,
  onMoveTeamUp,
  onMoveTeamDown,
  ...props
}: Props): JSX.Element {
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Appearance.boundWidth(410, 500),
        flexGrow: 1,
        display: "inline-grid",
        gridTemplateAreas: `
            "rank teamname actions"
            "rank players players"`,
        gridTemplateRows: "min-content 1fr",
        gridTemplateColumns: "min-content 1fr min-content",
        justifyItems: "left",
        boxSizing: "border-box",
        backgroundColor: palette.background.dp04,
        border: `2px solid ${palette.background.dp05}`,
        color: palette.foreground.dp04,
        padding: 10,
        borderRadius: 4,
        columnGap: 16,
        rowGap: 6,
        cursor: "default",
      }}
    >
      <div
        style={{
          placeSelf: "center start",
          gridArea: "rank",
          display: "grid",
          gridTemplateAreas: "stack",
          justifyContent: "center",
          alignItems: "center",
          color: palette.foreground.dp06,
          ...Appearance.boundSize("fit-content"),
        }}
      >
        <InkSwirlIcon
          side={40}
          style={{
            zIndex: 0,
            gridArea: "stack",
            transform: "rotate(-80deg)",
          }}
        />
        <span
          style={{
            zIndex: 1,
            gridArea: "stack",
            fontWeight: "bold",
            fontSize: fontSize.md,
          }}
        >
          {team.estimated_ranking}
        </span>
      </div>
      <span
        style={{
          gridArea: "teamname",
          fontWeight: "bold",
          color: palette.foreground.dp06,
          fontSize: fontSize.sm,
          textAlign: "left",
          whiteSpace: "nowrap",
          textOverflow: "ellipsis",
          overflow: "hidden",
          ...Appearance.boundWidth("100%"),
        }}
      >
        {team.name}
      </span>
      <div
        style={{
          gridArea: "players",
          fontSize: fontSize.sm,
          fontStyle: "italic",
          color: palette.foreground.dp04,
          textAlign: "left",
          display: "grid",
          gridTemplateAreas: `"player1 player3" "player2 player4"`,
          columnGap: 16,
          width: "100%",
        }}
      >
        {[
          team.player1Name,
          team.player2Name,
          team.player3Name,
          team.player4Name,
        ]
          .filter((x) => x !== undefined)
          // sort by nb of chars, descendingly
          .sort((a, b) => b!.length - a!.length)
          .map((playerName, i) => (
            <span
              key={playerName}
              style={{
                gridArea: `player${i + 1}`,
                whiteSpace: "nowrap",
                textOverflow: "ellipsis",
                overflow: "hidden",
                ...Appearance.boundWidth("100%"),
              }}
            >
              {`• ${playerName}`}
            </span>
          ))}
      </div>
      {!areActionsDisabled && (
        <div
          style={{
            gridArea: "actions",
            ...Layout.flex("row", "fit-content", 2),
            color: palette.foreground.dp05,
          }}
        >
          <button
            className="unset hoverable disable"
            style={{
              padding: 2,
              ...Appearance.boundSize("fit-content"),
              ...Layout.size("fit-content"),
              ...Layout.flex("row", undefined, undefined, "center", "center"),
              boxSizing: "border-box",
              cursor: "pointer",
            }}
            onClick={doMoveTeamUp}
            disabled={!canMoveTeamUp}
          >
            <ArrowTopIcon style={{ ...Appearance.boundSize(iconSize.md) }} />
          </button>
          <button
            className="unset hoverable disable"
            style={{
              padding: 2,
              ...Appearance.boundSize("fit-content"),
              ...Layout.size("fit-content"),
              ...Layout.flex("row", undefined, undefined, "center", "center"),
              boxSizing: "border-box",
              cursor: "pointer",
            }}
            onClick={doMoveTeamDown}
            disabled={!canMoveTeamDown}
          >
            <ArrowBottomIcon style={{ ...Appearance.boundSize(iconSize.md) }} />
          </button>
        </div>
      )}
    </div>
  );

  function doMoveTeamUp(): void {
    if (!canMoveTeamUp) {
      return;
    }
    onMoveTeamUp();
  }

  function doMoveTeamDown(): void {
    if (!canMoveTeamDown) {
      return;
    }
    onMoveTeamDown();
  }
}
