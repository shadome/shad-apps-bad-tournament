import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import RadioButtonsArray from "../../components/RadioButtonsArray";
import React, { ComponentProps } from "react";
import { PoolsConfigData } from "../ConfigPage";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  PlayIconOutlined,
} from "../../components/Icons";
import {
  LocalStorageEvent,
  persistState,
} from "../../../repositories/localstorage";

interface Props extends ComponentProps<"div"> {
  isDisabled: boolean;
  poolsConfigData: PoolsConfigData[];
  nbOfPools: number | undefined;
  nbOfMeetings: number | undefined;
  onSelectNbOfPools: (nbOfPools: number) => void;
  onSelectNbOfMeetings: (nbOfMeetings: number) => void;
  onValidateConfig: () => void;
}

export default function PoolsConfig({
  isDisabled,
  poolsConfigData,
  nbOfPools,
  nbOfMeetings,
  onSelectNbOfPools,
  onSelectNbOfMeetings,
  onValidateConfig,
  ...props
}: Props) {
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(false);

  const nbOfPoolsValues = poolsConfigData.map((poolConfigData) => ({
    key: poolConfigData.nbOfPools.toString(),
    displayValue: poolConfigData.nbOfPools.toString(),
    isDisabled: isDisabled === true && nbOfPools !== poolConfigData.nbOfPools,
  }));

  const possibleNbOfMeetingsPerPoolValues = poolsConfigData
    .find((data) => data.nbOfPools === nbOfPools)
    ?.possibleNbOfMeetingsForNbOfPools.map(
      (possibleNbOfMeetingsForNbOfPool) => ({
        key: possibleNbOfMeetingsForNbOfPool.toString(),
        displayValue: possibleNbOfMeetingsForNbOfPool.toString(),
        isDisabled:
          isDisabled === true &&
          possibleNbOfMeetingsForNbOfPool !== nbOfMeetings,
      })
    );
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        style={{
          ...Layout.flex("row", "100%", 16, "start"),
          width: "100%",
          boxSizing: "border-box",
          cursor: "pointer",
        }}
        onClick={doToggleCollapse}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Configuration de la phase de poules</span>
      </div>
      {!isCollapsed && nbOfPoolsValues.length > 0 && (
        <fieldset
          style={{
            boxSizing: "border-box",
            border: `1px solid ${palette.background.dp06}`,
            width: "100%",
            padding: 16,
          }}
        >
          <legend
            style={{
              padding: "0px 8px",
              textTransform: "uppercase",
              fontSize: fontSize.tn,
              color: palette.foreground.dp06,
              fontWeight: "bolder",
            }}
          >
            Nombre de poules
          </legend>

          <RadioButtonsArray
            selectedValue={nbOfPools?.toString() ?? ""}
            onSelectValue={(key) => {
              onSelectNbOfPools(Number(key));
            }}
            values={nbOfPoolsValues}
            style={{
              width: "100%",
            }}
          />
        </fieldset>
      )}
      {!isCollapsed && possibleNbOfMeetingsPerPoolValues !== undefined && (
        <fieldset
          style={{
            boxSizing: "border-box",
            border: `1px solid ${palette.background.dp06}`,
            width: "100%",
            padding: 16,
          }}
        >
          <legend
            style={{
              padding: "0px 8px",
              textTransform: "uppercase",
              fontSize: fontSize.tn,
              color: palette.foreground.dp06,
              fontWeight: "bolder",
            }}
          >
            Nombre de rencontres uniques par poule
          </legend>

          <RadioButtonsArray
            selectedValue={nbOfMeetings?.toString() ?? ""}
            onSelectValue={(key) => {
              onSelectNbOfMeetings(Number(key));
            }}
            values={possibleNbOfMeetingsPerPoolValues}
            style={{ width: "100%" }}
          />
        </fieldset>
      )}
      {!isCollapsed &&
        possibleNbOfMeetingsPerPoolValues !== undefined &&
        nbOfMeetings !== undefined &&
        !isDisabled && (
          <button
            disabled={isDisabled}
            onClick={doValidateConfig}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.success.foreground,
            }}
          >
            <PlayIconOutlined side={iconSize.sm} />
            Verrouiller et lancer le tournoi
          </button>
        )}
    </div>
  );

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }

  function doValidateConfig(): void {
    const confirmationMessage =
      "Voulez-vous lancer le tournoi ? Les équipes et les paramètres de la phase de poules ne seront plus modifiables.";
    if (window.confirm(confirmationMessage)) {
      onValidateConfig();
    }
  }
}
