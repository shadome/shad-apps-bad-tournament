import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  DangerousIconOutlined,
} from "../../components/Icons";

interface Props extends ComponentProps<"div"> {
  onResetTournament: () => void;
}

export default function TournamentReset({
  onResetTournament,
  ...props
}: Props) {
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(true);
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        onClick={doToggleCollapse}
        style={{
          ...Layout.flex("row", "100%", 8, "start"),
          boxSizing: "border-box",
          cursor: "pointer",
        }}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Réinitialiser le tournoi</span>
      </div>
      {!isCollapsed && (
        <div
          style={{
            ...Layout.flex("column", "fit-content", 16, "left"),
            boxSizing: "border-box",
            width: "100%",
          }}
        >
          <button
            onClick={doResetTournament}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.error.foreground,
            }}
          >
            <DangerousIconOutlined side={iconSize.sm} />
            Réinitialiser le tournoi
          </button>
        </div>
      )}
    </div>
  );

  function doResetTournament(): void {
    const confirmationMessage =
      "Voulez-vous réinitialiser le tournoi ? Toutes les informations enregistrées pour les rencontres seront supprimées.";
    if (window.confirm(confirmationMessage)) {
      onResetTournament();
    }
  }

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }
}
