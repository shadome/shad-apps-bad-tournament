import React, { ComponentProps } from "react";
import * as Config from "../../../repositories/tournamentConfig";
import * as Layout from "../../css/Layout";
import * as Appearance from "../../css/Appearance";
import { fontSize, iconSize } from "../../CssInterop";
import { UndoIcon } from "../../components/Icons";
import { TournamentPhase } from "../../../core/types";
import {
  LocalStorageEvent,
  persistState,
} from "../../../repositories/localstorage";

interface Props extends ComponentProps<"div"> {}

export default function ActionBar({ ...props }: Props): JSX.Element {
  return <></>;
  // const currentConfig = Config.get();
  // const isResetDisabled = currentConfig.current_phase === TournamentPhase.Setup;

  // return (
  //   <div
  //     {...props}
  //     style={{
  //       ...props?.style,
  //     }}
  //   >
  //     <button
  //       disabled={isResetDisabled}
  //       onClick={doReset}
  //       className="unset bbox hoverable disable"
  //       style={{
  //         ...Layout.flex("row", undefined, 8, "center"),
  //         ...Appearance.boundSize("fit-content", 48),
  //         alignSelf: "end",
  //         fontSize: fontSize.sm,
  //         padding: "2px 16px 2px 8px",
  //         cursor: "pointer",
  //         textTransform: "uppercase",
  //       }}
  //     >
  //       <UndoIcon side={iconSize.sm} />
  //       réinitialiser
  //     </button>
  //   </div>
  // );

  // function doReset(): void {
  //   if (isResetDisabled) {
  //     return;
  //   }

  //   dispatchPersistenceEvent(
  //     LocalStorageEvent.TournamentReset,
  //     `Réinitialisation du tournoi`
  //   );
  // }
}
