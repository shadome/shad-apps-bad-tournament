import { ComponentProps } from "react";
import { Team } from "../../../core/types";
import * as Layout from "../../css/Layout";
import TeamItem from "./team_list/TeamItem";

interface Props extends ComponentProps<"div"> {
  // true if the actions of this container should not be reachable
  isDisabled: boolean;
  teams: Team[];
  onMoveTeam: (teamIdMovingUp: number, teamIdMovingDown: number) => void;
}

export default function TeamList({
  isDisabled,
  teams,
  onMoveTeam,
  ...props
}: Props): JSX.Element {
  const orderedTeams = [...teams].sort(
    (a, b) => a.estimated_ranking - b.estimated_ranking
  );
  return (
    <div
      {...props}
      style={{
        ...props?.style,
        width: "100%",
        display: "grid",
        gap: 16,
        gridTemplateColumns: "repeat(auto-fill,minmax(410px, 1fr))",
      }}
    >
      {orderedTeams.map((team, idx) => (
        <TeamItem
          key={team.id}
          team={team}
          areActionsDisabled={isDisabled}
          canMoveTeamUp={!isDisabled && idx !== 0}
          canMoveTeamDown={!isDisabled && idx !== orderedTeams.length - 1}
          onMoveTeamUp={() => {
            onMoveTeam(orderedTeams[idx].id, orderedTeams[idx - 1].id);
          }}
          onMoveTeamDown={() => {
            onMoveTeam(orderedTeams[idx + 1].id, orderedTeams[idx].id);
          }}
        />
      ))}
    </div>
  );
}
