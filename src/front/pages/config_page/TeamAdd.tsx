import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  PlusIcon,
  UndoIcon,
} from "../../components/Icons";

interface Props extends ComponentProps<"div"> {
  disabled: boolean;
  onAddTeam: (
    teamName: string,
    player1Name: string,
    player2Name: string,
    optPlayer3Name?: string,
    optPlayer4Name?: string
  ) => void;
}

const DEFAULT_INPUT_VALUE: string = "";

export default function TeamAdd({ disabled, onAddTeam, ...props }: Props) {
  const [teamName, setTeamName] = React.useState<string>(DEFAULT_INPUT_VALUE);
  const [player1Name, setPlayer1Name] =
    React.useState<string>(DEFAULT_INPUT_VALUE);
  const [player2Name, setPlayer2Name] =
    React.useState<string>(DEFAULT_INPUT_VALUE);
  const [optPlayer3Name, setOptPlayer3Name] =
    React.useState<string>(DEFAULT_INPUT_VALUE);
  const [optPlayer4Name, setOptPlayer4Name] =
    React.useState<string>(DEFAULT_INPUT_VALUE);
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(true);

  const isSubmissionDisabled =
    disabled ||
    teamName === DEFAULT_INPUT_VALUE ||
    player1Name === DEFAULT_INPUT_VALUE ||
    player2Name === DEFAULT_INPUT_VALUE;

  const isResetDisabled =
    disabled ||
    (teamName === DEFAULT_INPUT_VALUE &&
      player1Name === DEFAULT_INPUT_VALUE &&
      player2Name === DEFAULT_INPUT_VALUE &&
      optPlayer3Name === DEFAULT_INPUT_VALUE &&
      optPlayer4Name === DEFAULT_INPUT_VALUE);

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        onClick={doToggleCollapse}
        style={{
          ...Layout.flex("row", "100%", 8, "start"),
          boxSizing: "border-box",
          cursor: "pointer",
        }}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Ajouter une équipe</span>
      </div>
      {!isCollapsed && (
        <div
          style={{
            ...Layout.flex("column", "fit-content", 16),
            boxSizing: "border-box",
            width: "100%",
          }}
        >
          <input
            placeholder="Nom de l'équipe"
            value={teamName}
            onChange={doTeamNameChange}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
            }}
          />
          <input
            placeholder="Nom du joueur 1"
            value={player1Name}
            onChange={doPlayer1NameChange}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
            }}
          />
          <input
            placeholder="Nom du joueur 2"
            value={player2Name}
            onChange={doPlayer2NameChange}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
            }}
          />
          <input
            placeholder="Nom du joueur 3 (optionnel)"
            value={optPlayer3Name}
            onChange={doOptPlayer3NameChange}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
            }}
          />
          <input
            placeholder="Nom du joueur 4 (optionnel)"
            value={optPlayer4Name}
            onChange={doOptPlayer4NameChange}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
            }}
          />
          <div style={{ ...Layout.flex("row-reverse", "100%", 16) }}>
            <button
              disabled={isSubmissionDisabled}
              onClick={doSubmit}
              className="unset bbox hoverable disable"
              style={{
                ...Layout.flex("row", undefined, 8, "center"),
                ...Appearance.boundSize("fit-content", 48),
                alignSelf: "end",
                fontSize: fontSize.sm,
                fontWeight: "bold",
                padding: "2px 16px 2px 8px",
                cursor: "pointer",
                color: colours.success.foreground,
              }}
            >
              <PlusIcon side={iconSize.sm} />
              Ajouter
            </button>
            <button
              disabled={isResetDisabled}
              onClick={doReset}
              className="unset bbox hoverable disable"
              style={{
                ...Layout.flex("row", undefined, 8, "center"),
                ...Appearance.boundSize("fit-content", 48),
                alignSelf: "end",
                fontSize: fontSize.sm,
                fontWeight: "bold",
                padding: "2px 16px 2px 8px",
                cursor: "pointer",
              }}
            >
              <UndoIcon side={iconSize.sm} />
              Réinitialiser
            </button>
          </div>
        </div>
      )}
    </div>
  );

  function doTeamNameChange(e: React.ChangeEvent<HTMLInputElement>): void {
    setTeamName(e.target.value);
  }

  function doPlayer1NameChange(e: React.ChangeEvent<HTMLInputElement>): void {
    setPlayer1Name(e.target.value);
  }

  function doPlayer2NameChange(e: React.ChangeEvent<HTMLInputElement>): void {
    setPlayer2Name(e.target.value);
  }

  function doOptPlayer3NameChange(
    e: React.ChangeEvent<HTMLInputElement>
  ): void {
    setOptPlayer3Name(e.target.value);
  }

  function doOptPlayer4NameChange(
    e: React.ChangeEvent<HTMLInputElement>
  ): void {
    setOptPlayer4Name(e.target.value);
  }

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }

  function doSubmit(): void {
    const _optPlayer3Name = optPlayer3Name === "" ? undefined : optPlayer3Name;
    const _optPlayer4Name = optPlayer4Name === "" ? undefined : optPlayer4Name;
    onAddTeam(
      teamName,
      player1Name,
      player2Name,
      _optPlayer3Name,
      _optPlayer4Name
    );
    doReset();
  }

  function doReset(): void {
    setTeamName(DEFAULT_INPUT_VALUE);
    setPlayer1Name(DEFAULT_INPUT_VALUE);
    setPlayer2Name(DEFAULT_INPUT_VALUE);
    setOptPlayer3Name(DEFAULT_INPUT_VALUE);
    setOptPlayer4Name(DEFAULT_INPUT_VALUE);
  }
}
