import React, { ComponentProps } from "react";
import { colours, fontSize, iconSize, palette } from "../../CssInterop";
import * as Appearance from "../../css/Appearance";
import * as Layout from "../../css/Layout";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  DeleteIconOutlined,
  NoAccountIconOutlined,
} from "../../components/Icons";
import { Team } from "../../../core/types";
import HorizontalSeparator from "../../components/HorizontalSeparator";

interface Props extends ComponentProps<"div"> {
  disabled: boolean;
  allTeams: Team[];
  onDeleteTeam: (teamId: number) => void;
  onDeleteAllTeams: () => void;
}

const DEFAULT_SELECT_VALUE: number = -1;

export default function TeamDelete({
  disabled,
  allTeams,
  onDeleteTeam,
  onDeleteAllTeams,
  ...props
}: Props) {
  const [selectedTeamId, setSelectedTeamId] =
    React.useState<number>(DEFAULT_SELECT_VALUE);
  const [isCollapsed, setIsCollapsed] = React.useState<boolean>(true);

  const isDeleteButtonDisabled =
    disabled || selectedTeamId === DEFAULT_SELECT_VALUE;

  const isDeleteAllTeamsButtonDisabled = disabled || allTeams.length === 0;

  return (
    <div
      {...props}
      style={{
        ...props?.style,
        ...Layout.flex("column", "fit-content", 16, "start"),
        boxSizing: "border-box",
        color: palette.foreground.dp04,
        padding: 16,
        borderRadius: 4,
      }}
    >
      <div
        onClick={doToggleCollapse}
        style={{
          ...Layout.flex("row", "100%", 8, "start"),
          boxSizing: "border-box",
          cursor: "pointer",
        }}
      >
        {isCollapsed ? (
          <ChevronUpIcon side={iconSize.md} />
        ) : (
          <ChevronDownIcon side={iconSize.md} />
        )}
        <span>Supprimer une équipe</span>
      </div>
      {!isCollapsed && (
        <div
          style={{
            ...Layout.flex("column", "fit-content", 16),
            boxSizing: "border-box",
            width: "100%",
          }}
        >
          <select
            value={selectedTeamId}
            onChange={doSelectTeamName}
            className="unset bbox focusable-borderbot"
            style={{
              width: "100%",
              height: 48,
              textAlign: "left",
              padding: 12,
              backgroundColor: palette.background.dp05,
              color: palette.foreground.dp02,
              cursor: "pointer",
            }}
          >
            <option value={DEFAULT_SELECT_VALUE}>Choisir une équipe</option>
            {allTeams.map((team) => (
              <option key={team.id} value={team.id}>
                {team.name}
              </option>
            ))}
          </select>

          <button
            disabled={isDeleteButtonDisabled}
            onClick={doDeleteTeam}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", undefined, 8, "center"),
              ...Appearance.boundSize("fit-content", 48),
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.error.foreground,
            }}
          >
            <DeleteIconOutlined side={iconSize.sm} />
            Supprimer
          </button>
          <HorizontalSeparator />
          <button
            disabled={isDeleteAllTeamsButtonDisabled}
            onClick={doDeleteAllTeams}
            className="unset bbox hoverable disable"
            style={{
              ...Layout.flex("row", "100%", 8, "center", "left"),
              ...Appearance.boundSize("100%", 48),
              alignSelf: "end",
              fontSize: fontSize.sm,
              padding: "2px 16px 2px 8px",
              cursor: "pointer",
              fontWeight: "bold",
              color: colours.error.foreground,
            }}
          >
            <NoAccountIconOutlined side={iconSize.sm} />
            Supprimer toutes les équipes
          </button>
        </div>
      )}
    </div>
  );

  function doDeleteAllTeams(): void {
    const confirmationMessage =
      "Voulez-vous vraiment supprimer toutes les équipes et leurs informations associées ?";
    if (
      !isDeleteAllTeamsButtonDisabled &&
      window.confirm(confirmationMessage)
    ) {
      onDeleteAllTeams();
      setSelectedTeamId(DEFAULT_SELECT_VALUE);
    }
  }

  function doSelectTeamName(evt: React.ChangeEvent<HTMLSelectElement>): void {
    const teamId = Number(evt.target.value);
    setSelectedTeamId(teamId);
  }

  function doToggleCollapse(): void {
    setIsCollapsed(!isCollapsed);
  }

  function doDeleteTeam(): void {
    if (disabled || isDeleteButtonDisabled) {
      return;
    }
    onDeleteTeam(selectedTeamId);
    setSelectedTeamId(DEFAULT_SELECT_VALUE);
  }
}
