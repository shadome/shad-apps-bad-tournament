import React from "react";
import { Team, TournamentConfig, TournamentPhase } from "../../core/types";
import * as Teams from "../../repositories/teams";
import * as Config from "../../repositories/tournamentConfig";
import * as Meetings from "../../repositories/meetings";
import TeamsModification from "./config_page/TeamList";
import PoolsConfig from "./config_page/PoolsConfig";
import { getMaxNbOfPools, getPossibleNbOfMeetings } from "../../core/functions";
import TeamAdd from "./config_page/TeamAdd";
import * as Layout from "../css/Layout";
import * as Appearance from "../css/Appearance";
import { palette } from "../CssInterop";
import TeamDelete from "./config_page/TeamDelete";
import TournamentReset from "./config_page/TournamentReset";
import HorizontalSeparator from "../components/HorizontalSeparator";
import {
  LocalStorageEvent,
  persistState as persistState,
} from "../../repositories/localstorage";

export const CONFIG_PAGE_PATH: string = "/";

export interface PoolsConfigData {
  nbOfPools: number;
  possibleNbOfMeetingsForNbOfPools: number[];
}

function getNbOfPoolsAndAssociatedPossibleNbOfMeetings(
  allTeamsLength: number
): PoolsConfigData[] {
  const maxNbOfPools = getMaxNbOfPools(allTeamsLength);
  const result: PoolsConfigData[] = [...Array(maxNbOfPools).keys()]
    .map((x) => x + 1)
    .map((nbOfPools) => ({
      nbOfPools: nbOfPools,
      possibleNbOfMeetingsForNbOfPools: getPossibleNbOfMeetings(
        allTeamsLength,
        nbOfPools
      ).filter((nb) => nb > 0),
    }))
    .filter((x) => x.possibleNbOfMeetingsForNbOfPools.length > 0);
  return result;
}

export default function ConfigPage(): JSX.Element {
  const [config, setConfig] = React.useState<TournamentConfig>(Config.get());
  const [allTeams, setAllTeams] = React.useState<Team[]>(Teams.getAll());

  const poolsConfigData = getNbOfPoolsAndAssociatedPossibleNbOfMeetings(
    allTeams.length
  );

  const isConfigModificationDisabled =
    config.current_phase !== TournamentPhase.Setup;

  return (
    <>
      <div
        className="bbox"
        style={{
          padding: 16,
          gridArea: "content",
          overflowY: "scroll",
          ...Appearance.boundHeight("100vh"),
          ...Layout.flex("row", "100%", 16, undefined, undefined, "wrap"),
        }}
      >
        <TeamsModification
          teams={allTeams}
          isDisabled={isConfigModificationDisabled}
          onMoveTeam={doMoveTeam}
          style={{
            ...Layout.size("100%", "fit-content"),
            flexGrow: 1,
          }}
        />
      </div>
      <div
        className="bbox"
        style={{
          padding: 16,
          gridArea: "rightpane",
          ...Layout.flex("column", "100%", 16),
          ...Appearance.boundWidth(450),
          ...Appearance.boundHeight("100vh"),
          overflowY: "scroll",
          backgroundColor: palette.background.dp04,
          borderLeft: `2px solid ${palette.background.dp05}`,
        }}
      >
        <PoolsConfig
          isDisabled={isConfigModificationDisabled}
          nbOfPools={config.nb_of_pools}
          poolsConfigData={poolsConfigData}
          nbOfMeetings={config.nb_of_meetings_per_team}
          onSelectNbOfPools={doSelectNbOfPools}
          onSelectNbOfMeetings={doSelectNbOfMeetings}
          onValidateConfig={doValidateConfig}
          style={{
            ...Layout.size("100%", "fit-content"),
          }}
        />
        {!isConfigModificationDisabled && (
          <>
            <HorizontalSeparator />
            <TeamAdd
              onAddTeam={doAddTeam}
              disabled={isConfigModificationDisabled}
              style={{
                ...Layout.size("100%", "fit-content"),
              }}
            />
            <HorizontalSeparator />
            <TeamDelete
              onDeleteTeam={doDeleteTeam}
              onDeleteAllTeams={doDeleteAllTeams}
              allTeams={allTeams}
              disabled={isConfigModificationDisabled}
              style={{
                ...Layout.size("100%", "fit-content"),
              }}
            />
          </>
        )}
        <HorizontalSeparator />
        <TournamentReset
          onResetTournament={doResetTournament}
          style={{
            ...Layout.size("100%", "fit-content"),
          }}
        />
      </div>
    </>
  );

  function doSelectNbOfPools(nbOfPools: number): void {
    Config.setNbOfPools(nbOfPools);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doSelectNbOfPools,
      `Choix du nombre de poules (${nbOfPools})`
    );
  }

  function doSelectNbOfMeetings(nbOfMeetings: number): void {
    Config.setNbOfMeetingsPerTeam(nbOfMeetings);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doSelectNbOfMeetings,
      `Choix du nombre de rencontres pour chaque équipe en phase de poules (${nbOfMeetings})`
    );
  }

  function doValidateConfig(): void {
    Config.setPhase(TournamentPhase.Ranking);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doValidateConfig,
      `Démarrage de la phase de classement`
    );
  }

  function doResetTournament(): void {
    // Note: no need for a "setMeetings" state setter because this page
    // doesn't need to re-render when the meetings are updated.
    Meetings.reset();
    Config.reset();
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doResetTournament,
      `Réinitialisation du tournoi (rencontres et configuration remises à zéro)`
    );
  }

  function doMoveTeam(teamIdToMoveUp: number, teamIdToMoveDown: number): void {
    Teams.invertRespectiveEstimatedRankings(teamIdToMoveDown, teamIdToMoveUp);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doMoveTeam,
      `Inversion du rang estimé des équipes '${
        Teams.getById(teamIdToMoveDown).name
      }' et '${Teams.getById(teamIdToMoveUp).name}'`
    );
  }

  function doAddTeam(
    teamName: string,
    player1Name: string,
    player2Name: string,
    player3Name?: string,
    player4Name?: string
  ): void {
    Config.resetPoolsConfiguration();
    Teams.addTeam(teamName, player1Name, player2Name, player3Name, player4Name);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doAddTeam,
      `Ajout de l'équipe '${teamName}'`
    );
  }

  function doDeleteTeam(teamIdToDelete: number): void {
    const teamName = Teams.getById(teamIdToDelete).name;
    Config.resetPoolsConfiguration();
    Teams.deleteTeam(teamIdToDelete);
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doDeleteTeam,
      `Suppression de l'équipe ${teamName}`
    );
  }

  function doDeleteAllTeams(): void {
    Config.resetPoolsConfiguration();
    Teams.reset();
    _reloadRepositoryState();
    persistState(
      LocalStorageEvent.doDeleteAllTeams,
      `Suppression de toutes les équipes`
    );
  }

  /**
   * Modifying the application state via repository RW functions does not trigger a React component re-render, because states
   * are not used in the Repository layer.
   * <br><br><br>
   * Call this function whenever an action method uses repository function which modifies the application state
   * to re-fetch the repository data and update the associated React state variables, which in turn provokes
   * a component re-render. */
  function _reloadRepositoryState(): void {
    setConfig(Config.get());
    setAllTeams(Teams.getAll());
  }
}
