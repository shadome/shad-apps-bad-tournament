interface RadioButtonsValue {
  // selection key unique amongst other radio button values
  key: number;
  displayValue: string;
}

export enum Direction {
  Vertical,
  Horizontal,
}

interface RadioButtonsProps {
  direction?: Direction;
  // html tag shared between input fields of type radio
  name: string;
  // currently selected value
  selectedValue: number;
  // callback for a new value selection
  onSelect: (key: number) => void;
  // values which can be selected and which are displayed as radio button choices
  values: RadioButtonsValue[];
}

export default function RadioButtons({
  direction = Direction.Vertical,
  name,
  selectedValue,
  onSelect,
  values,
}: RadioButtonsProps) {
  return (
    <div
      className={
        direction === Direction.Vertical
          ? "flexcol x-start g8"
          : "flexrow x-start g16"
      }
    >
      {values.map((value) => (
        <label className="radio-btn" key={value.key}>
          <input
            className={direction === Direction.Vertical ? "mr16" : "mr8"}
            name={name}
            type="radio"
            checked={selectedValue === value.key}
            onChange={() => {
              onSelect(value.key);
            }}
          />
          <span>{value.displayValue}</span>
        </label>
      ))}
    </div>
  );
}
