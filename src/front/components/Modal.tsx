import React from "react";
import { zIndexes } from "../CssInterop";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";

interface Props extends React.HTMLProps<HTMLDivElement> {
  isOpen: boolean;
  modalTitle?: React.ReactNode;
  onClose: () => void;
}

export default function Modal({
  isOpen,
  onClose: doClose,
  modalTitle,
  children,
  ...props
}: Props): JSX.Element {
  return (
    <div
      style={{
        position: "fixed",
        display: isOpen ? "flex" : "none",
        top: 0,
        left: 0,
        ...Layout.size("100%"),
        ...Appearance.boundSize("100%"),
        zIndex: zIndexes.modal,
        backgroundColor: "#0008",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {/* The below div is used because the above div 
            cannot handle the onClick event 
            else clicking within the modal would trigger it */}
      <div
        onClick={doClose}
        style={{
          ...Layout.size("100%"),
          ...Appearance.boundSize("100%"),
          top: 0,
          left: 0,
          position: "absolute",
        }}
      />
      <div
        {...props}
        style={{
          ...props?.style,
          ...Layout.size("fit-content"),
          ...Appearance.boundSize("fit-content", "fit-content", "90%", "90%"),
          position: "relative",
          overflowX: "scroll",
          overflowY: "scroll",
          height: "fit-content",
          width: "fit-content",
        }}
      >
        {children}
      </div>
    </div>
  );
}
