import { ComponentProps } from "react";
import { colours, fontSize, palette } from "../CssInterop";
import * as Appearance from "../css/Appearance";

export interface RadioButtonsValue {
  // selection key unique amongst other radio button values
  key: string;
  displayValue: string;
  // default is false
  isDisabled?: boolean;
}

interface Props extends ComponentProps<"div"> {
  selectedValue: string;
  onSelectValue: (key: string) => void;
  values: RadioButtonsValue[];
}

export default function RadioButtonsArray({
  selectedValue,
  onSelectValue,
  values,
  ...props
}: Props): JSX.Element {
  function isSelectedValue(idx: number): boolean {
    return selectedValue === values[idx].key;
  }
  return (
    <div
      {...props}
      className={`${props?.className}`}
      style={{
        ...props?.style,
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        gap: 5,
      }}
    >
      {values.map((value, i) => (
        <button
          key={value.key}
          className="unset hoverable center"
          disabled={value.isDisabled}
          style={{
            ...Appearance.boundSize(64, 48),
            flexWrap: "wrap",
            fontSize: fontSize.sm,
            ...(isSelectedValue(i)
              ? {
                  backgroundColor: colours.info.foreground,
                  color: palette.background.dp01,
                  border: `1px solid ${colours.info.foreground}`,
                  cursor: "default",
                  fontWeight: 800,
                  filter: "none",
                }
              : value.isDisabled
              ? {
                  cursor: "default",
                  border: `1px solid ${palette.background.dp08}`,
                  color: palette.background.dp08,
                }
              : {
                  cursor: "pointer",
                  border: `1px solid ${palette.background.dp08}`,
                }),
          }}
          onClick={() => {
            !isSelectedValue(i) && onSelectValue(value.key);
          }}
        >
          {value.displayValue}
        </button>
      ))}
    </div>
  );
}
