import { palette } from "../CssInterop";
import * as Appearance from "../css/Appearance";

export default function HorizontalSeparator(): JSX.Element {
  return (
    <div
      style={{
        boxSizing: "border-box",
        gridArea: "separator",
        backgroundColor: palette.background.dp05,
        ...Appearance.boundSize("100%", 2),
      }}
    />
  );
}
