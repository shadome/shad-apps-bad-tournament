import * as React from "react";
import * as Appearance from "../css/Appearance";
import * as Layout from "../css/Layout";
import {
  backgroundColour,
  borderColour,
  colours,
  palette,
} from "../CssInterop";

interface ToggleProps extends React.HTMLProps<HTMLLabelElement> {
  id?: string;
  isChecked: boolean;
  onToggle: (checked: boolean) => void;
}

export default function Toggle({
  id,
  isChecked,
  onToggle,
  ...props
}: ToggleProps) {
  const toggleHeight = 21;
  const toggleWidth = toggleHeight * 2;
  const togglePadding = 4;
  const squareDimension = toggleHeight;
  const squareLeftOffset = isChecked
    ? toggleWidth + togglePadding - squareDimension
    : togglePadding;

  return (
    <label
      htmlFor={id}
      {...props}
      style={{
        display: "block",
        cursor: "pointer",
        transition: "200ms",
        border: `1px solid ${palette.background.dp10}`,
        backgroundColor: isChecked ? palette.foreground.dp08 : undefined,
        ...props.style,
        // required styling which cannot be overriden by the caller
        ...Appearance.boundSize(toggleWidth, toggleHeight),
        position: "relative",
        padding: togglePadding,
      }}
    >
      <input
        id={id}
        type="checkbox"
        checked={isChecked}
        onChange={(evt) => {
          onToggle(evt.target.checked);
        }}
        style={{
          width: 0,
          height: 0,
          display: "none",
        }}
      />
      <span
        style={{
          ...Layout.size(squareDimension),
          transition: "inherit",
          position: "absolute",
          left: squareLeftOffset,
          backgroundColor: isChecked ? "currentcolor" : palette.background.dp10,
        }}
      />
    </label>
  );
}
