export type Size =
  | "xt" // extra tiny
  | "tn" // tiny
  | "xs" // extra small
  | "sm" // small
  | "md" // medium
  | "lg" // large
  | "xl"; // extra large

export type TextColour =
  | "strong"
  | "regular"
  | "soft"
  | "disabled"
  | "link"
  | "reverse"; // text colour on a "foreground-like" surface

export type Depth = "dp0" | "dp1" | "dp2";

export type Shade = "background" | "average" | "foreground" | "hovered";

export type Aspect = "neutral" | "info" | "success" | "warning" | "error";

export type Overlay = "hovered" | "selected" | "none" | "unreachable" | "both";

export type BorderColour = "default" | "light" | "dark";

export const overlay: Readonly<Record<Overlay, string>> = {
  hovered: "#fff1",
  selected: "#fff2",
  both: "#fff3",
  none: "#fff0",
  unreachable: "#0005",
};

export const fontSize: Readonly<Record<Size, string>> = {
  xt: "11px",
  tn: "13px",
  xs: "15px",
  sm: "17px",
  md: "20px",
  lg: "23px",
  xl: "26px",
};

export const iconSize: Readonly<Record<Size, string>> = {
  xt: "10px",
  tn: "14px",
  xs: "18px",
  sm: "22px",
  md: "26px",
  lg: "30px",
  xl: "34px",
};

export const borderColour: Readonly<Record<BorderColour, string>> = {
  default: "#333c44",
  light: "#738496",
  dark: "#171a1d",
};

export const textColour: Readonly<Record<TextColour, string>> = {
  regular: "#b6c2cf",
  disabled: "#5a5a5a",
  link: "#9af0de",
  reverse: "#0d1117",
  soft: "#a0a0a0",
  strong: "#b6c2cf", // TODO
};

export const backgroundColour: Readonly<Record<Depth, string>> = {
  dp0: "#1d2125",
  dp1: "#22272b",
  dp2: "#1d2125",
};

export const colours: Readonly<
  Record<Aspect, Readonly<Record<Shade, string>>>
> = {
  // https://mdigi.tools/color-shades/#d74545 9 shades, number 3, 5, 7
  neutral: {
    average: "#21262d",
    background: "#161b22",
    foreground: "#a0a0a0",
    hovered: "#21262d",
  },
  info: {
    background: "#004fa7",
    foreground: "#a5d7e8",
    average: "#1c82ad",
    hovered: "#1c82ad",
  },
  success: {
    average: "#009b22",
    foreground: "#88c38b",
    background: "#10452d",
    hovered: "#009b22",
  },
  warning: {
    average: "#f68a1c",
    foreground: "#f5a524",
    hovered: "#ffe2b7",
    background: "#581600",
  },
  error: {
    hovered: "#d22d2d",
    average: "#d22d2d",
    foreground: "#ff7f7f",
    background: "#560015",
  },
};

export type ZIndexKeys = "none" | "appbar" | "modal";

export const zIndexes: Readonly<Record<ZIndexKeys, number>> = {
  none: 0,
  appbar: 1,
  modal: 2,
};

export type Palette = "foreground" | "background";
export type Depth2 =
  | "dp01"
  | "dp02"
  | "dp03"
  | "dp04"
  | "dp05"
  | "dp06"
  | "dp07"
  | "dp08"
  | "dp09"
  | "dp10";

// https://codepen.io/luispadarotto/full/qBQjoOK
export const palette: Readonly<
  Record<Palette, Readonly<Record<Depth2, string>>>
> = {
  background: {
    // dp01: "#1e232a",
    // dp02: "#2d3642",
    // dp03: "#3c4856",
    // dp04: "#506277",
    // dp05: "#6a84a4",
    // dp06: "#85a7d0",
    // dp07: "#b7cee8",
    // dp08: "#e5edf3",
    // dp09: "#e9f1f8",
    // dp10: "#ecf4fa",
    dp01: "#111111",
    dp02: "#121416",
    dp03: "#181d22",
    dp04: "#192228",
    dp05: "#1c2831",
    dp06: "#1e2e3a",
    dp07: "#2f3d47",
    dp08: "#455056",
    dp09: "#5e6264",
    dp10: "#6a6f71",
  },
  foreground: {
    dp01: "#f0f9ff",
    dp02: "#f0f9ff",
    dp03: "#f0f8ff",
    dp04: "#f0f8ff",
    dp05: "#c9e3ff",
    dp06: "#99c1f1",
    dp07: "#769acc",
    dp08: "#5876a6",
    dp09: "#3d5480",
    dp10: "#2a3959",
  },
};

export function getNumberFromCss(str: string): number | undefined {
  const regex = /\d+/g;
  const optNumber = str.match(regex)?.join("");
  return optNumber === undefined ? undefined : Number(optNumber);
}

/* Serialises the above objects to pure css root variables. */
export default function CssInteropInitialiser() {
  function serialiseObjectStrings(
    separator: string,
    object: Readonly<Record<any, any>>, // Readonly<Record<X, Y>> is recursive: Y could be string, number or another Readonly<Record<X1,Y1>>, no idea how to do that in typescript
    currentPath: string[],
    currentOutput: string[]
  ) {
    // do not mix up what is passed to recursive calls through reference (currentOutput) and what is copied (currentPath)
    for (const key in object) {
      const element = object[key];
      if (typeof element === "string" || typeof element === "number") {
        const tag = currentPath.concat(key.toString()).join(separator);
        const elementStr = `${tag}: ${element};`;
        currentOutput.push(elementStr);
      } else if (typeof element === "object") {
        serialiseObjectStrings(
          separator,
          element,
          currentPath.concat([key.toString()]),
          currentOutput
        );
      }
    }
  }
  const s = "-";
  let cssOutput: string[] = [];
  serialiseObjectStrings(s, fontSize, ["--css", "fontsize"], cssOutput);
  serialiseObjectStrings(s, iconSize, ["--css", "iconsize"], cssOutput);
  serialiseObjectStrings(s, colours, ["--css", "colours"], cssOutput);
  serialiseObjectStrings(s, zIndexes, ["--css", "zindexes"], cssOutput);
  serialiseObjectStrings(s, backgroundColour, ["--css", "bgcolour"], cssOutput);
  serialiseObjectStrings(s, borderColour, ["--css", "bordercolour"], cssOutput);
  serialiseObjectStrings(s, textColour, ["--css", "textcolour"], cssOutput);
  serialiseObjectStrings(s, overlay, ["--css", "overlay"], cssOutput);
  serialiseObjectStrings(s, palette, ["--css", "palette"], cssOutput);

  return (
    <style
      style={{
        display: "none",
      }}
    >
      {`:root { ${cssOutput.join(" ")} }`}
    </style>
  );
}
